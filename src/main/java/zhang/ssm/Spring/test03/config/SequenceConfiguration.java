package zhang.ssm.Spring.test03.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import zhang.ssm.Spring.test03.DatePrefixGenerator;
import zhang.ssm.Spring.test03.SequenceGenerator;


@Configuration
public class SequenceConfiguration {

    @Bean
    public DatePrefixGenerator datePrefixGenerator() {
        DatePrefixGenerator dpg = new DatePrefixGenerator();
        dpg.setPattern("yyyy-MM-dd ");
        return dpg;
    }

    @Bean
    public SequenceGenerator sequenceGenerator() {
        SequenceGenerator sequence = new SequenceGenerator();
        sequence.setInitial(100000);
        sequence.setSuffix("A");
        sequence.setPrefixGenerator(datePrefixGenerator());
        return sequence;
    }
}
