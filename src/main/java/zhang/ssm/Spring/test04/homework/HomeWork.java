package zhang.ssm.Spring.test04.homework;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public interface HomeWork {
    void doHomeWork();
}
