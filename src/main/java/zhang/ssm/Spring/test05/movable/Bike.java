package zhang.ssm.Spring.test05.movable;

import org.springframework.stereotype.Service;

@Service("bike")
public class Bike implements Movable{
    @Override
    public String go(String content) {
        return  content;
    }
}
