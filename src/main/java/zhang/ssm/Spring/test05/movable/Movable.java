package zhang.ssm.Spring.test05.movable;

public interface Movable {
    String go(String content);
}
