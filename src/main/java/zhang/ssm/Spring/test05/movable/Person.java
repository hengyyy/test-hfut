package zhang.ssm.Spring.test05.movable;

import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class Person {
    @Resource(name="bike")
    private Movable movable;

    public Person() {
        movable = new Bike();
    }

    public void go(){
        System.out.println(movable.go("bike S"));
    }
}


