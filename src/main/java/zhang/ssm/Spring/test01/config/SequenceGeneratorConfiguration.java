package zhang.ssm.Spring.test01.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import zhang.ssm.Spring.test01.POJO.SequenceGenerator;

@Configuration//这个注解告诉Spring它是个配置类。
//当Spring遇到@Configuration注解的类时，它会寻找类中的bean实例定义，这些定义是由@Bean注解所修饰的Java方法。这些Java方法会创建并返回一个bean实例。
public class SequenceGeneratorConfiguration {

    @Bean
    public SequenceGenerator sequenceGenerator() {

        SequenceGenerator seqgen = new SequenceGenerator();
        seqgen.setPrefix("30");
        seqgen.setSuffix("A");
        seqgen.setInitial(100000);
        return seqgen;
    }
}
