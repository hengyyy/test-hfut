package zhang.com.SomeBehave.Clone;

/**
 * java在对内存的管理方面不如 C++
 *
 * java克隆，必须要实现克隆的接口   重写克隆方法
 */
class Relation implements Cloneable{
    private String name;
    private int age;
    public Relation(String name,int age){
        this.name=name;
        this.age=age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Relation{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
    @Override
    protected Relation clone() throws CloneNotSupportedException{
        return (Relation)super.clone();
    }
}
class Person implements Cloneable{
    private String name;//保护属性安全， 通过get set 方法获取属性的信息
    private int age;
    Relation relation;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Person(String name, int age){
        this.name=name;
        this.age=age;
    }
    public Person(String name, int age,Relation r1){
        this.name=name;
        this.age=age;
        this.relation=r1;
    }
    public Person(){
    }
    @Override
    protected Person clone() throws CloneNotSupportedException{
        Person p=(Person)super.clone();
        if(p.relation==null) return p;
        p.relation=relation.clone();//完成深克隆
        return p;
    }
    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", relation=" + relation +
                '}';
    }
}
public class clone {
    public static void main(String []args){
        Person p1=new Person("路远",18);
        Person p2=new Person("山河",18);
        Person p3=p1;
        try{
            Person p4=p2.clone();
            System.out.println("p2的地址："+p2);
            System.out.println("p4的地址："+p4);
            System.out.println("p4的姓名属性："+p4.getName());
            /**浅克隆
             * p2的地址：zhang.com.TestSpace.cece2.Clone.Person@10f87f48
             * p4的地址：zhang.com.TestSpace.cece2.Clone.Person@4e50df2e
             * */
        }catch(CloneNotSupportedException e){
            e.printStackTrace();
        }
        System.out.println(p3);
        System.out.println(p1);
        /**
         * zhang.com.TestSpace.cece2.Clone.Person@10f87f48
         * zhang.com.TestSpace.cece2.Clone.Person@10f87f48
         * 既不是深拷贝也不是浅拷贝
         * 这种方式既不属于浅拷贝也不属于深拷贝就是简单的引用传递。person与person2实例都指向堆中同一个引用地址。
         */



    }
}
