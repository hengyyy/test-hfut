package zhang.com.SomeBehave.Clone;

/**
 * 深克隆
 *
 */
public class Dclone {
    public static void main(String []args){
        Person p1=new Person("玛丽",12,new Relation("提米",12));
        Person p2=new Person("汤姆",12);
        try{
            Person p3=p1.clone();
            p1.setName("玛莎");
            p1.relation.setName("提米2");
            /**
             *  p1.setName("玛莎");
             *  p1.relation.setName("提米2");
             * Person{name='玛莎', age=12, relation=Relation{name='提米2', age=12}}
             * Person{name='玛丽', age=12, relation=Relation{name='提米2', age=12}}
             * 对克隆的亲属的属性也进行了改变
             * 涉及到String在内存池的指向问题， 浅克隆的问题！
             *
             */
            System.out.println(p1);
            System.out.println(p3);
        }catch(CloneNotSupportedException e){
           // e.printStackTrace();
        }


    }
}
