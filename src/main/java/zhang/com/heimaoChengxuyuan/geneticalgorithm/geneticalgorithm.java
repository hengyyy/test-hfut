package zhang.com.heimaoChengxuyuan.geneticalgorithm;

import java.util.*;

/**
 *遗传算法
 * 人工智能的基础
 *
 */
public class geneticalgorithm {
    public static final int GENG_LENGTH=14;
    public static final int MAX_X=127;
    public static final int MAX_Y=127;
    public int x;
    public int y;
    private String gene;//显示字符串
    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public String getGene(){
        return gene;
    }
    public geneticalgorithm(int x,int y){
        if(x>this.MAX_X||y>this.MAX_Y||x<0||y<0){
            return;
        }
        this.x=x;
        this.y=y;
        String tem = Integer.toBinaryString(x);//x的字符串
        for(int i=tem.length();i<GENG_LENGTH/2;i++){
            tem="0"+tem;//不足补零
        }
        gene=tem;
        tem=Integer.toBinaryString(y);
        for(int i=tem.length();i<GENG_LENGTH/2;i++){
            tem="0"+tem;
        }
        gene=gene+tem;//叠加字符串
    }
    public geneticalgorithm(String gene){
        if(gene.length()!=this.GENG_LENGTH){
            return;
        }
        this.gene=gene;
        String xstr=gene.substring(0,this.GENG_LENGTH/2);//从该位置截取字符串
        String ystr=gene.substring(this.GENG_LENGTH/2);
        this.x=Integer.parseInt(xstr,2);
        this.y=Integer.parseInt(ystr,2);
    }

    public String toString(){
        return "X:"+this.x+",Y:"+this.y+"\tgene"+gene;
    }
    public void selfinit(String gene){//重新初始化
        if(gene.length()!=this.GENG_LENGTH){
            return;
        }
        this.gene=gene;
        String xstr=gene.substring(0,this.GENG_LENGTH/2);//从该位置截取字符串
        String ystr=gene.substring(this.GENG_LENGTH/2);
        this.x=Integer.parseInt(xstr,2);
        this.y=Integer.parseInt(ystr,2);//二进制
    }
    //初始化种群
    public static ArrayList<geneticalgorithm> initGroup(int size){
        ArrayList<geneticalgorithm> list=new ArrayList<geneticalgorithm>();
        Random rd=new Random();
        for(int i=0;i<size;i++){
            int x=rd.nextInt()%128;
            int y=rd.nextInt()%128;
            if(x<0){
                x=-x;
            }
            if(y<0){
                y=-y;
            }
            list.add(new geneticalgorithm(x,y));//插入种群对象
        }
        return list;
    }
    public int calcXY(){
        return x*x+y*y;//评估适应度
    }
    //选择计算
    public static ArrayList<geneticalgorithm>seletor(ArrayList<geneticalgorithm> fathargroup,int sonGroupsize){
        ArrayList<geneticalgorithm> sonGroup= new ArrayList<geneticalgorithm>();//下一代
        int totalfitness=0;
        double []fitness=new double[fathargroup.size()];
        for(geneticalgorithm c:fathargroup){
            totalfitness+=c.calcXY();
        }
        int index=0;
        //计算适应度
        for(geneticalgorithm c:fathargroup){
            fitness[index]+=c.calcXY()/((double)totalfitness);
            index++;
        }
        //计算累加适应度
        for(int i=1;i<fitness.length;i++){
            fitness[i]=fitness[i-1]+fitness[i];
        }
        //轮盘选择
        for(int i=0;i<sonGroupsize;i++){
            Random rd=new Random();
            double probability=rd.nextDouble();//概率
            int choose;
            for(choose=1;choose<fitness.length-1;choose++){
                if(probability<fitness[choose]){
                    break;//跳出循环
                }
            }
            sonGroup.add(new geneticalgorithm(fathargroup.get(choose).getGene()));
        }
        return sonGroup;
    }
    //交叉计算
    public static ArrayList<geneticalgorithm> crossover(ArrayList<geneticalgorithm> fathargroup,double probaility){
        ArrayList<geneticalgorithm> sonGroup= new ArrayList<geneticalgorithm>();//下一代
        sonGroup.addAll(fathargroup);//追加父类对象
        Random rd=new Random();
        for(int k=0;k<fathargroup.size()/2;k++){
            if(probaility>rd.nextDouble()){
                int i=0;
                int j=0;
                do{
                    i=rd.nextInt(fathargroup.size());
                    j=rd.nextInt(fathargroup.size());
                }while(i==j);
                //i!=j
                int pos=rd.nextInt(GENG_LENGTH);
                String parent1=fathargroup.get(i).getGene();
                String parent2=fathargroup.get(j).getGene();

                String son1=parent1.substring(0,pos)+parent2.substring(pos);
                String son2=parent2.substring(0,pos)+parent1.substring(pos);
                //基因的交叉
                sonGroup.add(new geneticalgorithm(son1));
                sonGroup.add(new geneticalgorithm(son2));
            }
        }
        return sonGroup;
    }
    //变异计算
    public static void mutation(ArrayList<geneticalgorithm> fathargroup,double probaility){
        Random rd=new Random();
        //抓取最优
        geneticalgorithm bestone=best(fathargroup);
        fathargroup.add(new geneticalgorithm(bestone.getGene()));//插入最优
        for(geneticalgorithm c:fathargroup){
            String newGene=c.getGene();//抓字符串
            for(int i=0;i<newGene.length();i++){
                if(probaility>rd.nextDouble()){
                    String newchar = newGene.charAt(i)=='0'?"1":"0";
                    newGene=newGene.substring(0,i)+newchar+newGene.substring(i+1);//变异

                }
            }
            c.selfinit(newGene);//自我变异
        }
    }
    //评估最优
    public static geneticalgorithm best(ArrayList<geneticalgorithm> group){
        geneticalgorithm bestone=group.get(0);
        for(geneticalgorithm gg:group){
            if(gg.calcXY()>bestone.calcXY()){
                bestone=gg;
            }
        }
        return bestone;
    }
    public static void main(String []args){
        final int GroupSize=20;//
        final double C_P=0.6;
        final double M_P=0.01;
        ArrayList<geneticalgorithm> group=geneticalgorithm.initGroup(GroupSize);

        geneticalgorithm thebest;
        do{
            group=geneticalgorithm.crossover(group,C_P);
            geneticalgorithm.mutation(group,M_P);
            group=geneticalgorithm.seletor(group,GroupSize);
            thebest=geneticalgorithm.best(group);
            System.out.println(thebest.calcXY());
        }while(thebest.calcXY()<32258);
    }
}
