package zhang.com.heimaoChengxuyuan.HuiSu;

public class zz {
    public int n;//集装箱数量
    public int []w;//集装箱的重量
    public int c;//第一艘船
    public int cw;//当前载重量
    public int bestw;//当前最优载重
    public int r;//剩余的集装箱
    public int []x;//当前解
    public int[]bestx;//保存最优解
    public void backtrace(int i){
        //叶子节点
        if(i>n-1){//i的值，叶子节点+1
            if(cw>bestw){
                for(int j=0;j<n;j++){
                    bestx[j]=x[j];
                    bestw=cw;//循环节点保存最优
                }
                return;
            }

        }
        r-=w[i];//减去权重
        //左子树
        if(cw+w[i]<c){
            x[i]=1;
            cw+=w[i];
            backtrace(i+1);
            cw-=w[i];
        }
        //右子树
        if(cw+r>bestw){
            x[i]=0;
            backtrace(i+1);
        }
        r+=w[i];
    }

    public static void main(String []args){
        zz zzwt = new zz();
        zzwt.n=10;
        zzwt.w=new int[zzwt.n];
        zzwt.x=new int[zzwt.n];
        zzwt.bestx=new int[zzwt.n];
        for(int i=0;i<zzwt.n;i++){
            zzwt.w[i]=(int)(100*Math.random());
            System.out.print("w="+zzwt.w[i]);
        }
        System.out.println();
        zzwt.c=400;
        for(int i=0;i<zzwt.n;i++){
            zzwt.r+=zzwt.w[i];//装载
        }
        zzwt.backtrace(0);
        for(int i=0;i<zzwt.n;i++){
            System.out.print(zzwt.bestx[i]+" ");
        }
        System.out.println();
        System.out.println("最优解"+zzwt.bestw);
    }
}
