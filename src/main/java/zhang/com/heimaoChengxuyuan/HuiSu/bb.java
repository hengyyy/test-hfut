package zhang.com.heimaoChengxuyuan.HuiSu;

public class bb {
    public int n=5;//物品数量
    public int capacity=10;//容量
    public int[]weight={2,6,4,1,5};//重量
    public double[]value={6,9,6,1,4};//价格
    int maxvalue=0;
    int tempvalue;//临时值
    int tempweight;//临时权重
    int []way= new int[n];//选择
    int []bestway=new int[n];

    public double Bound(int k){//获取最大价值
        double maxleft=tempvalue;
        int leftweight=capacity-tempweight;
        //尽力按照重量价值次序装剩下的
        while(k<=n-1&&leftweight>weight[k]){
            leftweight-=weight[k];
            maxleft+=value[k];
            k++;
        }
        if(k<=n-1){
            maxleft+=value[k]/weight[k]*leftweight;
        }
        return maxleft;
    }

    public void BackTrack(int t){
        if(t>n-1){//根节点
            if(tempvalue>maxvalue){
                maxvalue=tempvalue;//保存最大价值
                for(int i=0;i<n;i++){
                    bestway[i]=way[i];//保存路径
                }
            }
            return;
        }
        //搜索左边节点
        if(tempweight+weight[t]<=capacity){
            tempweight+=weight[t];
            tempvalue+=value[t];
            way[t]=1;
            BackTrack(t+1);
            tempweight-=weight[t];
            tempvalue-=value[t];
            way[t]=0;
        }
        //搜索右
        if(Bound(t+1)>=maxvalue){
            BackTrack(t+1);
        }
    }

    public static void main(String []args){
        bb bg = new bb();
        bg.BackTrack(0);
        System.out.println(bg.maxvalue);
        for(int i:bg.bestway){
            System.out.print(i+" ");
        }
    }
}
