package zhang.com.Z_NiuKe.NiuKe;

class A {
    public A() {
        System.out.println("class A");  //4
    }
    { System.out.println("I'm A class"); }  //3
    static { System.out.println("class A static"); }  // 1  *
}
public class B extends A {
    public B() {
        System.out.println("class B"); //6
    }
    { System.out.println("I'm B class"); }  // 5
    static { System.out.println("class B static"); }  //2   *

    public static void main(String[] args) {
        new B();
    }}
