package zhang.com.shangji.finalWorks.Huffman2;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class HuffmanTree {

    public String decompress(String fileName){
        DataInputStream in = null;
        String srcContent = "";
        try{
            in = new DataInputStream(new FileInputStream(new File(fileName)));//读取码表的信息，构建出码表
            Map<String,String> map = readCodes(in); //2 读回具体的数据内容

            byte[] datas = this.readDatas(in);
            //3 把读回的字节还原成对应的整型数据
            int[] dataInts = this.bytes2IntArray(datas);
            //int[] dataInts = this.readDatas(in);
            //根据码表，把内容组成的哈夫曼编码，依次转换回原始的字符，从而得到原始的内容
            srcContent = this.huffman2Char(map, dataInts);
        }catch(Exception err){
            err.printStackTrace();
        }finally{
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return srcContent;
    }
    //读取码表
    //key对应哈夫曼编码   value对应字符
    private Map<String,String> readCodes(DataInputStream in)throws Exception{
        Map<String,String> map = new HashMap<String,String>();
        //读回编码的个数
        int codeNum = in.readInt();    //从输入流in中读取一个整数codeNum，表示要读取的编码个数    ------------readInt()-----------

        for(int i=0;i<codeNum;i++){      //读回每个字符、编码长度、haffuman编码
            char codeChar = in.readChar();
            int codeLen = in.readInt();
            String code = "";
            char[] cs = new char[codeLen];
            for(int j=0;j<cs.length;j++){   //时间复杂度有点大
                code += in.readChar();
            }
            map.put(code, ""+codeChar);      //value是字符
        }
        return map;//获取哈夫曼码表
    }
    //读取数据部分
    private byte[] readDatas(DataInputStream in)throws Exception{
        int dataByteNum =  in.readInt();
        byte[] bs = new byte[dataByteNum];
        for(int i=0;i<dataByteNum;i++){
            bs[i] = in.readByte();
        }
        return bs;
    }
    //把这些byte转换成写int型
    private int[] bytes2IntArray(byte[] datas){
        int[] as = new int[datas.length];
        for(int i=0;i<datas.length;i++){
            if(datas[i] >= 0){
                as[i] = Math.abs(datas[i]);
            }else{
                as[i] = datas[i] + 256;
            }
        }
        return as;
    }
    //进一步转换成原始的字符
    private String huffman2Char(Map<String,String> map,int[] dataInts){
        StringBuffer buffer = new StringBuffer();
        String str = this.int2BinaryString(dataInts,map);


//        while(str.length()>0){              //根据哈夫曼编码，把文本转换为初始文本   代码优化

//                if(str.startsWith(code)){
//                    buffer.append(map.get(code));
//                    str = str.substring(code.length());//从字符串 str 中截取从指定位置 code.Length() 开始到字符串末尾的子字符串
//                    break;
//                }

        //}
        return str;
        //return buffer.toString();
    }
    //把int值转换回对应的二进制编码的字符串
    private String int2BinaryString(int[] as,Map<String,String> map){
        int len = as.length;
        int zeros = as[len-1];

        StringBuffer buffer = new StringBuffer();
        String[] ss = new String[len];
        String binaryStr = "";

        for(int i=0;i<len-1;i++){
            ss[i] = Integer.toBinaryString(as[i]);
            ss[i] = this.addZero(ss[i]);
            if(i==len-2){
                ss[i]=ss[i].substring(0,8-zeros);
            }
            binaryStr += ss[i];
            for(String code : map.keySet()){
                if(binaryStr.startsWith(code)){
                    buffer.append(map.get(code));
                    binaryStr = binaryStr.substring(code.length());//从字符串 str 中截取从指定位置 code.Length() 开始到字符串末尾的子字符串

                }
            }

        }

                while(binaryStr.length()>0){              //根据哈夫曼编码，把文本转换为初始文本   代码优化
        for(String code : map.keySet()){
                if(binaryStr.startsWith(code)){
                    buffer.append(map.get(code));
                    binaryStr = binaryStr.substring(code.length());//从字符串 str 中截取从指定位置 code.Length() 开始到字符串末尾的子字符串
                    break;
                }
        }
        }

        return buffer.toString();
    }
    //给每个二进制字符串补足成8位
    private String addZero(String str){
        if(str.length()<8){
            int zeroNum = 8-str.length();
            for(int i=0;i<zeroNum;i++){
                str = "0"+str;
            }
        }
        return str;
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void compress(String str,String outFile){            //str读取的内容     outFile输出的内容
        HuffmanPriorityQueue queue = this.statistics(str);      //统计字符出现的次数,通过优先队列构建哈夫曼树的节点

        HuffmanNode tree = this.buidHuffmanTree(queue);         //构建哈夫曼树
        //3 编码  对哈夫曼树的左边标记0，右边标记1，就可以的到字符的哈夫曼编码
        Map<String,String> map = new HashMap<String,String>();      //单个字符的哈夫曼编码
        this.buildHuffmanCode(map, tree, "");//将每个字符的哈夫曼编码存入到map集合中

        //map.forEach((key, value) -> System.out.println("Key: " + key + ", Value: " + value));
        //4 输出：把编码序列输出，这就是压缩后的数据
        this.outData(str,map,outFile);
    }
    //统计内容中字符出现的次数，并构建权重的优先级队列
    private HuffmanPriorityQueue statistics(String str){
        /**统计次数*/
        Map<Character,Integer> map = new HashMap<Character,Integer>();  //key :Character  value:Integer
        char[] cs = str.toCharArray();

        for(char c : cs){
            Object obj = map.get(c);//get() 方法获取指定 key 对应对 value
            if(obj==null){
                map.put(c, 1);//put() 方法添加 key 和 value
            }else{
                map.put(c, ((Integer)obj)+1);//
            }
        }//完成词频统计
        /**构建优先级队列*/
        HuffmanPriorityQueue queue = new HuffmanPriorityQueue(map.size());
        for(char c : map.keySet()){//- keySet() 方法返回映射中所有 key 组成的 Set 视图
            HuffmanNode node = new HuffmanNode(c, map.get(c));//构建节点
            queue.insert(node);//排序
        }
       // map.forEach((key, value) -> System.out.println("Key: " + key + ", Value: " + value));//------------------------------
        return queue;
    }
    /**构建哈夫曼树*/
    private HuffmanNode buidHuffmanTree(HuffmanPriorityQueue queue){
        while(queue.size() > 1){
            /**先取出两个权重最小的对象*/
            HuffmanNode n1 = queue.remove();
            HuffmanNode n2 = queue.remove();
            /**构建两个对象的父节点*/
            HuffmanNode n3 = new HuffmanNode((char)0,n1.getCount()+n2.getCount());
            n3.setLeftChild(n1);
            n3.setRightChild(n2);
            queue.insert(n3); //把父节点加入队列中
        }
        return queue.peekFront();
    }
    //根据huffman树生成对应的哈夫曼码 ，字符对应的哈夫曼码值都在叶子节点上，碰到左子树加0,右子树加1
    private void buildHuffmanCode(Map<String,String> map, HuffmanNode tree, String zeroOrOneStr){//递归得到叶子节点对应的哈夫曼编码
        if(tree.getLeftChild() == null && tree.getRightChild()==null){  //树没有子节点
            map.put(""+tree.getC(), zeroOrOneStr);//map的方法put()
        }
        //有左子节点
        if(tree.getLeftChild()!=null){
            this.buildHuffmanCode(map, tree.getLeftChild(), zeroOrOneStr+"0");
        }
        //有右子节点
        if(tree.getRightChild()!=null){
            this.buildHuffmanCode(map, tree.getRightChild(), zeroOrOneStr+"1");
        }
    }
    //输出数据，str原始数据  map哈弗曼编码  outFile输出文件的路径和文件名
    private void outData(String str,Map<String,String> map,String outFileName){     //          !!!!!!!!!!!!!!!!!
        File outFile = new File(outFileName);//File file = new File(String pathName);//参数可以传目录路径，或者文件路径（相对和绝对均可）。
        DataOutputStream os = null;//数据输出流允许应用程序以与机器无关方式将Java基本数据类型写到底层输出流。
        try{
            os = new DataOutputStream(new FileOutputStream(outFile));//输出二进制文件 ， 字节输出流
            this.outCodes(os, map);         //输出哈夫曼编码表（解码也要用到）
            String dataHuffmanCode = this.source2HumanStr(str, map);//输出源内容的每个字符对应的huffman编码
            this.outDataHuffmanCode(os, dataHuffmanCode);           //完成压缩
        }catch(Exception err){
            err.printStackTrace();
        }finally{
            try {
                os.close();                     //输入的文件存储的是二进制文件
            } catch (IOException e) {           //压缩的文本是二进制
                e.printStackTrace();
            }
        }
    }
    //输出哈夫曼编码表（outData中的子方法）
    private void outCodes(DataOutputStream os,Map<String,String> map)throws Exception{
        //1 输出码的个数
        System.out.println("压缩中，请稍等...");
        os.writeInt(map.size());//将一个int值写入输出流  size() 方法用于计算 hashMap 中键/值对的数量          -------writeInt------
        for(String key : map.keySet()){
            os.writeChar(key.charAt(0));//输出每个字符以及编码的长度
            os.writeInt(map.get(key).length());
            os.writeChars(map.get(key)); //输出每个字符对应的huffman编码
        }
    }
    //把原始的内容转换成为哈夫曼编码串
    private String source2HumanStr(String str,Map<String,String> map){
        StringBuffer buffer = new StringBuffer();
        char[] cs = str.toCharArray();
        for(char c : cs){
            buffer.append(map.get(""+c));
        }
        //System.out.println("压缩后的哈夫曼编码："+buffer.toString());//------------------------------------------------------------------
        return buffer.toString();
    }
    //输出哈夫曼的编码   回来的时候读成byte数组  哈夫曼二进制编码，转换成为数组
    private void outDataHuffmanCode(DataOutputStream os,String dataHuffmanCode)throws Exception{
        byte[] bs = this.string2ByteArrays(dataHuffmanCode);//把这个huffman编码串转换成对应的byte[]
        // 2 输出byte数组个数
        os.writeInt(bs.length);
        // 3 输出byte数组
        os.write(bs);
        System.out.println("压缩完成");
    }
    //把哈弗曼编码转换成真正的二进制数组
    private byte[] string2ByteArrays(String dataHuffmanCode){
        byte[] retBytes = null;
        char[] cs = dataHuffmanCode.toCharArray();
        int len = cs.length;
        int lenByte = 0;
        //判断整个串的长度是否能被8整除
        if(len % 8 == 0){ //能被8整除
            lenByte = len/8 + 1;
            retBytes = new byte[lenByte];
            for(int i=0;i<lenByte-1;i++){
                String s = "";
                for(int j=i*8;j<(i+1)*8;j++){
                    s += cs[j];
                }
                retBytes[i] = this.chars2byte(s);
            }
            //补0
            retBytes[lenByte - 1] = 0;
        }else{
            //不能被8整除，往后补0
            lenByte = len/8 + 2;
            retBytes = new byte[lenByte];
            int zeroNum = 8 - len % 8;
            //补0
            for(int i=0;i<zeroNum;i++){
                dataHuffmanCode +="0";
            }
            //重新计算char数组
            cs = dataHuffmanCode.toCharArray();
            for(int i=0;i<lenByte -1;i++){
                String s = "";
                for(int j=i*8;j<(i+1)*8;j++){
                    s += cs[j];
                }
                retBytes[i] = this.chars2byte(s);
            }
            retBytes[lenByte - 1] = (byte)zeroNum;
        }
        return retBytes;
    }
    //把一个字符串转换成一个byte类型的数据
    private byte chars2byte(String s){
        byte ret = 0;
        char[] cs = s.toCharArray();
        for(int i=0;i<cs.length;i++){
            byte tempB = (byte)(Byte.parseByte(""+cs[i])*Math.pow(2,cs.length-i-1));//---计算二进制的真正数值
            ret = (byte)(ret+tempB);
        }
        return ret;
    }

    public static String readFile(String fileName) {
        StringBuilder buffer = new StringBuilder();
        DataInputStream in = null;
        try {
            FileInputStream fis = new FileInputStream(new File(fileName));
            in = new DataInputStream(new BufferedInputStream(fis));
            InputStreamReader isr = new InputStreamReader(in, "UTF-16LE");
            BufferedReader br = new BufferedReader(isr);
            String tempStr = "";
            while ((tempStr = br.readLine()) != null) {
                buffer.append(tempStr).append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return buffer.toString();
    }
    public static void writeStringToFile(String content, String fileName) {
        try {
            // 创建一个FileWriter对象，用于写入字符到指定的文件
            FileWriter fileWriter = new FileWriter(fileName);
            // 创建一个BufferedWriter对象，用于缓冲字符，提高写入性能
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            // 将字符串写入到文件中
            bufferedWriter.write(content);
            // 关闭BufferedWriter对象，释放资源
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        HuffmanTree t = new HuffmanTree();
        long currentTimeMillis1 = System.currentTimeMillis();
        t.compress(t.readFile("三国演义.txt"), "三国压缩文件2.txt");
        long currentTimeMillis2 = System.currentTimeMillis();
        String s = t.decompress("三国压缩文件.txt");
        String fileName = "三国解压文件2.txt";
        long currentTimeMillis3 = System.currentTimeMillis();
        writeStringToFile(s,fileName);
        //System.out.println("s=="+s);

        File file = new File("三国演义.txt");
        double fileSize = file.length();
        System.out.println("文件的大小为：" + fileSize + "字节");

        File file2 = new File("三国.txt");
        double fileSize2 = file2.length();
        System.out.println("压缩后的文件的大小为：" + fileSize2 + "字节");

        double percenta =fileSize2/fileSize;
        System.out.printf("压缩的百分比==%.2f",percenta*100);
        System.out.println("%\n");
        System.out.println("压缩所用时间："+(currentTimeMillis2-currentTimeMillis1)+" ms");
        System.out.println("解压所用时间："+(currentTimeMillis3-currentTimeMillis2)+" ms");
    }
}