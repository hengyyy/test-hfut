package zhang.com.shangji.com.test3.stack;

public class PostInfix{
    public String doTransfer(String str){
        int yes=0;
        StringBuffer buffer = new StringBuffer();
        MyStack ms = new MyStack(20);
        //把字符串转换成字符串数组
        String[] cs=str.split(" ");
        //对每一个字符串进行判断并执行相应的操作
        for(int i=0;i<cs.length;i++){
            String c=cs[i];
            //如果是操作符，就要分级别操作
            if(c.equals("+")||c.equals("-")){

                if(isTrue(cs[i-1]))
                doOperation(ms,buffer,c,1);
                else {
                    System.out.println("输入不合法");
                    yes=1;
                    break;
                }

            }
            else if(c.equals("*")||c.equals("/")){

                if(isTrue(cs[i-1]))
                doOperation(ms,buffer,c,2);
                else {
                    System.out.println("输入不合法");
                    yes=1;
                    break;
                }
            }
            //如果是左括号压栈
            else if(c.equals("(")){
                ms.push(c);
            }
            //如果是右括号，贪占到输出中，直到遇到未知
            else if(c.equals(")")){
                doRightBracket(ms,buffer);
            }
            //如果是操作数，直接加入到输出
            else{
                buffer.append(c+" ");
            }
        }
        while(!ms.isEmpty()){
            buffer.append(ms.pop()+" ");
        }
        if(yes==0)
        return buffer.toString();
        else return "";
    }
    private <T>void doOperation(MyStack ms,StringBuffer buffer, T c,int level){
        //依次从栈顶获取一个值
        while(!ms.isEmpty()){
            T topC= (T) ms.pop();
            //用这个值跟传入的数据进行比较
            //如果栈顶元素是（，不动，就是把它压回去
            if(topC.equals("(")){
                ms.push(topC);
                break;
            }else{
                //首先获取到栈顶元素所对应的优先级别
                int topLevel=0;
                if(topC.equals("+")||topC.equals("-")){
                    topLevel=1;
                }
                else{
                    topLevel=2;
                }
                //如果栈顶的数据的优先级别大于等于传入的数据的级别
                if(topLevel>=level){
                    buffer.append(topC+" ");
                }else{
                    ms.push(topC);
                    break;
                }
                //如果栈顶的数据优先级小于传入的数据的级别，不动
            }
        }
        //找到位置后，把传入的操作符压入
        ms.push(c);
    }

    //判断是否合规
    public boolean isTrue(String c){

            if (c.equals("+") || c.equals("/") || c.equals("-") || c.equals("*")){
                return false;
        }else
            return true;

    }
    private void doRightBracket(MyStack ms,StringBuffer buffer){
        //从栈中弹出数据，输出到后缀表达式中
        while(!ms.isEmpty()){
            Object topC=ms.pop();
            //直到遇到”“（为止
            if(topC.equals("(")){
                break;
            }else{
                buffer.append(topC+" ");
            }
        }
    }
    public static void main(String []args){
        PostInfix t= new PostInfix();
        String ret = t.doTransfer("2.5 * 50 + 6 - 5 * ( 8 - 3 )");

        System.out.println("ret==="+ret);
    }
}