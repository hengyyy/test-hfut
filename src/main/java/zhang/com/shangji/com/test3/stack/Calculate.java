package zhang.com.shangji.com.test3.stack;

public class Calculate {
    /**
     *
     * 使用类型转换判断是否为数字
     */
    public static boolean isDigit(String str) {
        try {
            //类型转换
            Double.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public <T> Object calculate(String str){
        MyStack ms=new MyStack(20);
        String[] cs = str.split(" ");
        for(int i =0;i<cs.length;i++){
            String c=cs[i];
            if(isDigit(c)){
               // int number=c.compareTo("0");//String.valueOf(number)
                ms.push(c);//如果cs[i]是数字，再push回去
            }
            else{
                String num2=(String)ms.pop();
                String num1=(String)ms.pop();
                double n1=Double.valueOf(num1);
                double n2=Double.valueOf(num2);
                double temp=0;
                if(c.equals("+")){
                    temp=(n1+n2);
                }else if(c.equals("-")){
                    temp=n1-n2;
                }else if(c.equals("*")){
                    temp=n1*n2;
                }
                else if(c.equals("/")){
                    temp=n1/n2;
                }
                ms.push(String.valueOf(temp));
            }
        }
        return ms.pop();
    }
    public static void main(String []args){
        Calculate t=new Calculate();


        try{
            String str = new PostInfix().doTransfer("2.0 * + 5 + 6.3 - 5 * ( 8 - 3 )");/**运算符左右需要有一个空格*/
            String ret=(String)t.calculate(str);
            System.out.println("后缀表达式："+str);
            System.out.println("计算结果："+ret);
        }catch(Exception e){
            //System.out.println("输入不合法");
        }




    }
}