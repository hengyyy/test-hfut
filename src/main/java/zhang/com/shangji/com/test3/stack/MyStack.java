package zhang.com.shangji.com.test3.stack;

/**
 * 泛型真恶心，不知道什么时候能够强制转型。Object是什么东西！！！
 * @param <T>
 */
public class MyStack<T> {
    private T[] datas;
    private int topIndex=-1;
    public MyStack(int length) {
        datas =(T[])new Object[length];
    }
    public void push(T d){
        topIndex++;
        datas[topIndex]=d;
    }
    public T pop(){
        T ret =datas[topIndex];
        topIndex--;
        return ret;
    }
    public T peek(){
        return datas[topIndex];
    }
    public boolean isEmpty(){
        return topIndex==-1;
    }
    public boolean isFull(){
        return topIndex==(datas.length-1);
    }

    public void printStack(){
        for(int i=0;i<topIndex;i++){
            System.out.println(datas[i]);
        }
    }
}
