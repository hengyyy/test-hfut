package zhang.com.shangji.com.test8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class findAncestor {

        final int INF = 0x3f3f3f3f;
        int n;
        int e;
        int edges[][];
        List<List<Integer>> L = new ArrayList<>();

        public findAncestor(){

        }
        public void createMatGraph(int arr[][],int n ,int e){
            this.n=n;
            this.e=e;
            this.edges=arr;
        }

        public void disMatGraph(){
            for(int i=0;i<edges.length;i++){
                for(int j=0;j<edges[0].length;j++){
                    if(edges[i][j]==INF)System.out.printf("%4s","∞");
                    else System.out.printf("%4d",edges[i][j]);
                }
                System.out.println();
            }
        }

        public void floyd()    //弗洛伊德算法
        {
            int[][] A = edges;         //建立A数组
            int[][] path = new int[n][n];      //建立path数组
            for (int i = 0; i < n; i++)        //给数组A和path置初值即求A-1[i][j]
                for (int j = 0; j < n; j++) {
                    if (i != j && edges[i][j] < INF) path[i][j] = i;            //i和j顶点之间有边时
                    else path[i][j] = -1;            //i和j顶点之间没有边时
                }
            for (int k = 0; k < n; k++)            //求Ak[i][j]
            {
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < n; j++)
                        if (A[i][j] > A[i][k] + A[k][j]) {
                            A[i][j] = A[i][k] + A[k][j];
                            path[i][j] = path[k][j];    //修改最短路径
                        }
            }
            disFloydPath(A, path);            //生成最短路径和长度
        }

        public void disFloydPath(int[][] A, int[][] path) {
            int arr[][]=new int[n][n];
            for(int i=0;i<n;i++){
                Arrays.fill(arr[i],-1);
            }
            //输出所有的最短路径和长度
            int[] apath = new int[n];
            int d;            //apath[0..d]存放一条最短路径(逆向)
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++) {
                    if (A[i][j] != INF && i != j) {        //若顶点i和j之间存在路径
                        System.out.println("  顶点" + i + "到" + j +
                                "的最短路径长度:" + A[i][j]);
                        arr[j][i]=i;
                        //i 是 j的祖先
                        int k = path[i][j];
                        d = 0;
                        apath[d] = j;                       //路径上添加终点
                        while (k != -1 && k != i) {         //路径上添加中间点
                            d++;
                            apath[d] = k;                   //顶点k加入到路径中
                            k = path[i][k];
                        }
                        d++;
                        apath[d] = i;                       //路径上添加起点
                    }
                }

            for(int i=0;i<n;i++){
                List<Integer> l = new ArrayList<>();
                for(int j=0;j<n;j++){
                    if(arr[i][j]!=-1)l.add(arr[i][j]);
                }
                L.add(l);
            }
            System.out.println(L);

        }
        /**
         * 说明：
         * 1 首先，你要自己创建邻接矩阵，事先声明图的顶点个数和边的个数。然后对类中的属性进行赋值。
         * 2 把邻接矩阵打印出来
         * 3 floyd算法设计：
         *    floyd算法的实现需要三个步骤：
         *      ①初始化       建立A[][]数组  path[][]数组
         *      ②给A[][] path[][]赋初始值
         *      ③ 更新A[][]
         *      ④打印各个源点到其他点的最短距离disFloydpath()
         *
         * @param args
         */

        public static void main(String []args){
           findAncestor g2 = new findAncestor();
            int n=8;

            int[][] a2 = { {0,3},
                           {0,4},
                           {1,3},
                           {2,4},
                           {2,7},
                           {3,5},
                           {3,6},
                           {3,7},
                           {4,6}};
            int e=a2.length;
            int aa[][]=new int[n][n];
            for(int i=0;i<n;i++){
                Arrays.fill(aa[i],0x3f3f3f3f);
            }
            for(int i=0;i<n;i++) aa[i][i]=0;
            for(int i=0;i<a2.length;i++){
                for(int j=0;j<a2[0].length;j++){
                    aa[a2[i][0]][a2[i][1]]=1;
                }
            }
            g2.createMatGraph(aa,n,e);
            g2.disMatGraph();
            g2.floyd();
        }
    }


