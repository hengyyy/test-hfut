package zhang.com.shangji.com.test8;


import java.util.Arrays;

public class InternetLag {//副本
    final int INF =0x3f3f3f3f;
    int n;//顶点个数
    int e;//边的条数
    int edges[][];//邻接矩阵
    public InternetLag(){
    }
    public void createMatGraph(int arr[][],int n,int e){
        this.n=n;
        this.e=e;
        this.edges=arr;
    }

    public void dispMatGraph(){
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(edges[i][j]==INF)System.out.printf("%3s","∞");
                else System.out.printf("%3d",edges[i][j]);
            }
            System.out.println();
        }
    }
    public static boolean isFindWay(int S[]){
        for(int i=0;i<S.length;i++){
            if(S[i]!=1){
                System.out.println("不能使所有节点都有信号！");
                return false;
            }
        }
        return true;
    }

    private static void dispAllPath(int[] dist, int[] path, int[] S, int v, int n) {
        int[] apath = new int[n];
        int d;                             //path[0..d]存放一条最短路径(逆向)
        for (int i = 0; i < n; i++) {       //循环输出从顶点v到i的路径

            /**
             * 判断是否存在S[i]!=1的情况
             * */
            if(!isFindWay(S)) return;

            if (S[i] == 1 && i != v) {
                Arrays.sort(dist);  //排序，便于找到最大的时间。
                d=0;
                apath[d]=i;
                int k=path[i];
                if(k==-1){
                    System.out.println("无路径");
                }
                else{
                    while(k!=v){
                        d++;
                        apath[d]=k;
                        k=path[k];
                    }
                    d++;
                    apath[d]=v;
                }
            }
        }
        for(int i=dist.length-1;i>=0;i--){
            if(dist[i]!=0x3f3f3f3f){
                System.out.println("所需要的时间："+dist[i]+"秒");/**得到最大值*/
                break;
            }

        }
    }

    //dijkstra算法设计
    public void dijkstra(int v){  //n 顶点数 v源点编号
        int dist[] = new int[n];                      //初始化
        int path[] = new int[n];
        int S[] = new int[n];
        for(int i=0;i<n;i++){
            S[i]=0;
            if(edges[v][i]!=INF)
            dist[i]=edges[v][i];
            else dist[i]=0x3f3f3f3f;
            if(edges[v][i]<INF)path[i]=v;
            else path[i]=-1;
        }
        S[v]=1;     //  将源点放入S中
        int mindis;
        int u=v;
        for(int i=0;i<n-1;i++){
            mindis=INF;
            for(int j=0;j<n;j++){
                if (S[j] == 0 && dist[j] < mindis) {
                    mindis = dist[j];
                    u=j;
                }
            }
            S[u]=1;
            for(int j=0;j<n;j++){
                if(S[j]==0){
                    if (edges[u][j] < INF && dist[u] + edges[u][j] < dist[j]) {
                        dist[j] = dist[u] + edges[u][j];
                        path[j]=u;
                    }
                }
            }
        }
        dispAllPath(dist,path,S,v,n);
    }
    public static void main(String []args) {
        InternetLag g1 = new InternetLag();
        int k = 1;//k是节点的编号
        int n1 = 5;//4个顶点
        int times[][] = {{1, 0, 1}, {1, 2, 1}, {2, 3, 1},{3,4,11}};
        int e1 = times.length;
        int arr1[][] = new int[n1][n1];  //1 把数组转换为邻接矩阵
        {
        int inf = 0x3f3f3f3f;
        //填充二维数组
        for (int i = 0; i < n1; i++) {
            Arrays.fill(arr1[i], inf);
        }
        for (int i = 0; i < times.length; i++) {
            for (int j = 0; j < times[0].length; j++) {
                //只需要得到times[i][0]和times[i][1]
                arr1[times[i][0]][times[i][1]] = times[i][2];
                arr1[i][i] = 0;
            }
        }
        g1.createMatGraph(arr1, n1, e1);
        g1.dispMatGraph();
    }
        g1.dijkstra(k);
    }
}