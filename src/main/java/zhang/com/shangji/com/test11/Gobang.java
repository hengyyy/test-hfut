package zhang.com.shangji.com.test11;

public class Gobang {
    //定义棋盘，（无子状态为0）机器黑子(状态棋盘为1)，玩家白子（状态棋盘为2）
    int BOARD_SIZE = 15;//棋盘格数
    private String[][] board;//存放UI棋盘
    int[][] boardStatus;//存放棋盘状态
    char c1 = '○', c2 = '●';//黑白棋子  2● , 1○

    public Gobang(int BOARD_SIZE) {
        this.BOARD_SIZE = BOARD_SIZE;
        System.out.println("电脑执○，玩家执●");
    }


    //判断是否五子连环（遍历每一个点的八个方向，是否有五子相连）
    public boolean isFive() {
        int x, y, lenth;
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (boardStatus[i][j] != 0) {
                    x = i;
                    y = j;
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; --x, --y, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;

                    //上
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; --x, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;

                    //右上
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; ++y, --x, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                    //右
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; ++y, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                    //右下
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; ++y, ++x, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                    //下
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; ++x, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                    //左下
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; ++x, --y, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                    //左
                    for (x = i, y = j, lenth = 1; lenth < 6 && x > -1 && y > -1 && y < BOARD_SIZE && x < BOARD_SIZE; --y, lenth++)
                        if (boardStatus[x][y] != boardStatus[i][j]) break;
                    if (lenth == 6) return true;
                }
            }
        }
        return false;
    }

    //给棋盘增加一颗棋
    public void setBoard(int user, int x, int y) {

        if (user == 0) {
            board[x][y] = "" + c1 + " ";
            boardStatus[x][y] = 1;
        } else {
            board[x][y] = "" + c2 + " ";
            boardStatus[x][y] = 2;
        }
    }

    //初始化棋盘数组
    public void initBoard() {
        //初始化棋盘数组，
        board = new String[BOARD_SIZE][BOARD_SIZE];
        boardStatus = new int[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = "+ ";
                boardStatus[i][j] = 0;
            }
        }

    }

    //输出棋盘
    public void printBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j]);
            }
            System.out.printf("\t %d\n",i+1);
        }
    }

    public void printBoardStatus() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(boardStatus[i][j]);
            }
            System.out.println();
        }
    }

    //是否可下棋
    public int isFill(int x, int y) {
        if (boardStatus[x][y] == 0) {
            return 0;
        } else return 1;
    }


}
