//package shangji.com.test11;
//
//
//import java.util.Arrays;
//import java.util.LinkedHashMap;
//import java.util.Map;
//import java.util.Scanner;
//
//import static java.util.Arrays.binarySearch;
//
//
//class AIPlayer {
//
//    int level = 1;//默认随机下棋
//    private int[][] chessValue = new int[15][15];
//    static int BOARD_SIZE = 15;
//    private static final Map<String, Integer> SCORE = new LinkedHashMap<>();//哈希表，便于查找
//
//    private enum ChessModel {//棋盘模型
//        LIAN_WU(10000000, new String[]{"11111"}), // 连五  一千万
//        CHONG_SI(9000, new String[]{"11110", "01111", "10111", "11011", "11101"}),//冲四  9000
//        HUO_SI(1000000, new String[]{"011110"}),// 活四   一百万
//        HUO_SAN(10000, new String[]{"001110", "011100", "010110", "011010"}),// 活三   一万
//        HUO_ER(100, new String[]{"001100", "011000", "000110", "001010", "010100"}),// 活二
//        HUO_YI(80, new String[]{"010200", "002010", "020100", "001020", "201000", "000102", "000201"}),// 活一
//        MIAN_SAN(30, new String[]{"001112", "010112", "011012", "211100", "211010"}), // 眠三
//        MIAN_ER(10, new String[]{"011200", "001120", "002110", "021100", "110000", "000011", "000112", "211000"}),// 眠二
//        MIAN_YI(1, new String[]{"001200", "002100", "000210", "000120", "210000", "000012"});  // 眠一
//        final int score;//分数
//        final String[] values;//局势数组
//        ChessModel(int score, String[] values) {
//            this.score = score;
//            this.values = values;
//        }
//        @Override
//        public String toString() {
//            return Arrays.toString(this.values);
//        }}
//    static {
//        // 初始化棋型分数表，便于访存数据
//        for (ChessModel chessScore : ChessModel.values()) {
//            for (String value : chessScore.values) {
//                SCORE.put(value, chessScore.score);
//            }
//        }
//    }
//
//    public AIPlayer(int level, int BOARD_SIZE) {
//        this.level = level;
//        AIPlayer.BOARD_SIZE = BOARD_SIZE;
//    }
//
//    public int[] getPosition(int[][] boardStatus, int[] humanPosition) {
//        //获取位置信息
//        return switch (this.level) {
//            case 1 -> randomPosition();
//            case 2 -> lowerLevelPosition(boardStatus, humanPosition);
//            case 3 -> highLevelPosition(boardStatus, humanPosition);
//            default -> randomPosition();
//        };
//    }
//
//    private int[] lowerLevelPosition(int[][] boardStatus, int[] humanPosition) {
//        int score = -10000000;
//        int[] bestPos = {0, 0};
//        if (humanPosition[0] == -1 && humanPosition[1] == -1) {//如果是第一步下棋
//            bestPos[0] = BOARD_SIZE / 2;
//            bestPos[1] = BOARD_SIZE / 2;
//            return bestPos;
//        }
//        /* 遍历所有能下棋的点位，评估各个点位的分值，选择分值最大的点位 */
//        for (int i = 0; i < BOARD_SIZE; i++) {
//            for (int j = 0; j < BOARD_SIZE; j++) {
//                if (boardStatus[i][j] != 0) {
//                    // 该点已有棋子，跳过
//                    continue;
//                }
//                //TODO 评估该点AI得分
//                int val = lowerEvaluate(new int[]{i,j},boardStatus);
//                // 选择得分最高的点位
//                if (val > score) {score = val;// 最高分被刷新
//                    // 更新最佳点位
//                    bestPos[0] = i;bestPos[1] = j;}}}
//        return bestPos;}
//
//    //------------------------------------------------ 计算收益 ----------------------------------------------------------
//    public int value(String code){
//        Integer value;
//        int result=0;
//
//        /**现在我们得到了由这个点所组成的 六元组 和 五元组*/
//
//        for(int l =0 ;l<code.length()-5;l++){ //------------------------------六元组-------------------------------------
//            String temp=code.substring(l,l+6);
//            value = SCORE.get(temp);
//            if (value != null && value >result) {
//                result = value; // 权值累加
//            }
//            else {
//            StringBuffer buffer = new StringBuffer(temp);//--------翻转-----6元组正序不被包含，逆序可能被包含---------------
//            temp =buffer.reverse().toString();
//            value = SCORE.get(temp);
//                if (value != null && value>result) {
//                    result = value; // 权值累加
//                }
//            }
////            //------------------------------------------------------风险计算----------------------------------------------
////            String s ="";
////            for(int len=0;len<temp.length();len++){//---------------------------黑白交换----------------------------------
////                if(temp.charAt(len)==0) s+=0;
////                if(temp.charAt(len)==2)s=s+1;
////                else {
////                    if(temp.charAt(len)==1) s=s+2;
////                }
////            }
////            value2 = SCORE.get(s);
////            if (value2 != null && value2>result[1]) {
////                result[1] = value2; //
////            }
////            else{
////                StringBuffer buffer2 = new StringBuffer(s);//------------------------转换--------------------------------
////                s =buffer2.reverse().toString();
////                value2 = SCORE.get(s);
////                if (value2 != null && value2 > result[1]) {
////                    result[1] = value2; // 权值累加
////                }
////            }
//        }
//
//        for(int l =0 ;l<code.length()-4;l++){ //------------------------------五元组-------------------------------------
//            String temp=code.substring(l,l+5);
//            value = SCORE.get(temp);
//            if (value != null && value >result) {
//                result = value; // 权值累加
//            }else{
//                StringBuffer buffer = new StringBuffer(temp);
//                temp =buffer.reverse().toString();
//                value = SCORE.get(temp);
//                if (value != null &&value > result) {
//                    result = value; //
//                }
//            }
//            //------------------------------------------------------风险计算----------------------------------------------
////            String s ="";
////            for(int len=0;len<temp.length();len++){//---------------------------黑白交换----------------------------------
////                if(temp.charAt(len)==0) s+=0;
////                if(temp.charAt(len)==2)s=s+1;
////                else {
////                    if(temp.charAt(len)==1) s=s+2;
////                }
////            }
////            value2 = SCORE.get(s);
////            if (value2 != null && value2>result[1]) {
////                result[1] = value2; //
////            }
////            else{
////                StringBuffer buffer2 = new StringBuffer(s);//------------------------转换--------------------------------
////                s =buffer2.reverse().toString();
////                value2 = SCORE.get(s);
////                if (value2 != null && value2 > result[1]) {
////                    result[1] = value2; // 权值累加
////                }
////            }
//        }
//        return result;
//    }
//
//    public int value2(String code){   //------------------------------- 风险计算 -----------------------------------------
//        Integer value2;
//        int result=0;
//        String s ="";
//        for(int len=0;len<code.length();len++){ //黑白交换
//            if(code.charAt(len)=='0'){
//                s+=0;
//            }
//            if(code.charAt(len)=='2'){
//                s+=1;
//            }
//            if(code.charAt(len)=='1'){
//                s+=2;
//            }
//        }
//        /**现在我们得到了由这个点所组成的 六元组 和 五元组*/
//
//        for(int l =0 ;l<code.length()-5;l++){ //------------------------------六元组-------------------------------------
//            String temp=s.substring(l,l+6);
//
//            value2 = SCORE.get(temp);
//            if (value2 != null && value2>result) {
//                result = value2; //
//            }
//            else{
//                StringBuffer buffer2 = new StringBuffer(s);//------------------------转换--------------------------------
//                temp =buffer2.reverse().toString();
//                value2 = SCORE.get(temp);
//                if (value2 != null && value2 > result) {
//                    result = value2; //
//                }
//            }
//        }
//
//        for(int l =0 ;l<code.length()-4;l++){ //------------------------------五元组-------------------------------------
//            String temp=s.substring(l,l+5);
//
//            value2 = SCORE.get(temp);
//            if (value2 != null && value2>result) {
//                result = value2; //
//            }
//            else{
//                StringBuffer buffer2 = new StringBuffer(temp);//------------------------转换-----------------------------
//                temp =buffer2.reverse().toString();
//                value2 = SCORE.get(temp);
//                if (value2 != null && value2 > result) {
//                    result = value2; // 权值累加
//                }
//            }
//        }
//        return result;
//    }
//
//    //------------------------------------------------ lowerEvaluate方法 ------------------------------------------------
//
//    private int lowerEvaluate(int[] bestPos,int[][] boardStatus) {
//        int result1[] = new int[4];     //判断当前点的收益分数
//        int result2[] = new int[4];     //判断当前点的风险分数
//        int black =1 ;//黑一，白二。
//        int white =2;
//
//        int i = bestPos[0];
//        int j = bestPos[1];
//
//                    String code = "";
//                    String code2= "";           //记录白棋走该位置时的得分
// // ------------------- ----------------------------------  向下  -------------------------------------------------------
//        /**从当前坐标开始，上下各 取 4 个元素*/
//                    for (int k = i + 1; k < boardStatus.length; k++) {
//
//                        if (boardStatus[k][j] == 0) {
//                            code+=0;
//                        } else {//右一不是空
//                                if (boardStatus[k][j] == black) { // 下边是黑棋
//                                    code+=black;
//                            } else {                              // 下边是白棋
//                                    code+=white;
//                            }
//                        }
//                        if(code.length()==4)break;
//                    }
//                    code2 = white+code;
//                    code= black+code;
//        /**从当前坐标开始，向上 取 4 个元素*/
//                    // 向上
//                    for (int k = i - 1; k >= 0; k--) {
//                        if (boardStatus[k][j] == 0) {
//                            code=0+code;
//                            code2=0+code2;
//                        } else {
//                                if (boardStatus[k][j] == black) {
//                                code = black+code;
//                                    code2 = black+code2;
//                            } else {
//                                code = white+code;
//                                    code2 = white+code2;
//                            }
//                        }
//                        if(code.length()==9)break;
//                    }
//
//                    result1[0]=value(code);      //计算收益
//                    result2[0]=value2(code2);      //计算风险,得到长度为9的字符串，但是不知道哪一个是要走的白棋
//
////----------------------------------------------------------- 向右 ------------------------------------------------------
//        code = "";
//        code2 = "";
//                    for (int k = j + 1; k < boardStatus[i].length; k++) {
//                        if (boardStatus[i][k] == 0) {
//                            code+=0;
//                        } else {
//                                if (boardStatus[i][k] == black) {
//                                code += black; // 保存棋局
//                            } else {
//                                code += white; // 保存棋局
//                            }
//                        }
//                        if(code.length()==4)break;
//                    }
//
////----------------------------------------------------------- 向左 ------------------------------------------------------
//        code2=2+code;
//        code=1+code;
//
//
//                    for (int k = j - 1; k > 0; k--) {
//                        if (boardStatus[i][k] == 0) {
//                            code = 0+code;
//                            code2 = 0+code2;
//                        } else {
//                                if (boardStatus[i][k] == black) {
//                                code = black+code; // 保存棋局
//                                    code2 = black+code2; // 保存棋局
//                            } else {
//                                code = white+code; // 保存棋局
//                                    code2 = white+code2; // 保存棋局
//                            }
//                        }
//                        if(code.length()==9)break;
//                    }
//
//        result1[1]=value(code);
//        result2[1]=value2(code2);
//
////------------------------------------------------------- 左斜向上 ------------------------------------------------------
//        code="";
//        code2="";
//                    for (int k = 1; i-k >0&&j-k>0 ; k++) {
//                        if (boardStatus[i-k][j-k] == 0) {
//                            code+=0;
//                        } else {
//                                if (boardStatus[i-k][j-k] == black) {
//                                code += 1; // 保存棋局
//                            } else {
//                                code += 2; // 保存棋局
//                            }
//                        }
//                        if(code.length()==4)break;
//                    }
//        code2= 2+code;
//                    code= 1+code;
//
////-------------------------------------------------------- 右斜向下 ------------------------------------------------------
//
//                    for (int k = 1; i+k<boardStatus.length&&j+k<boardStatus[i].length ; k++) {
//                        if (boardStatus[i+k][j+k] == 0) {
//                            code = 0+code;
//                            code2 = 0+code2;
//                        } else {
//                                if (boardStatus[i+k][j+k] == black) {
//                                code = 1+code; // 保存棋局
//                                code2 = 1+code2; // 保存棋局
//                            } else {
//                                code = 2+code; // 保存棋局
//                                code2 = 2+code; // 保存棋局
//                            }
//                        }
//                        if(code.length()==9)break;
//                    }
//
//        result1[2]=value(code);
//        result2[2]=value2(code2);
//// ------------------------------------------------ 右斜向上-------------------------------------------------------------
//        code = "";
//        code2 = "";
//                    for (int k = 1; j-k>0&&i+k<boardStatus[i].length ; k++) {
//                        if (boardStatus[i+k][j-k] == 0) {
//                            code+=0;
//                        } else {
//                                if (boardStatus[i+k][j-k] == black) {
//                                code += 1; // 保存棋局
//                            } else {
//                                code += 2; // 保存棋局
//                            }
//                        }
//                        if(code.length()==4)break;
//                    }
//        code2=2+code2;
//                    code=1+code;
////--------------------------------------------------- 左斜向下 ----------------------------------------------------------
//                    for (int k = 1; j+k<boardStatus.length&&i-k>0; k++) {
//                        if (boardStatus[i-k][j+k] == 0) {
//                            code=0+code;
//                            code2=0+code2;
//                        } else {
//                                if (boardStatus[i-k][j+k] == black) {
//                                code = 1+code; // 保存棋局
//                                    code2 = 1+code2; // 保存棋局
//                            } else {
//                                code = 2+code;       // 保存棋局
//                                    code2 = 2+code2; // 保存棋局
//                            }
//                        }
//                        if(code.length()==9)break;
//                    }
//
//        result1[3]=value(code);
//        result2[3]=value2(code2); // 权值累加
//
//        int sum1=0;
//        for(int e:result1) sum1+=e;
//        int sum2=0;
//        for(int e:result2) if(e>sum2) {
//            sum2=e;
//        }
//
//        sum1=isDouble(result1,sum1);   //总收益
//        sum2=isDouble(result2,sum2);   //总风险，总风险 +总收益的值
//        //System.out.println("风险总值："+sum2);
//
//        return Math.max(sum1,sum2);
//    }
//    public int isDouble(int []result1,int sum1){
//        if(binarySearch(result1,9000)>0&&binarySearch(result1,10000)>0) return sum1=8*sum1;   //冲四活三
//        if(isTwo(result1,10000)) return sum1=6*sum1;   //两个以上活三
//        if(binarySearch(result1,9000)>0&&binarySearch(result1,100)>0) return sum1=4*sum1;     //冲四活二
//        if(binarySearch(result1,10000)>0&&binarySearch(result1,100)>0)return  sum1=2*sum1;    //活三活二
//        return sum1;
//    }
//    public boolean isTwo(int arr[],int target){
//        int count=0;
//        for(int i = 0;i<arr.length;i++){
//            if(arr[i]==target) count++;
//        }
//        if(count>1)
//        return true;
//        else return false;
//    }
//
//    private int[] highLevelPosition(int[][] boardStatus, int[] humanPosition) {
//        //TODO exp12 此处进行改进优化
//        int x = (int) ((BOARD_SIZE - 1) * Math.random());
//        int y = (int) ((BOARD_SIZE - 1) * Math.random());
//        return new int[]{x, y};
//    }
//
//    private int[] randomPosition() {//无脑下棋
//        int x = (int) ((BOARD_SIZE - 1) * Math.random());
//        int y = (int) ((BOARD_SIZE - 1) * Math.random());
//        return new int[]{x, y};
//    }
//
//}
//
//
//public class Game {
//    public static void main(String[] args) {
//        final int BOARD_SIZE = 15;
//        Gobang game = new Gobang(BOARD_SIZE);//初始化
//        game.initBoard();//初始化棋盘
//        game.printBoard();//输出棋盘
//        System.out.println("选择等级(1: 随机；2：入门；3：高级)：");
//        Scanner sc1 = new Scanner(System.in);
//        int level = sc1.nextInt();
//        System.out.println("你选择了等级：" + level);
//        AIPlayer aiPlayer = new AIPlayer(level, BOARD_SIZE);
//        int x, y;
//        int humanX = -1, humanY = -1;
//        while (true) {
//            for (int j = 0; ; )//随机生成 x,y 并判断是否可以下棋
//            {
//                int[] position = aiPlayer.getPosition(game.boardStatus, new int[]{humanX, humanY});
//                System.out.println(humanX+":"+humanY);
//                System.out.println("1 2 3 4 5 6 7 8 9 ⑩ ① ② ③ ④ ⑤");
//                x = position[0];
//                y = position[1];
//                if (game.isFill(x, y) == 1) continue;
//                else break;
//            }
//            game.setBoard(0, x, y);
//            game.printBoard();
//            if (game.isFive()) {
//                System.out.println("电脑获胜了");
//                break;
//            }
//            System.out.println("输入落子点(x y)：");
//
//            for (int j = 0; ; j++) {
//                humanX = sc1.nextInt() - 1;//读取玩家x,y
//                humanY = sc1.nextInt() - 1;
//                if (humanX < 0 || humanX > BOARD_SIZE - 1 || humanY < 0 || humanY > BOARD_SIZE - 1 || game.isFill(humanX, humanY) == 1)//判断输入是否合法
//                {//包括坐标的准确性 和 该点是否已经有棋子
//                    System.out.println("请重新输入");
//                    continue;
//                } else break;
//            }
//            game.setBoard(1, humanX, humanY); // 添加玩家棋子
//            if (game.isFive()) //判断玩家是否5连
//            {
//                System.out.println("你获胜了");
//                game.printBoard();
//                break;
//            }
//        }
//    }
//}