package zhang.com.shangji.com.test5.回文子串;


public class Palindrome {
//String substring(int beginIndex, int endIndex) 返回一个新字符串，它是此字符串从 beginIndex 开始截取到 endIndex（不包含）的一个子字符串
    public static boolean isPalindrome(String t){
        char []a=t.toCharArray();
       // int i=0;
        int j=a.length-1;

        for(int i=0;i<(a.length+1)/2;i++){

                if(a[i]!=a[j-i]) return false;
        }
        return true;
    }
    public static String findsubstring(String s){
       // StringBuffer buffer = new StringBuffer();
        String temp="";
        if(s.length()==1)return s;
       for(int i=0;i<s.length();i++){
           for(int j=i+1;j<s.length();j++){
               //判断substring(i,j+1){长度为i~j}是否为回文子串
               String t = s.substring(i,j+1);
               if(isPalindrome(t)){
                   //如果是，存入字符串中，通过与上一个字符串的长度进行比较，存入长度更长的字符串
                   temp=t.length()>temp.length()?t :temp;
               }
           }
       }
        return temp;
    }
    public static void main(String []args){
        String s="babad";
        /**
         *            回文子串 ：1 2 3 4......
         *            a[1]~a[2]  a[1]~a[3]....
         *            特点：对称
         *            找到所有的回文子串，直接存起来，通过逐个比较子串的长度大小，来更换子串
         */
        String subString =findsubstring(s);
        System.out.println(subString);
    }
}