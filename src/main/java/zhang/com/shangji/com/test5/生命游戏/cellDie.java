package zhang.com.shangji.com.test5.生命游戏;

import java.util.Random;
public class cellDie {
    public static int count(int arr[][],int i,int j){
        int count=0;
        //i j  周围有8个点
       // count = arr[i-1][j-1] +arr[i-1][j] +arr[i-1][j+1] +arr[i][j-1] +arr[i][j+1] +arr[i+1][j-1] +arr[i+1][j] +arr[i+1][j+1];
        //i j 靠左
        //i j 靠右
        //i j 靠上
        if(i!=0&&j!=0&&arr[i-1][j-1]==1){
            count++;
        }
        if(i!=0&&arr[i-1][j]==1){
            count++;
        }
        if(i!=0&&j!=arr[0].length-1&&arr[i-1][j+1]==1){
            count++;
        }
        if(j!=0&&arr[i][j-1]==1){
            count++;
        }
        if(j!=arr[0].length-1&&arr[i][j+1]==1){
            count++;
        }
        if(j!=0&&i!=arr.length-1&&arr[i+1][j-1]==1){
            count++;
        }
        if(i!=arr.length-1&&arr[i+1][j]==1){
            count++;
        }
        if(i!=arr.length-1&&j!=arr[0].length-1&&arr[i+1][j+1]==1){
            count++;
        }
        return count;
    }
public static int[][] getCountArray(int arr[][]){
    int a[][]=arr;
    int count=0;
    int t[][]=new int[arr.length][arr[0].length];
    for(int i=0;i<a.length;i++){
        for(int j=0;j<a[0].length;j++){
            //逐个分析每个细胞周围存货的个数
            count=count(arr,i,j);
            //按顺序将count存入数组中
            t[i][j]=count;
        }
    }
    return t;
}
    public static int[][] cellChange(int arr[][]){
        //分析细胞周围的活细胞数
        int a[][]=arr;
        int t[][]=getCountArray(a);
        int count=0;
        //数组时9*9的方格
        for(int i=0;i<a.length;i++){
            for(int j=0;j<a[0].length;j++){
                //如果为count<2   死亡
                if(a[i][j]==1&&t[i][j]<2){
                    a[i][j]=0;
                }
                //如果为2~3 继续活着
                if(a[i][j]==1&&t[i][j]>1&&t[i][j]<4){
                    a[i][j]=1;
                }
                //超过3个死亡
                if(a[i][j]==1&&t[i][j]>3){
                    a[i][j]=0;
                }
                //死细胞周围正好有3个 复活
                if(a[i][j]==0&&t[i][j]==3){
                    a[i][j]=1;
                }
            }
        }
        return a;
        //分析完成之后，对数组进行更改
    }
    //打印数组
    public static void print(int arr[][]){
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr[0].length;j++){
                System.out.print(arr[i][j]+"\t");
            }
            System.out.println();
        }
    }
    public static void main(String []args){
        //初始话方格  10*10
        int table[][]=new int[9][9];
        Random random =new Random();
        //生成地图
        for(int i=0;i<table.length;i++){
            for(int j=0;j<table[0].length;j++){
                table[i][j]=random.nextInt(2);
            }
        }
        System.out.println();
        print(table);
        for(int i=0;i<100;i++){
            cellChange(table);
            System.out.println();
            print(table);
        }

    }
}
