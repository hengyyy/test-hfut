package zhang.com.shangji.com.test4;

public class BF {   //时间复杂度为O(n^2)
    public static int FindBF(String a,String b){
        char[] aa=a.toCharArray();
        char[] bb=b.toCharArray();

        for(int i=aa.length-1;i>bb.length-1;i--){
            int n=i;//记录每次循环开始的i值
          for(int j=bb.length-1;j>=0;j--){
              if(bb[bb.length-1]!=aa[i]){ //bb[bb.length-1]==aa[i]判断每次循环第一对值是否相同
                  break;
              }
              while(bb[j]==aa[i]){//while改成if会有错误！！！
                  //System.out.println(i);
                  j--;
                  i--;
                  if(j==-1)return i+1;
              }
          }
        }
        return -1;
    }
    public static void main(String []args){
        String s="abcdabcd";
        String t="abc";
        int index=FindBF(s,t);
        System.out.println(index);
    }
}