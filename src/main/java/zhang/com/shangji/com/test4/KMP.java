package zhang.com.shangji.com.test4;

//算法的核心是利用匹配失败后的信息，尽量减少模式串与主串的匹配次数以达到快速匹配的目的。
public class KMP {
    public static int[] getNext(String a){//getNext的方法的实现
        char []aa=a.toCharArray();
                 // 字符本身不会影响到它本身的next[]值，只会影响到它下一个值的next[]
        int next[]=new int[a.length()];
        int n=a.length()-1;//n对应0，n-1对应1，n-2对应2
        next[n]=n+1;  int k=n;  next[n-1]=n; int i=n-2;
        while(i>=0){
            if(k==n+1||aa[i+1]==aa[k]){
                next[i]=k-1;
                k--;
                i--;
            }else{
                k=next[k];
            }
        }
        return next;
    }
    public static int FindKMP(String a,String b ,int[]arr){
        char []aa=a.toCharArray();
        char []bb=b.toCharArray();
        int i=aa.length-1;
        int j=bb.length-1;
        while(i>=0){
            if(j==bb.length ||aa[i]==bb[j]){//bb.length+1就报错
                i--;
                j--;
                if(j==-1)return i+1;
            }
            else{
                j=arr[j];
            }
        }
        System.out.println("未找到");
        return (-1);
    }
    public static void main(String []args){
        String a="abcdabcd";
        String b="abc";

        int []next=getNext(b);
        int c=FindKMP(a,b,next);
        System.out.println(c);
    }
}
