package zhang.com.shangji.com.test10.LinkedToAVL;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class ListNode {
      int val;
      ListNode next;
      ListNode() {}
    ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }



  }
class MyLinkedList {
    ListNode head;

    public void createListF(int[] a)    //头插法：由数组a整体建立单链表
    {

        ListNode s;
        for (int i = 0; i < a.length; i++)    //循环建立数据结点s
        {
            s = new ListNode(a[i]);    //新建存放a[i]元素的结点s

            if(head==null){
                head = s;
                continue;
            }
            ListNode temp=head;
            while(temp.next!=null){
                temp=temp.next;
            }
            temp.next=s;
        }
    }
}
 class TreeNode {
      int val;
     TreeNode left;
      TreeNode right;
      TreeNode() {}

     public int height() {
         return Math.max(left == null ? 0 : left.height(), right == null ? 0 : right.height()) + 1;
     }
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
     }
 }

public class LinkedToAVL {
    public ListNode globalHead;
        public  TreeNode sortedListToBST(ListNode head) {
            globalHead = head;
            int length = getLength(head);
            return buildTree(0, length - 1);
        }
        public int getLength(ListNode head) {
            int ret = 0;
            while (head != null) {
                ++ret;
                head = head.next;
            }
            return ret;
        }
        public TreeNode buildTree(int left, int right) {
            if (left > right) {
                return null;
            }
            int mid = (left + right + 1) / 2;
            TreeNode root = new TreeNode();
            root.left = buildTree(left, mid - 1);
            root.val = globalHead.val;
            globalHead = globalHead.next;
            root.right = buildTree(mid + 1, right);
            return root;
        }


        public static void infixOrder(TreeNode tree){
            List<Object> l = new LinkedList<>();

            Queue<TreeNode> q = new LinkedList<>();
            int height = tree.height();
            int ret=0;
            q.offer(tree);
            ret+=1;
            while(!q.isEmpty()){
                int size = q.size();   TreeNode nums[]=new TreeNode[size];
                for(int i=0;i<size;i++){

                    nums[i]=q.poll();

                    if(nums[i]!=null){
                       // System.out.print(nums[i].val+" ");
                        l.add(nums[i].val);
                    }

                    else{
                     //   System.out.print("null ");
                        l.add("null");
                    }


                    if(nums[i]==null) continue;

                    if(nums[i].left!=null){
                        q.offer(nums[i].left);
                        if(nums[i].right==null&&ret<=height)q.offer(null);
                    }
                    if(nums[i].right!=null){
                        q.offer(nums[i].right);
                        if(nums[i].left==null&&ret<=height)q.offer(null);
                    }
                }
                ret+=1;

            }

            for(int j=l.size()-1;j>0;j--){

//                if(l.get(j).equals("null"))break;
                if (l.get(j).equals("null")) {
                    l.remove(j);
                }
                else break;
            }

            System.out.println(l);
        }

    public static void main(String []args){

        int arr[]={-10,-3,0,5,9,110,200,1000,10000};
        MyLinkedList list = new MyLinkedList();//[0,-3,9,-10,null,5]
        list.createListF(arr);

        LinkedToAVL l = new LinkedToAVL();
        l.infixOrder(l.sortedListToBST(list.head));

    }
}
