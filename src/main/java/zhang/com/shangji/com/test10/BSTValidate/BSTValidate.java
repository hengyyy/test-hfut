package zhang.com.shangji.com.test10.BSTValidate;

import java.util.Deque;
import java.util.LinkedList;


    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    class Solution{
        public static boolean isValidBST(TreeNode root) {
            Deque<TreeNode> stack = new LinkedList<TreeNode>();
            double inorder = -Double.MAX_VALUE;

            while (!stack.isEmpty() || root != null) {
                while (root != null) {
                    stack.push(root);
                    root = root.left;
                }
                root = stack.pop();
                // 如果中序遍历得到的节点的值小于等于前一个 inorder，说明不是二叉搜索树
                if (root.val <= inorder) {
                    return false;
                }
                inorder = root.val;
                root = root.right;
            }
            return true;
        }
    }

    public class BSTValidate{
        public static TreeNode constructTree(Integer[] array) { //层次遍历构建二叉树
            if (array == null || array.length == 0 || array[0] == null) {
                return null;
            }

            int index = 0;
            int length = array.length;

            TreeNode root = new TreeNode(array[0]);
            Deque<TreeNode> nodeQueue = new LinkedList<>();
            nodeQueue.offer(root);
            TreeNode currNode;
            while (index < length) {
                index++;
                if (index >= length) {
                    return root;
                }
                currNode = nodeQueue.poll();
                Integer leftChild = array[index];
                if (leftChild != null) {
                    currNode.left = new TreeNode(leftChild);
                    nodeQueue.offer(currNode.left);
                }
                index++;
                if (index >= length) {
                    return root;
                }
                Integer rightChild = array[index];
                if (rightChild != null) {
                    currNode.right = new TreeNode(rightChild);
                    nodeQueue.offer(currNode.right);
                }
            }

            return root;
        }

        public static void main(String[] args) {
            Solution solution = new Solution();
            Integer[] arr = {5,1,4,null,null,3,6};  // 你的数组
            try {
                TreeNode tree = constructTree(arr);
                boolean isSym = solution.isValidBST(tree);
                System.out.println(isSym);
            } catch (Exception e) {
                System.out.println("无法构建树");
            }
        }
    }


