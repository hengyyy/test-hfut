package zhang.com.shangji.com.test7.DGraph;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

public class FindGraph {
    final int INF = 0x3f3f3f3f;
    public boolean isContainK(int[][]arr,int s,int e,int k){

        int n = arr.length;
        int Bl = 0;
        for(int i = 0 ;i < arr.length;i++){//统计边数
            for(int j = 0;j < arr[0].length;j++){
                if(arr[i][j] != 0 && arr[i][j] != INF)Bl++;
            }
        }
        AdjGraph adjGraph = new AdjGraph();     //生成邻接表图
        adjGraph.createAdjGraph(arr,n,Bl);

        adjGraph.visited = new int[n];          //用于判断能不能走
        for(int i = 0;i < n;i++)
            adjGraph.visited[i] = 0;

        Stack<Integer> path = new Stack<>();
        return DFS(adjGraph,e,path,s,k);
    }

    public boolean DFS(AdjGraph adjGraph,int to,Stack stack,int from,int k){
        List<Integer> l = new LinkedList<>();
        if(from >= adjGraph.adjList.length) return false;
        if(from == to) return true;
        ArcNode temp = adjGraph.adjList[from].firstArc;
        while(temp != null){
            if(adjGraph.visited[from] != 1){
                adjGraph.visited[from] = 1;

                stack.add(from);   //入栈
                if(DFS(adjGraph,to,stack,temp.adjvex,k)) {
                    //判断成功，输出栈
                    for(int i = 0;i < stack.size();i++){
                        if(stack.get(i) != null) {
                            l.add((Integer)stack.get(i));       //强制转型
                            if((Integer)stack.get(i)==k) return true;
                        }
                    }
//                        l.add(5);
//                    System.out.println(l);
                    for(int i = 0;i < adjGraph.visited.length;i++) adjGraph.visited[i] = 0;
                    stack.pop();  //出栈最后一个数
                    }
                if(temp.Bnext == null){
                    return false;
                }
                if(!stack.empty() && temp.Bnext != null){
                    stack.pop();   //再出栈一个
                }
                temp = temp.Bnext;
            }
        }
        return false;
    }
    public static void main(String[] args){
           int[][] graph = {{0,1,0,1,0,0},
                            {0,0,0,0,0,1},
                            {0,1,0,0,0,1},
                            {0,0,0,0,1,0},
                            {0,1,0,0,0,1},
                            {0,0,0,0,0,0}};
        FindGraph findTheWay = new FindGraph();
        boolean a=findTheWay.isContainK(graph,0,5,4);
        System.out.println(a);
    }
}