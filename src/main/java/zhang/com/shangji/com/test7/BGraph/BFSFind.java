package zhang.com.shangji.com.test7.BGraph;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class BFSFind {
    public static int findMaxDistance(int[][] graph){
        int n = 0;             //用于结算海洋点的个数
        int maxDistance = 0;   //最大距离
        BfsGraph matGraph = new BfsGraph();
        matGraph.createMatGraph(graph,(graph.length), (graph.length - 1));//创建地图

        for(int i = 0;i < graph.length;i++){
            for(int j = 0;j < graph.length;j++){
                if(graph[i][j] == 0){
                    n++;
                    int[] temp_arr = BFS(matGraph,i,j);
                    maxDistance = Math.max(maxDistance, result(i,j,temp_arr[0],temp_arr[1]));   //比较从而找出最大距离
                }
            }
        }
        if(n == graph.length * graph.length||n == 0)   //全是海洋或陆地
            return -1;
        return maxDistance;
    }
    public static int[] BFS(BfsGraph g, int i, int j) {   //邻接矩阵广度遍历
        Queue<int[]> q = new LinkedList<>();   //创建队列
        q.offer(new int[]{i,j});               //头结点入队
        int[] temp;
        while (!q.isEmpty()){
            temp = q.poll();                    //出队一个数据
            g.visited[temp[0]][temp[1]] = 1;  //标记访问过的节点

            if(temp[0] < g.n - 1 && g.visited[temp[0]+1][temp[1]] != 1){   //判断
                if(g.edges[temp[0] + 1][temp[1]] == 1)
                    return new int[]{temp[0]+1,temp[1]};
                else
                    q.add(new int[]{temp[0]+1,temp[1]});
            }
            if(temp[0] > 0 && g.visited[temp[0]-1][temp[1]] != 1){
                if(g.edges[temp[0] - 1][temp[1]] == 1)
                    return new int[]{temp[0]-1,temp[1]};
                else
                    q.offer(new int[]{temp[0]-1,temp[1]});
            }
            if(temp[1] < g.n - 1 && g.visited[temp[0]][temp[1]+1] != 1){
                if(g.edges[temp[0]][temp[1]+1] == 1)
                    return new int[]{temp[0],temp[1]+1};
                else
                    q.add(new int[]{temp[0],temp[1]+1});
            }
            if(temp[1] > 0 && g.visited[temp[0]][temp[1]-1] != 1){
                if(g.edges[temp[0]][temp[1]-1] == 1)
                    return new int[]{temp[0],temp[1]-1};
                else
                    q.add(new int[]{temp[0],temp[1]-1});
            }

        }
        return new int[]{i,j};                       //未找到节点，返回头结点
    }

     static int result(int x1, int y1, int x2, int y2){   //计算曼哈顿距离

         int x=Math.abs(x1-x2);
        int y =Math.abs(y2 - y1);
        int result=x+y;
        return result;
    }

    public static void main(String[] args) {

        Random rand = new Random();

        int[][] map = new int[5][5];
        for(int i = 0;i < 5;i++){
            for(int j = 0;j < 5;j++){
                map[i][j]=rand.nextInt(2);
                System.out.print(map[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println("输出结果为："+BFSFind.findMaxDistance(map));
    }
}
