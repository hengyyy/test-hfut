package zhang.com.shangji.com.test7.DGraph;

public class ArcNode {
    int adjvex;
    ArcNode Bnext;
    int weight;
}
class VNode {
    int data;
    ArcNode firstArc;
}
class AdjGraph {
    final int MAXV = 100;
    final int INF = 0x3f3f3f3f;
    VNode[] adjList;
    int[] visited;
    int n,e;

    public AdjGraph(){
        adjList = new VNode[MAXV];
        for(int i = 0;i < MAXV;i++){
            adjList[i] = new VNode();
        }
    }

    public void createAdjGraph(int[][] a,int n,int e){
        this.n = n;
        this.e = e;
        ArcNode p;
        for(int i = 0;i < n;i++){
            for(int j = n - 1;j >= 0;j--){
                if(a[i][j] != 0 && a[i][j] != INF){
                    p = new ArcNode();
                    p.adjvex = j;
                    p.weight = a[i][j];
                    p.Bnext = adjList[i].firstArc;
                    adjList[i].firstArc = p;
                }
            }
        }
    }

}


