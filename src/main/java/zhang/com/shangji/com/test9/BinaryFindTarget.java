package zhang.com.shangji.com.test9;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class BinaryFindTarget {


    /**
     * 二分查找
     * 首先找到二维数组中，大于arr[i][0]，小于arr[i][arr[0].lenght-1]的行
     * 对应行的下标存入队列中
     * 然后对每行进行二分查找
     */
    public static boolean findTarget(int arr[][],int target){

        Queue<Integer> q= new LinkedList<>();
        for(int i=0;i<arr.length;i++){
            if(target>arr[i][0] && target<arr[i][arr[0].length-1]){
                q.add(i);  //如果没有元素入队，会跳过下面的while循环，直接返回false
            }
        }
        while(!q.isEmpty()){  //判断队列是否为空
            int e=q.poll();
                int left = 0;
                int right = arr[e].length - 1;
                while (left <= right) {
                    int mid = left + (right - left) / 2;
                    if (arr[e][mid] == target) {
                        return true;
                    } else if (arr[e][mid] < target) {
                        left = mid + 1;
                    } else {
                        right = mid - 1;
                    }
                }
        }
        return false;
    }

    public static void main(String []args){
        int matrix[][]={{1, 4, 7, 11,15},
                        {2, 5, 8, 12,19},
                        {3, 6, 9, 16,22},
                        {10,13,14,17,24},
                        {18,21,23,26,30}
                                            };
        int target = 12;
        System.out.println(findTarget(matrix,target));
    }
}
