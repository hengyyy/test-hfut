package zhang.com.shangji.com.test9;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BInaryFindPeekValue {

    /**
     * 取其交集
     *找到每一行中最大的值的坐标，将他们存入到集合l中去
     * 找到每一列中最大的值的坐标，判断l集合中是否包含这个元素，如果包含，存入到res集合中去。
     */

    //二分查找，查找的是有序的数组
    public static int findMaxIndex(int[] arr) {
        int left = 0;
        int right = arr.length - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (arr[mid] < arr[mid + 1]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return left;
    }
    public static List findPeek(int arr[][]){
        System.out.println("55===？"+arr[5][findMaxIndex(arr[5])]);
        List<List<Integer>> l = new ArrayList<>();
        int index[] =new int[arr.length];
        int result[]=new int[arr[0].length];
        /**找打行中的最大值*/

        for(int j=0;j<arr.length;j++){
            int temp=arr[j][0];
        for(int i=0;i<arr[0].length;i++){
            if(temp<arr[j][i]){
                temp=arr[j][i];
                index[j] =i;
            }
             }
        }
        System.out.println("每一行中最大的下标：");
        for(int e:index)
        System.out.print(e+" ");
        System.out.println();
        for(int j=0;j<arr.length;j++){
                    result[j] = arr[j][index[j]];
               List<Integer> ll = new ArrayList<>();
                ll.add(j);
                ll.add(index[j]);
                l.add(ll);
        }
        System.out.println(l);

        /**找到列中的最大值*/
        int index2[] =new int[arr[0].length];
        for(int j=0;j<arr[0].length;j++){
            int temp=arr[0][j];
            for(int i=0;i<arr.length;i++){
                if(temp<arr[i][j]){
                    temp=arr[i][j];
                    index2[j] =i;
                }
            }
        }
        List<List<Integer>> l2 = new ArrayList<>();
        List<List<Integer>> res = new ArrayList<>();
        System.out.println("每一列中最大的下标：");
        for(int e:index2)
            System.out.print(e+" ");
        System.out.println();

        for(int j=0;j<arr[0].length;j++) {
            result[j] = arr[index2[j]][j];
            List<Integer> ll = new ArrayList<>();
            ll.add(index2[j]);
            ll.add(j);
            l2.add(ll);
            if(l.contains(ll))res.add(ll);
        }
        System.out.println(l2);
        return res;
    }

    public static void main(String []args){
        /**任意相邻两个格子中的数值都不相同。*/
            int mat[][]={{10, 110,13,  1,  5,2,2,1,  2},         //1   11
                         {19,11, 0,   1,  5,2,2,100,200},        //8   200
                         {1, 11, 1000,1,  5,2,2,1,  2},          //2   11
                         {1, 1,  10,  1,  5,2,2,131,2},          //7   131
                         {1, 11, 130, 188,5,2,2,1,  2},          //3   188
                         {1, 11, 1,   55, 1,2,2,1,  2},          //3   55
                             };
            int mat1[][]={{1,4},
                    {3,2}};
        int mat2[][]={{1,4},
                {2,3}};
        System.out.println("随机的一个峰值："+ findPeek(mat));
    }
}
