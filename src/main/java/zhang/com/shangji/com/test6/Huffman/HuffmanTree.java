package zhang.com.shangji.com.test6.Huffman;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * v
 //编码格式有 UTF-8 GB2312 GBK UTF-16 UTF-32
 try {
 int length = "测试不同编码格式的字节长度".getBytes("编码格式").length;
 } catch (UnsupportedEncodingException e) {
 e.printStackTrace();

 ————————————————
 版权声明：本文为CSDN博主「醉梦洛」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 原文链接：https://blog.csdn.net/weixin_39921821/article/details/121083799
 */

public class HuffmanTree {
    public String decompress(String fileName){
        DataInputStream in = null;
        String srcContent = "";
        try{
            in = new DataInputStream(new FileInputStream(new File(fileName)));
            //1 读取码表的信息，构建出码表
            Map<String,String> map = readCodes(in);
            //2 读回具体的数据内容
            byte[] datas = this.readDatas(in);
            //3 把读回的字节还原成对应的整型数据
            int[] dataInts = this.bytes2IntArray(datas);
            //4 根据码表，把内容组成的哈夫曼编码，依次转换回原始的字符，从而得到原始的内容
            srcContent = this.huffman2Char(map, dataInts);
        }catch(Exception err){
            err.printStackTrace();
        }finally{
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return srcContent;
    }
    //读取码表
    //key对应哈夫曼编码   value对应字符
    private Map<String,String> readCodes(DataInputStream in)throws Exception{
        Map<String,String> map = new HashMap<String,String>();
        //1 读回编码的个数
        int codeNum = in.readInt();    //------------读取 4 个输入字节并返回一个整数值------------
        //2 读回每个字符、编码长度、haffuman编码
        for(int i=0;i<codeNum;i++){
            char codeChar = in.readChar();
            int codeLen = in.readInt();
            String code = "";
            char[] cs = new char[codeLen];
            for(int j=0;j<cs.length;j++){
                code += in.readChar();
            }
            map.put(code, ""+codeChar);
        }
        return map;
    }
    //读取数据部分
    private byte[] readDatas(DataInputStream in)throws Exception{
        int dataByteNum =  in.readInt();
        byte[] bs = new byte[dataByteNum];
        for(int i=0;i<dataByteNum;i++){
            bs[i] = in.readByte();
        }
        return bs;                                                      /**怎么返回byte类型的数据，那么压缩的到底是什么*/
    }
    //把这些byte转换成写int型
    private int[] bytes2IntArray(byte[] datas){
        int[] as = new int[datas.length];
        for(int i=0;i<datas.length;i++){
            if(datas[i] >= 0){
                as[i] = datas[i];
            }else{
                as[i] = datas[i] + 256;
            }
        }
        return as;
    }
    //进一步转换成原始的字符
    private String huffman2Char(Map<String,String> map,int[] dataInts){
        StringBuffer buffer = new StringBuffer();
        String str = this.int2BinaryString(dataInts);
        while(str.length()>0){
            for(String code : map.keySet()){
                if(str.startsWith(code)){
                    buffer.append(map.get(code));
                    str = str.substring(code.length());
                    break;
                }
            }
        }
        return buffer.toString();
    }
    //把int值转换回对应的二进制编码的字符串
    private String int2BinaryString(int[] as){
        int len = as.length;

        String[] ss = new String[len];

        String binaryStr = "";

        for(int i=0;i<len-1;i++){

            ss[i] = Integer.toBinaryString(as[i]);
            ss[i] = this.addZero(ss[i]);
            binaryStr += ss[i];
        }
        //处理尾部原来不得0的个数，就是去掉这些补得0
        int zeros = as[len-1];
        if(zeros > 0){
            binaryStr = binaryStr.substring(0,binaryStr.length() - zeros);
        }

        return binaryStr;
    }
    //给每个二进制字符串补足成8位
    private String addZero(String str){
        if(str.length()<8){
            int zeroNum = 8-str.length();
            for(int i=0;i<zeroNum;i++){
                str = "0"+str;
            }
        }
        return str;
    }






    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void compress(String str,String outFile){//---------------str读取的内容     outFile输出的内容-------------------
        //1 统计字符出现的次数
        HuffmanPriorityQueue queue = this.statistics(str);
        //2 构建哈夫曼树
        HuffmanNode tree = this.buidHuffmanTree(queue);
        //3 编码  对哈夫曼树的左边标记0，右边标记1，就可以的到字符的哈夫曼编码
        Map<String,String> map = new HashMap<String,String>();
        this.buildHuffmanCode(map, tree, "");
        //4 输出：把编码序列输出，这就是压缩后的数据
        this.outData(str,map,outFile);
    }
    //-----------------------------------统计内容中字符出现的次数，并构建权重的优先级队列-----------------------------------------
    private HuffmanPriorityQueue statistics(String str){
        /**统计次数*/
        Map<Character,Integer> map = new HashMap<Character,Integer>();//-------key :Character  value:Integer-----------------------------
        char[] cs = str.toCharArray();
        for(char c : cs){
            Object obj = map.get(c);//------------------------get() 方法获取指定 key 对应对 value----------------------------
            if(obj==null){
                map.put(c, 1);//------------------------------put() 方法添加 key 和 value ---------------------------------
            }else{
                map.put(c, ((Integer)obj)+1);//
            }
        }//----------------------------------------------------------完成词频统计------------------------------------------

        /**构建优先级队列*/
        HuffmanPriorityQueue queue = new HuffmanPriorityQueue(map.size());
        for(char c : map.keySet()){//------------------- keySet() 方法返回映射中所有 key 组成的 Set 视图 ---------------------
            HuffmanNode node = new HuffmanNode(c, map.get(c));//--------------------构建节点---------------------
            queue.insert(node);//----------------------------------------------------排序-----------------------------
        }
        return queue;
    }

    /**构建哈夫曼树*/
    private HuffmanNode buidHuffmanTree(HuffmanPriorityQueue queue){
        while(queue.size() > 1){
            /**先取出两个权重最小的对象*/
            HuffmanNode n1 = queue.remove();
            HuffmanNode n2 = queue.remove();

            HuffmanNode n3 = new HuffmanNode((char)0,n1.getCount()+n2.getCount());//构建两个对象的父节点
            n3.setLeftChild(n1);
            n3.setRightChild(n2);
            queue.insert(n3); //把父节点加入队列中
        }
        return queue.peekFront();
    }


    //根据huffman树生成对应的哈夫曼码 ，字符对应的哈夫曼码值都在叶子节点上，碰到左子树加0,右子树加1
    private void buildHuffmanCode(Map<String,String> map,HuffmanNode tree,String zeroOrOneStr){//递归得到叶子节点对应的哈夫曼编码-------
        //树没有子节点
        if(tree.getLeftChild() == null && tree.getRightChild()==null){
            map.put(""+tree.getC(), zeroOrOneStr);//map的方法put()
        }
        //有左子节点
        if(tree.getLeftChild()!=null){
            this.buildHuffmanCode(map, tree.getLeftChild(), zeroOrOneStr+"0");
        }
        //有右子节点
        if(tree.getRightChild()!=null){
            this.buildHuffmanCode(map, tree.getRightChild(), zeroOrOneStr+"1");
        }
    }


    //输出数据，str原始数据  map哈弗曼编码 outFile 输出文件的路径和文件名
    private void outData(String str,Map<String,String> map,String outFileName){     //          !!!!!!!!!!!!!!!!!
        File outFile = new File(outFileName);//File file = new File(String pathName);//参数可以传目录路径，或者文件路径（相对和绝对均可）。
        DataOutputStream os = null;//-----------------------数据输出流允许应用程序以与机器无关方式将Java基本数据类型写到底层输出流。
        try{
            os = new DataOutputStream(new FileOutputStream(outFile));
            this.outCodes(os, map);//------------------------输出哈夫曼编码表（解码也要用到）-----------------------------------
            String dataHuffmanCode = this.source2HumanStr(str, map);//----------输出源内容的每个字符对应的huffman编码-----------
            this.outDataHuffmanCode(os, dataHuffmanCode);           //完成压缩
        }catch(Exception err){
            err.printStackTrace();
        }finally{
            try {
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    //输出哈夫曼编码表（outData中的子方法）
    private void outCodes(DataOutputStream os,Map<String,String> map)throws Exception{  //????????????????????
        //1 输出码的个数
        os.writeInt(map.size());//将一个 int 值写入输出流，该值由四个字节组成----size() 方法用于计算 hashMap 中键/值对的数量---------
        for(String key : map.keySet()){
            // 2输出每个字符以及编码的长度
            os.writeChar(key.charAt(0));
            os.writeInt(map.get(key).length());
            //3输出每个字符对应的huffman编码
            os.writeChars(map.get(key));
        }
    }

    //把原始的内容转换成为哈夫曼编码串
    private String source2HumanStr(String str,Map<String,String> map){
        StringBuffer buffer = new StringBuffer();
        char[] cs = str.toCharArray();
        for(char c : cs){
            buffer.append(map.get(""+c));
        }
        return buffer.toString();
    }
    //输出哈夫曼的编码   回来的时候读成byte数组？？？ 哈夫曼二进制编码，转换成为数组，而且是二进制的数组
    private void outDataHuffmanCode(DataOutputStream os,String dataHuffmanCode)throws Exception{
        byte[] bs = this.string2ByteArrays(dataHuffmanCode);//--------------把这个huffman编码串转换成对应的byte[]--------------
        // 2 输出byte数组个数
        os.writeInt(bs.length);
        // 3 输出byte数组
        os.write(bs);
    }
    //把哈弗曼编码转换成真正的二进制数组
    private byte[] string2ByteArrays(String dataHuffmanCode){
        byte[] retBytes = null;
        char[] cs = dataHuffmanCode.toCharArray();
        int len = cs.length;
        int lenByte = 0;
        //1 判断整个串的长度是否能被8整除
        if(len % 8 == 0){ //--------------------------------------------能被8整除------------------------------------------
            lenByte = len/8 + 1;
            retBytes = new byte[lenByte];
            for(int i=0;i<lenByte-1;i++){
                String s = "";
                for(int j=i*8;j<(i+1)*8;j++){
                    s += cs[j];
                }
                retBytes[i] = this.chars2byte(s);
            }
            //补0
            retBytes[lenByte - 1] = 0;
        }else{
            //不能被8整除，往后补0
            lenByte = len/8 + 2;
            retBytes = new byte[lenByte];
            int zeroNum = 8 - len % 8;
            //补0
            for(int i=0;i<zeroNum;i++){
                dataHuffmanCode +="0";
            }
            //重新计算char数组
            cs = dataHuffmanCode.toCharArray();
            for(int i=0;i<lenByte -1;i++){
                String s = "";
                for(int j=i*8;j<(i+1)*8;j++){
                    s += cs[j];
                }
                retBytes[i] = this.chars2byte(s);
            }

            retBytes[lenByte - 1] = (byte)zeroNum;
        }

        return retBytes;
    }

    //把一个字符串转换成一个byte类型的数据
    private byte chars2byte(String s){
        byte ret = 0;
        char[] cs = s.toCharArray();
        for(int i=0;i<cs.length;i++){
            //(byte)(Byte.parseByte(""+cs[i])*Math.pow(2,cs.length-i-1));
            byte tempB = (byte)(Byte.parseByte(""+cs[i])*Math.pow(2,cs.length-i-1));//---计算二进制的真正数值(0~127)--------
            ret = (byte)(ret+tempB);
        }
        return ret;
    }

    public String readFile(String fileName){
        StringBuffer buffer = new StringBuffer();
        DataInputStream in = null;

        try {
            in = new DataInputStream(new FileInputStream(new File(fileName)));
            String tempStr = "";
            while((tempStr=in.readLine())!=null){
                buffer.append(tempStr+"\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return buffer.toString();
    }
    public static void main(String[] args) {
        HuffmanTree t = new HuffmanTree();

        t.compress(t.readFile("英语单词.txt"), "temp5.txt");
        String s = t.decompress("temp5.txt");
        System.out.println("s=="+s);


                File file = new File("英语单词.txt");
                double fileSize = file.length();
                System.out.println("文件的大小为：" + fileSize + "字节");

        File file2 = new File("temp5.txt");
        double fileSize2 = file2.length();
        System.out.println("压缩后的文件的大小为：" + fileSize2 + "字节");

        double percenta =fileSize2/fileSize;
        System.out.println("压缩的比例=="+percenta);
    }
}