package zhang.com.shangji.com.test6.isSymmertry;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

public class IsSymmetry {
    static class Node{
        Object key;
        Node left;
        Node right;
        Node (Object key){
            this.key=key;
            left=null;
            right=null;
        }

        @Override
        public String toString() {
            return "{" +
                    "" + key +
                    "," + left +
                    "," + right +
                    '}';
        }
    }
    static Node root;
    static Node temp = root;

    //在遍历二叉树的时候分为两大类，一种是BFS（广度优先遍历）Breadth FirstSearch
    //一种是DFS（深度优先遍历）即Depth First Search,深度优先遍历又分为前序，中序和后序。
    static void inorder(Node temp){
        if(temp==null){
            return;
        }
        inorder(temp.left);
        System.out.print(temp.key+" ");
        inorder(temp.right);
    }
    static boolean isSymmetric(Node root){
        if(root==null)return true;
        return symmetry(root.left,root.right);
    }

    static boolean symmetry(Node A, Node B){
        if(A==null&&B==null) return true;
        if(A==null||B==null) return false;
        if(A.key!=B.key) return false;
        return symmetry(A.left,B.right)&&symmetry(A.right,B.left);
    }
    //插入节点
    static void insert(Node temp,Object key){
        Queue<Node> q= new LinkedList<Node>();
        if(temp==null){
            root=new Node(key);
            return;
        }
        q.add(temp);
        //层序遍历，直到寻找到一个可用的位置插入节点
        while(!q.isEmpty()){
            temp=q.peek();
            q.remove();

            if(temp.left==null){
                temp.left=new Node(key);
                //判断是否为null
                if(temp.left.key==null){
                   // temp.left=null;
                    temp.left.left=new Node(null);
                    temp.left.right=new Node(null);
                }
                break;
            }else{
                q.add(temp.left);
            }
            if(temp.right==null){
                temp.right=new Node(key);
                //判断是否为null
                if(temp.right.key==null){
                   // temp.right=null;
                    temp.right.left=new Node(null);
                    temp.right.right=new Node(null);
                }
                break;
            }
            else{
                q.add(temp.right);
            }
        }
    }

    static void changeNull(Node temp){
        //非递归先序遍历（需一个辅助栈，顺序为根——右——左）
            if (root==null) return;
            ArrayDeque<Node> stack = new ArrayDeque<>();
            stack.addLast(root);
            while (!stack.isEmpty()){
                Node node = stack.pollLast();
                if(node.key==null)node=null;
                else{
                    if (node.right!=null) stack.addLast(node.right);
                    if (node.left!=null) stack.addLast(node.left);
                }
            }
    }
    public static void main(String []args){
        IsSymmetry t=new IsSymmetry();
       // t.root=new Node(6);
        /**
         *               1
         *         2         2
         *      3      n    n   3
         *    4   4  n  n n  n 4 4
         *  4  4 4 4
         */
        Object arr[]= {1,2,2,3,null,null,3,4,4,4,4,4,4,4,4};
        for(int i=0;i<arr.length;i++){
            insert(t.root,arr[i]);      //将数组进行插入二叉树中
        }
        changeNull(t.root);
        System.out.println(t.root);
        System.out.println("中序遍历：");
        inorder(t.root);                //中序遍历
        System.out.println();
        System.out.println("是否是对称二叉树："+isSymmetric(t.root));//判断是否对称
        //System.out.println(t.root.left.left.key);//检测节点问题
        //注意这个空节点，空节点的数据域是空的
        //System.out.println(t.root.left.right.key);//检测节点问题
    }
}