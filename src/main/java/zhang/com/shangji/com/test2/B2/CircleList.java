package zhang.com.shangji.com.test2.B2;


/** 双指针的方式 */
class L{
    Node head;//头结点
    int size;//记录链表长度
    class Node{
        int data;
        Node next;
        public Node(){
            this.next=null;
        }
        public Node(int e){
            this.data=e;
            this.next=null;
        }
    }

    public L(){
        head=new Node();
    }
    //头插法插入节点
    public void add(int e){
        Node newNode = new Node(e);
        newNode.next=head.next;
        head.next=newNode;
        size++;
    }

    //生成环形链表
    public void pos(int i){//将尾节点指向第i个节点
        Node temp=head;
        Node temp1=head;

        //找到尾节点
        while(temp.next!=null){
            temp=temp.next;

        }
        Node tail=temp;

        for(int j=0;j<i;j++){
            temp1=temp1.next;
        }
        tail.next=temp1;
    }

    //遍历链表
    public void print(){
        Node temp=head;
        while(temp.next!=null){
            temp=temp.next;
        }
    }
    //得到链表的第i个元素
    public int get(int j){//通过索引+1的方式获得数据元素
        Node temp=head;
        for(int i=0;i<j;i++){
            temp=temp.next;
        }

        return temp.data;
    }

    //判断链表中有没有环
    public boolean isCircleList(){
        int n = size;
        Node temp=head;
        for(int k=0;k<n;k++){//得到尾节点
            temp=temp.next;
        }
        if(temp.next==null)return false;
        else return true;
    }
}

public class CircleList {
    public static void main(String []args){
        L l1 = new L();//是链表环
        l1.add(3);
        l1.add(2);
        l1.add(0);
        l1.add(-4);
        l1.pos(2);//链表li变成链表环

        L l2 = new L();//不是链表环
        l2.add(1);
        l2.add(2);
        l2.pos(1);

        L l3 = new L();//是链表环
        l3.add(1);

        System.out.println("l1是不是链表环："+l1.isCircleList());
        System.out.println("l2是不是链表环："+l2.isCircleList());
        System.out.println("l3是不是链表环："+l3.isCircleList());



    }
}
