package zhang.com.shangji.com.test2.B3;


class NodeN{
    double data;//系数
    int n;//指数
    NodeN next;

    public NodeN(){
        next=null;
    }

    public NodeN(int n){
        this.n=n;
        next=null;
    }
}
class Nlist{
   NodeN head;
   int size;


    public Nlist(){
        head =new NodeN();
    }

    public Nlist(int n){
        head =new NodeN(n);
    }

    //插入节点
    public void add(int n,double x){//n是指数，x是系数
        NodeN newnode = new NodeN();
        newnode.n=n;
        newnode.data=x;
        newnode.next=head.next;
        head.next=newnode;
        size++;
    }

    //找到尾节点


    //删除节点
    public void delete(){
        NodeN temp=head;
        NodeN tail=head;
        //找到尾节点
        for(int i=0;i<size-1;i++){//tail.next为尾节点
            tail=tail.next;
        }

       while(temp.next.next!=null){
           if(temp.next.data==0){
               temp.next=temp.next.next;
           }
           temp=temp.next;
        }
        if(tail.next.data==0)
            tail.next=null;
    }

    //打印节点
    public void print(){
        NodeN temp= head;

        while(temp.next!=null){
            if(temp.next.data<0&&temp.next.n==0){
                System.out.print("("+temp.next.data+")");
            }
            if(temp.next.data<0&&temp.next.n!=0){
                System.out.print("("+temp.next.data+"）x^"+temp.next.n);
            }
            if(temp.next.data>0&&temp.next.n==0){
                System.out.print(+temp.next.data);
            }
            if(temp.next.data>0&&temp.next.n!=0){
                System.out.print(temp.next.data+"x^"+temp.next.n);
            }

            if(temp.next.next!=null)System.out.print("+");
            temp=temp.next;
        }
    }

}


public class DToOne {
    public static Nlist toOne(Nlist l1,Nlist l2){
        Nlist l = new Nlist();

            if(l1.size>l2.size){
                //在短链表中所独有的项，要插入到l中

                NodeN temp1 = l1.head;
                for(int i=0;i<l1.size;i++){
                    NodeN temp2 = l2.head;
                    for(int j=0;j<l2.size;j++){        //temp1.next.n   temp2.next.n
                        if(temp1.next.n==temp2.next.n){
                            l.add(temp1.next.n,temp1.next.data+temp2.next.data);
                            break;
                        }
                        else{
                            temp2=temp2.next;
                        }
                        if(j==l2.size-1&&temp1.next.n!=temp2.n)l.add(temp1.next.n,temp1.next.data);
                    }
                    temp1=temp1.next;
                }
                //把短链中独有的幂指数项插入到l链表中去(l2短)
                NodeN nn2 = l2.head;
                for(int i=0;i<l2.size;i++){
                    NodeN nn1=l1.head;
                    for(int j=0;j<l1.size;j++){
                        if(nn1.next.n==nn2.next.n)break;
                        if(nn1.next.n!=nn2.next.n&&j==l1.size-1){
                            l.add(nn2.next.n,nn2.next.data);
                        }
                        nn1=nn1.next;
                    }
                    nn2=nn2.next;
                }

            }
            else{                               //l2.size>l1.size
                NodeN temp2=l2.head;
                for(int i=0;i<l2.size;i++){
                    NodeN temp1= l1.head;//第一层for循环执行的时候，将temp1初始化
                    for(int j=0;j<l1.size;j++){
                        if(temp2.next.n==temp1.next.n){
                            l.add(temp2.next.n,temp2.next.data+temp1.next.data);
                            break;
                        }
                        else{
                            temp1=temp1.next;

                        }
                        if(j==l1.size-1&&temp2.next.n!=temp1.n){//92
                            l.add(temp2.next.n,temp2.next.data);
                        }
                    }
                    temp2=temp2.next;
                }
            //把短链中独有的幂指数项插入到l链表中去(l1短)
               NodeN nn1 = l1.head;
                for(int i=0;i<l1.size;i++){
                    NodeN nn2=l2.head;
                    for(int j=0;j<l2.size;j++){
                        if(nn1.next.n==nn2.next.n)break;
                        if(nn1.next.n!=nn2.next.n&&j==l2.size-1){
                            l.add(nn1.next.n,nn1.next.data);
                        }
                        nn2=nn2.next;

                    }
                    nn1=nn1.next;
                }

            }
        return l;
    }
    public static void main(String []args){

        Nlist l1 = new Nlist();
        l1.add(3,2);
        l1.add(5,3.2);
        l1.add(1,-6);
        l1.add(0,10);
        l1.add(10,10);
        System.out.print("p(x)=");
        l1.print();
        System.out.println();

        Nlist l2 = new Nlist();
        l2.add(1,6);
        l2.add(5,1.8);
        l2.add(3,-2);
        l2.add(2,1);
        l2.add(4,-2.5);
        l2.add(0,-5);
        System.out.print("q(x)=");
        l2.print();
        System.out.println();

        Nlist l3 = new Nlist();

        l3=toOne(l1,l2);//126
        l3.delete();
        System.out.print("合并后的项数r(x)=");
        l3.print();
        System.out.println();

    }
}
