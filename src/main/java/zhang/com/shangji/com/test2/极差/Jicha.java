package zhang.com.shangji.com.test2.极差;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


class mmArrayList{
    int size;
    int data[];
    int capacity=10;

    public mmArrayList(){
        data = new int[capacity];
    }
    //插入数据
    public void add(int e){
        data[size]=e;
        size++;
        if(size>capacity)upData();
    }
    //扩容2倍
    public void upData(){
        capacity = capacity*2;
        int newdata[] = new int[capacity];
        for(int i=0;i<size;i++){
            newdata[i]=data[i];
        }
        data=newdata;
    }

    //遍历顺序表
    public void print(){
        System.out.println("--------------遍历顺序表：------------");
        for(int i=0;i<size;i++){
            System.out.print(data[i]+" ");
        }
        System.out.println();
    }

    //获得顺序表中第i个元素
    public int get(int i){
        return data[i];
    }//i是索引    时间复杂度为O(1)

    //获取顺序表的大小
    public int size(){
        return size;
    }//时间复杂度为1


}

public class Jicha {

        public static void main(String[] args){
            mmArrayList l1 = new mmArrayList();//创建第一个链表
            System.out.println("你想在第一个顺序表中输入几个数据：");
            Scanner scan01 = new Scanner(System.in);
            int n1 = scan01.nextInt();
            System.out.println("请输入第一组整数存入第一个顺序表中:");
                    Scanner scan1 = new Scanner(System.in);//以空格作为分隔符

            for(int i=0;i<n1;i++){
                l1.add(scan1.nextInt());//将输入的数据存入到顺序表中
            }

            mmArrayList l2 = new mmArrayList();//创建第二个链表
            System.out.println("你想在第二个顺序表中输入几个数据：");
            Scanner scan02 = new Scanner(System.in);
            int n2 = scan02.nextInt();
            System.out.println("请输入第一组整数存入第一个顺序表中:");
            Scanner scan2 = new Scanner(System.in);//以空格作为分隔符

            for(int i=0;i<n2;i++){
                l2.add(scan2.nextInt());//将输入的数据存入到顺序表中
            }


            List<Integer> L = new ArrayList<>();//创建集合L用于存储l1和l2的极差
            for(int i=0;i<l1.size();i++){/*执行l1.size次  时间复杂度为O（n^2） */
                for(int j=0;j<l2.size();j++){/*执行l2.size次   时间复杂度为O（n）*/
                    if(l1.get(i)==l2.get(j)){/*时间复杂度为O(1)*/
                        break;
                    }
                        if(j==l2.size()-1&&l1.get(i)!=l2.get(l2.size()-1)) L.add(l1.get(i));//找到极差存入到L集合中  时间复杂度为O（1） 空间复杂度为O(n)
                }
            }
            System.out.println("两顺序表的极差：");/*执行1次*/

                System.out.print(L); /*时间复杂度为O(1)*/



    }
}
