package zhang.com.shangji.com.test1.泛型;
import java.util.Scanner;

public class FanXing{
    public static void main(String[] args)
    {
        System.out.println("确定元素类型：");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        if(n==1){//整数
            System.out.println("输入五个整数");
            int arr[]=new int[5];
            for(int i=0;i<5;i++)
            {
                Scanner sc = new Scanner(System.in);
                arr[i]=sc.nextInt();
            }
            print(arr[0],arr[1],arr[2],arr[3],arr[4]);
        }
        if(n==2){//小数
            System.out.println("输入五个小数");
            float arr[]=new float[5];
            for(int i=0;i<5;i++)
            {
                Scanner sc = new Scanner(System.in);
                arr[i]=sc.nextFloat();
            }
            print(arr[0],arr[1],arr[2],arr[3],arr[4]);
        }
        if(n==3){//字符
            System.out.println("输入五个字符");
            char arr[]=new char[5];
            for(int i=0;i<5;i++)
            {
                Scanner sc = new Scanner(System.in);
                arr[i]=sc.next().charAt(0);
            }
            print(arr[0],arr[1],arr[2],arr[3],arr[4]);
        }



    }
    private static <A,B,C,D,E> void print(A content0,B content1,C content2,D content3,E content4){
        System.out.println("["+content0+","+content1+","+content2+","+content3+","+content4+"]");

    }
}
