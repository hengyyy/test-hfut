package zhang.com.shangji.com.test1.接口;

public interface Soundable {
    void open();
    void close();
    void up();
    void down();
}
