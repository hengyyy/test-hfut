package zhang.com.shangji.com.test1.阶乘;

import java.util.Scanner;

/*
计算n的阶乘的递归调用和非递归调用
 */
public class test1 {
    public static void main(String[] args)
    {
        int n=0;

        Scanner scan1 = new Scanner(System.in);
        System.out.println("其输入任意一个正整数：");
        n= scan1.nextInt();

        //递归调用
        int result = sum1(n);
        System.out.printf("递归调用计算结果：！%d="+result+"\n",n);

        //非递归调用
        int result2=sum2(n);
        System.out.printf("非递归调用计算结果：！%d="+result2+"\n",n);
    }
//递归调用
    public static int sum1(int n)
    {
        if(n==0) return 1;
        else{

            return n*sum1(n-1);
        }
    }

    //非递归调用
    public static int sum2(int n)
    {
        int temp=n;
        for(int i=n;i>1;i--)
        {
            temp=temp*(i-1);
        }
        return temp;
    }
}
