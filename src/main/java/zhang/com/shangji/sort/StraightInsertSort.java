package zhang.com.shangji.sort;

import java.util.Arrays;



public class StraightInsertSort {

    /**
     * 直接插入排序    insertSort算法
     * */
    public static int[] strInsert(int[] arr){
        for(int i=1;i<arr.length;i++){
            int j = i;
            if(arr[i]<arr[i-1]) {
                int temp = arr[i];
                while (temp < arr[j - 1]) {             //temp  ?    arr[i]!!!
                    arr[j] = arr[j-1];
                    j--;
                    if (j == 0){
                        break;
                    }
                }
                arr[j]=temp;
            }
        }
        return arr;
    }

    /**
     *          binaryInsertSort算法
     * */

    public static int[] binaryInsertSort(int arr[]){
        int ret[] = arr;
        for(int i=1;i<arr.length;i++){
            int left= 0;
            int right = i-1;
            int mid;
            if(arr[i]<arr[i-1]){
                int temp = arr[i];
                while(right>left){
                    mid = (left+right)/2;
                    if(arr[mid]>temp) right=mid;
                    else left=mid;
                }
                for(int k=i;k>right;k--){
                    arr[k]=arr[k-1];
                }
                arr[right]=temp;
            }
        }

        return ret;

    }


    /**
     * shellSort算法
     * */

    public static int[] shellSort(int arr[]){
        int ret []= arr;
        int n = arr.length;
        int d=n/2;
        while(d>0){
            for(int i=d;i<n;i++){
                int temp = arr[i];
                int j =i-d;
                while(j>=0 && temp<arr[j]){
                    arr[j+d]=arr[j];
                    j=j-d;
                }
                arr[j+d]= temp;
            }
            d=d/2;
        }
        return ret;
    }
    /**
     * 冒泡排序
     * */

    public static int[] bubbleSort(int arr[]){
        int ret[]=arr;
        for(int j=0;j<arr.length;j++)
        for(int i=0;i<arr.length-1;i++){
            if(arr[i]>arr[i+1]){
                int temp = arr[i];
                arr[i]=arr[i+1];
                arr[i+1]=temp;
            }
        }
        return ret;
    }

    /**
     * 快速排序
     * */

    public static int[] partition(int a[],int l,int r){
        if (l < r)
        {
            int i = l;int j = r;int temp = a[i];
            while (i < j) {
                while (i < j && temp < a[j])j--;
                if (i < j)a[i++] = a[j];
                while (i<j && temp>a[i])i++;
                if (i < j)a[j--] = a[i];
            }
            a[i] = temp;
            partition(a, l, i - 1);
            partition(a, i + 1, r);
        }
        return a;
    }

    public static void print(int arr[],int k){
        partition(arr,0,8);
        for(int i=arr.length-k;i<arr.length;i++){
            System.out.print(arr[i]+" ");
        }
    }

    /**
     * 选择排序
     * */
    public static int[] SimpleSort(int[] a, int len)
    {
        int min_index;
        int temp;
        for (int i = 0;i < len - 1;i++)
        {
            min_index = i;
            for (int j = i;j < len;j++) min_index = (a[min_index] > a[j]) ? j : min_index;

            temp = a[min_index];
            a[min_index] = a[i];
            a[i] = temp;
        }
        return a;
    }

    /**
     * 判断是否为大顶堆
     * */
    public static boolean isHeap(int arr[]){
        int n = arr.length;
        int i;
        if(n%2==0){
            if(arr[n/2-1]<arr[n-1])return false;
            for(i = n/2-2;i>=1;i--)
                if(arr[i]<arr[2*i+2]||arr[i]<arr[2*i+1]) return false;
        }
        else for(i = n/2-1;i>=1;i--)
            if(arr[i]<arr[2*i+2]||arr[i]<arr[2*i+1])return false;
        return true;
    }

    /**
     * 归并排序  自顶向下
     * */
    public static void merge(int a[], int low, int mid, int hight)  //合并函数
    {
        int[] b = new int[hight - low + 1];
        int i = low, j = mid + 1, k = 0;    // k为 b 数组的小标

        while (i <= mid && j <= hight)
        {
            if (a[i] <= a[j])
            {
                b[k++] = a[i++];  //按从小到大存放在 b 数组里面
            }
            else
            {
                b[k++] = a[j++];
            }
        }
        while (i <= mid)   // j 序列结束，将剩余的 i 序列补充在 b 数组中
        {
            b[k++] = a[i++];
        }
        while (j <= hight)  //i 序列结束，将剩余的 j 序列补充在 b 数组中
        {
            b[k++] = a[j++];
        }
        k = 0;  //从小标为 0 开始传送
        for ( i = low; i <= hight; i++)  //将 b 数组的值传递给数组 a
        {
            a[i] = b[k++];
        }
    }

    public static void mergesort(int[] a, int low, int hight) //归并排序
    {
        if (low < hight){
            int mid = (low + hight) / 2;
            mergesort(a, low, mid);                //对 a[low,mid]进行排序
            mergesort(a, mid + 1, hight);     //对 a[mid+1,hight]进行排序
            merge(a, low, mid, hight);             //进行合并操作
        }
    }

    /**
     * 基数排序
     * */
    public static void radixSort(int[] arr) {
        // 假定arr[0] 是最大数
        // 1. 通过遍历arr, 找到数组中真正最大值
        // 2. 目的是确定要进行多少轮排序
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        // 计算最大数字是几位数
        int maxLength = (max + "").length();
        // 定义一个二维数组， 就是10个桶
        // 1. 该二维数组有10个一维数组 0-9
        // 2. 为了防止溢出，每个一维数组(桶)，大小定为 arr.length
        // 3. 很明确, 基数排序是空间换时间
        int[][] bucket = new int[10][arr.length];
        // 用于记录在每个桶中，实际存放了多少个数据,这样才能正确的取出
        int[] bucketElementCounts = new int[10];
        // 根据最大长度的数决定比较的次数
        // 1. 大循环的次数就是 最大数有多少位,前面分析过
        // 2. n = 1, n *= 10 是为了每轮循环排序时，分别求出各个元素的 个位，十位，百位，千位 ...
        //    就是一个小算法
        // 3. 这个基础排序，完全可以使用 冒泡分步写代码来完成，比较简单!!
        for (int i = 0, n = 1; i < maxLength; i++, n *= 10) {
            // 把每一个数字分别计算本轮循环的位数的值,比如第1轮是个位...
            for (int j = 0; j < arr.length; j++) {
                // 计算
                int digitOfElement = arr[j] / n % 10;
                // 把当前遍历的数据放入指定的数组中
                bucket[digitOfElement][bucketElementCounts[digitOfElement]] = arr[j];
                // 记录数量
                bucketElementCounts[digitOfElement]++;
            }
            // 记录取的元素需要放的位置
            int index = 0;
            // 把各个桶中(10个桶)存放的数字取出来, 放入到arr中
            for (int k = 0; k < bucketElementCounts.length; k++) {
                // 如果这个桶中，有数据才取，没有数据就不取了
                if (bucketElementCounts[k] != 0) {
                    // 循环取出元素
                    for (int l = 0; l < bucketElementCounts[k]; l++) {
                        // 取出元素
                        arr[index++] = bucket[k][l];
                    }
                    // 把这个桶的对应的记录的数据个数置为0,注意,桶本身数据(前面存的数据还在)
                    bucketElementCounts[k] = 0; //
                }
            }
        }
    }


    public static void main(String []args){
        int arr[]={10,9,8,7,6,5,4,3,2};
        int ret[]={369,367,167,239,237,138,230,139};
        //System.out.println("直接插入排序：\n"+Arrays.toString(strInsert(arr)));
        //System.out.println("折半插入排序：\n"+Arrays.toString(binaryInsertSort(arr)));
        //System.out.println("shell排序：\n"+ Arrays.toString(shellSort(arr)));
        //System.out.println("shell排序：\n"+ Arrays.toString(bubbleSort(arr)));
        //System.out.println("shell排序：\n"+ Arrays.toString(partition(arr,0,8)));
        //print(arr,3);
        //System.out.println("简单选择排序：\n"+ Arrays.toString(SimpleSort(arr,9)));
        //System.out.println("数组能否构成大顶堆："+ isHeap(arr));
//        mergesort(arr,0,8);
//        System.out.println("归并排序："+ Arrays.toString(arr));

        radixSort(ret);
        System.out.println("基数排序："+ Arrays.toString(ret));
    }
}
