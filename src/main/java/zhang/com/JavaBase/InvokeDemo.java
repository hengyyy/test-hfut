package zhang.com.JavaBase;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InvokeDemo {

    public static void main(String[] args) {
        Employee employee = new Employee("1", "张三");
        Class aClass = employee.getClass();
        System.out.println("a:"+aClass);
        try {
            Method mGetName = aClass.getMethod("getName");
            System.out.println(mGetName);
            Method mGetI = aClass.getMethod("getId");
            System.out.println(mGetI);
            Method mGetId = aClass.getMethod("getId");
            String name = (String) mGetName.invoke(employee);
            Object id = mGetId .invoke(employee);
            System.out.println(id);
            System.out.println(name);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}


