package zhang.com.JavaBase;

// Java program to demonstrate the example
// of Class []  getInterfaces() method of Class

public class Test {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();
        Class cl = sb.getClass();

        // It returns an array of interfaces represented
        // by the class StringBuilder
        Class[] interfaces = cl.getInterfaces();

        for (int i = 0; i < interfaces.length; ++i) {
            System.out.print("StringBuilder Interfaces: ");
            System.out.println(interfaces[i].toString());
        }
    }
}


