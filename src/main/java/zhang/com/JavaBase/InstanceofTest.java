package zhang.com.JavaBase;

public class InstanceofTest {
    public static void main(String[] args) {
        Animal animal1 = new Animal();
        System.out.println("animal1 instanceof Animal:"+ (animal1 instanceof Animal));
        System.out.println("animal1 instanceof Bird:"+ (animal1 instanceof Bird));

        Animal animal2 = new Bird();
        System.out.println("animal1 instanceof Animal:"+ (animal2 instanceof Animal));
        System.out.println("animal1 instanceof Bird:"+ (animal2 instanceof Bird));

        Bird bird = new Bird();
        System.out.println("bird instanceof Animal:"+ (bird instanceof Animal));
        System.out.println("bird instanceof Bird:"+ (bird instanceof Bird));
    }
}
