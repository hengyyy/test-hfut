package zhang.com.JavaBase;

public class AnimalTest {
    public static void main(String[] args){
        Animal a = new Animal();
        a.move();
        a = new Bird();
        a.move();
        a = new Fish();
        a.move();
    }


}

class Animal {
    private String name;
    private int legs;
    public void move() {
        System.out.println("Animal is moving!");
    }
}

class Bird extends Animal {
    public void move() {
        System.out.println("Bird is flying!");
    }

    public void eat() {
        System.out.println("Bird is eating!");
    }
}

class Fish extends Animal{
    public void move() {
        System.out.println("Fish is swimmings!");
    }
}

