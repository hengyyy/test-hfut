package zhang.com.NiuKKK_Vector.vector;

import org.springframework.test.context.TestConstructor;

import java.util.*;

public class array {//vector到底有什么特别的用处呀？
    public static void main(String []args){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        //集合的遍历：迭代器遍历
        //迭代器模式是23种设计模式之一

        //迭代器模式的核心思想：接口的转移。
        //有很多集合，每种集合都有不同的遍历方法，每种集合的底层实现不一样
        Iterator<Integer> vector = list.iterator();
        while (vector.hasNext()){
            System.out.println(vector.next());
        }


    }
}
