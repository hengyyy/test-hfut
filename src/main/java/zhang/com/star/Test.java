package zhang.com.star;

public class Test {
    public static void main(String[] args) {
        BigStar s = new BigStar("霉霉");
        Star starProxy = ProxyUtil.createProxy(s);

        String rs = starProxy.sing("baby");
        System.out.println(rs);
    }
}
