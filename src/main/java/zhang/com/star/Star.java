package zhang.com.star;

public interface Star {
    String sing(String name);
    void dance();
}
