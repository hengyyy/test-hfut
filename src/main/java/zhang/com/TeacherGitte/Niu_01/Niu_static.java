package zhang.com.TeacherGitte.Niu_01;

public class Niu_static {
    static int x,y;

    void method(){//非静态方法，可以调用静态成员变量
        y = x++ + ++x;
        System.out.println("x:"+x+" y:"+y);
    }
    static void print(){
        System.out.println("static");
    }


    public static void main(String[] args) {
        Niu_static keypathExam = new Niu_static();
        keypathExam.method();
        System.out.println(x);
        System.out.println(keypathExam.x);
        keypathExam.print();
        Niu_static.print();
    }
}

