package zhang.com.TeacherGitte.Niu_01;

public class Test {
    static int i = 0;
    public int aMethod(){
//             //Java中静态变量只能在类主体中定义，不能在方法中定义。 静态变量属于类所有而不属于方法。
          //int i = 0;
        i++;
        return i;
    }
    public static void main(String args[]){
        Test test = new Test();
        Test test1 = new Test();

        test1.aMethod();  //静态成员共享相同的内存空间

        test.aMethod();
        System.out.println(i);//1
        int j = test.aMethod();
        System.out.println(j);//2
    }
}
