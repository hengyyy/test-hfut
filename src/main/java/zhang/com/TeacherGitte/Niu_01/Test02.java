package zhang.com.TeacherGitte.Niu_01;

public class Test02 {
    public void print(){
        System.out.println("Hello World!");
    }

    public void print(int a){   //方法重写
        System.out.println(a);
    }
    public static void main(String[] args) {
        Test02 test02 = new Test02();
        test02.print();
        test02.print(1);
    }
}
