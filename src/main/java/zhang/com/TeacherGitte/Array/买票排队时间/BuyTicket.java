package zhang.com.TeacherGitte.Array.买票排队时间;


import java.util.Deque;
import java.util.LinkedList;

class Ticket{

    public int timeRequiredToBuy(int []tickets, int k){
        //构造FIFO双端队列，尾进头出
        Deque<TicketBuyer> buyerDeque = new LinkedList<>();
        for(int i=0;i<tickets.length;i++){
            TicketBuyer buyer = new TicketBuyer();
            buyer.index=i;
            buyer.ticketCnt=tickets[i];
            buyerDeque.addLast(buyer);
        }

        //统计耗时
        int cost=0;
        while(!buyerDeque.isEmpty()){
            //头出
            TicketBuyer buyer = buyerDeque.pollFirst();
            //一人一次只能买一张票
            buyer.ticketCnt-=1;
            //每人买票都需要1秒
            cost++;
            if(buyer.ticketCnt>0){
                //如果需要购买更多的票，他必须重新排队
                buyerDeque.addLast(buyer);
            }else{
                if(buyer.index==k){
                    //返回位置k的人完成买票需要的时间
                    return cost;
                }
            }
        }

        return cost;
    }
    private class TicketBuyer{
        //索引位
        private Integer index;
        //要购买的票
        private Integer ticketCnt;
    }
}

public class BuyTicket {
    public static void main(String []args){
        int []tickets = {2,3,2};
        int pos =1;
        Ticket ticket1 = new Ticket();
        int timeReq = ticket1.timeRequiredToBuy(tickets,pos);
        System.out.println(timeReq);

    }
}
