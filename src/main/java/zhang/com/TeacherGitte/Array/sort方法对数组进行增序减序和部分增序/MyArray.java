package zhang.com.TeacherGitte.Array.sort方法对数组进行增序减序和部分增序;

import java.util.Arrays;
import java.util.Comparator;

public class MyArray {
    public static void main(String []args){
        int []a={6,2,1,9,5,7,4,3,8};
        System.out.println("增序排序：");
        Arrays.sort(a);
        for(int e:a){
            System.out.print(e+" ");
        }
        System.out.println("");
        System.out.println("减序排序：");
        //需要用到包装类型，而不是基本类型
        Integer []b={6,2,1,9,5,7,4,3,8};
        Arrays.sort(b,new Comparator<Integer>(){/***/

            public int compare(Integer o1,Integer o2){
                return o2-o1;//返回值>0时进行交换
            }
        });
        for(int e:b){
            System.out.print(e+" ");
        }

        System.out.println("\n部分排序:");
        int []c={6,2,1,9,5,7,4,3,8};
        Arrays.sort(c,2,6);
        for(int e:c){
            System.out.print(e+" ");
        }
    }

}
