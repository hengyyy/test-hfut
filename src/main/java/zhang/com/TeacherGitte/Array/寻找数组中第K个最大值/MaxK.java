package zhang.com.TeacherGitte.Array.寻找数组中第K个最大值;

import java.util.Comparator;
import java.util.PriorityQueue;

class Solver{
    public int findKthLargest(int []nums,int k){
        int len = nums.length;
        //使用一个含有k个元素的最小堆，priorityQueue底层是动态数组，为了防止数组扩容产生消耗，可以先指定数组的长度
        PriorityQueue<Integer> minHeap = new PriorityQueue<>(k, Comparator.comparing(a->a));
        //java中没有heapify，因此我们逐个将钱k个元素添加到minHeap里面
        for(int i=0;i<k;i++){
            minHeap.offer(nums[i]);
        }

        for(int i=k;i<len;i++){
            //看一眼，不拿出，因为有可能没有必要替换
            Integer topElement = minHeap.peek();
            //只要当前遍历的元素比锥顶元素大，锥顶弹出，遍历的元素进去
            if(nums[i]>topElement){
                //java没有replace（）,所以先poll出来，再放进去

                minHeap.poll();
                minHeap.offer(nums[i]);
            }
        }
        return minHeap.peek();
    }
}

public class MaxK {
    public static void main(String []args){
        int myNums[]={1,2,3,4,5,2,1,5,8,19};
        int pos=2;
        Solver solver = new Solver();
        int result = solver.findKthLargest(myNums,pos);
        System.out.println(result);
    }
}
