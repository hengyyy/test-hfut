package zhang.com.afterClassWorks.streams;


import java.io.*;
import java.nio.charset.*;
import java.nio.file.*;
import java.util.*;

public class CountLongWords {           //对某本书中的所有长单词进行计数
    public static void main(String []args) throws IOException {
        var con = new String(Files.readAllBytes(Paths.get("C:\\Users\\86176\\Desktop\\5概率与概率分布.txt")),StandardCharsets.UTF_8);
        List<String> words = List.of(con.split("\\PL+"));


        long count = 0;
        for(String w : words){
            if(w.length()>12) count++;
        }

        System.out.println(count);
        count = words.stream().filter(w->w.length()>12).count();
        System.out.println(count);

        count = words.parallelStream().filter(w->w.length()>12).count();
        System.out.println(count);
    }
}
