package zhang.com.afterClassWorks.streams;
class NodeT{
    int data;
    NodeT next;
    public NodeT(){
        next=null;
    }
    public NodeT(int e){
        data=e;
        next=null;
    }
}
class TList{
    NodeT head;
    NodeT tail;
    int size;

    public TList(){
        size=0;
        head=new NodeT();
        tail=new NodeT();
    }
    public void add(int i){
        NodeT newnode =new NodeT(i);
        newnode.next=head.next;
        head.next=newnode;
        size++;
       NodeT temp=head;
      for(int j=0;j<size;j++){
           temp=temp.next;
       }
      temp.next=head.next;
       tail.next=temp;
    }
    public void print(){
        NodeT temp=head;
        for(int k=0;k<size;k++){
            System.out.println(temp.next.data);
            temp=temp.next;
        }
    }
}

public class TwoToOneL {
    public static TList TwoToOne(TList La,TList Lb){
        La.tail.next.next=Lb.head.next;
        Lb.tail.next.next=La.head.next;
        La.size=La.size+Lb.size;
        La.tail=Lb.tail;
        Lb.head=La.head;
        return La;
    }
    public static void main(String []args){
        TList La = new TList();
        La.add(4);
        La.add(3);
        La.add(2);
        La.add(1);

        TList Lb = new TList();
        Lb.add(8);
        Lb.add(7);
        Lb.add(6);
        Lb.add(5);

        TList Lc=TwoToOne(La,Lb);
        Lc.print();

    }
}
