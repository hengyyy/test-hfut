package zhang.com.afterClassWorks.textFile;

public class DuiChenMatrix {

    public static void main(String []args){
        int map[][]={{1,6,1,1,1},
                {6,2,6,1,1},
                {1,6,3,6,1},
                {1,1,6,4,6},
                {1,1,1,6,5},};//5*5
        int n=5*(1+5)/2;
        int []arr=new int[n];
        arr[0]=map[0][0];
        for(int i=0;i<map.length;i++){//对称矩阵压缩存储
            for(int j=0;j<=i;j++){
                if(map[i][j]==map[j][i]){
                    arr[i*(i+1)/2+j]=map[i][j];
                }
            }
        }
        int m[][]=new int[5][5];
        for(int i=0;i<m.length;i++){
            for(int j=0;j<m[0].length;j++){
                int t=i*(i+1)/2+j;
                m[i][j]=arr[t];
                m[j][i]=arr[t];
            }
        }
        for(int i=0;i<m.length;i++){
            for(int j=0;j<m[0].length;j++){
                System.out.print(m[i][j]+"\t");
            }
            System.out.println();
        }
        System.out.println();
        for(int i=0;i<arr.length;i++){
            System.out.print(arr[i]+"\t");
        }
        System.out.println();
        System.out.println();
        for(int i=0;i<map.length;i++){
            for(int j=0;j<map[0].length;j++){
                System.out.print(map[i][j]+"\t");
            }
            System.out.println();
        }
    }
}