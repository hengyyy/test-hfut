package zhang.com.afterClassWorks.textFile;

public class test1 {
    public static int[][] compress(int arr[]){
        int n = 1;
        for(int i=0;i<arr.length-1;i++){//计算出数组有多少个不同的数字
            if(arr[i]!=arr[i+1])n++;
        }
        int compression[][]=new int[3][n];
        int temp=0;
        compression[0][0]=arr[0];
        compression[1][0]=0;
        for(int i=0;i<arr.length-1;i++){
            if(arr[i]!=arr[i+1]){
                temp++;
                compression[0][temp]=arr[i+1];
                compression[1][temp]=i+1;
                compression[2][temp-1]=i;
            }
        }
        compression[2][n-1]=arr.length-1;
        System.out.println();
        return compression;
    }

    public static void print(int a[][]){//打印二维数组
        for(int i=0;i<a .length;i++){
            if(i==0)System.out.print("data:\t");
            if(i==1)System.out.print("start:\t");
            if(i==2)System.out.print("end:\t");
            for(int j=0;j<a[0].length;j++){
                System.out.print(a[i][j]+"\t");
            }
            System.out.println();
        }
    }

    public static void main(String []args){
        int arr[]={1,1,1};

        int a[][]=compress(arr);
        print(a);
        //解压
        System.out.println();
        System.out.println("——————————————分割线——————————————");



    }
}