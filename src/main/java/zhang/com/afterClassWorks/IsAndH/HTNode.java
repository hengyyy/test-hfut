package zhang.com.afterClassWorks.IsAndH;

public class HTNode implements Comparable<HTNode>{
    public enum Code{
        ZERO('0'), ONE('1');
        private char code;
        private Code(char c){
            this.code = c;
        }
        public char getCode(){
            return code;
        }
    }
    //哈夫曼树的叶子结点数据
    private char data;
    //结点的编码，只有0和1两种可能
    private Code code;
    private double weight;
    private HTNode lchild;
    private HTNode rchild;
    private boolean isLeaf;
    //存放编码的字符串
    private String huffmancode = "";
    public String getHuffmancode() {
        return huffmancode;
    }
    public void setHuffmancode(String huffmancode) {
        this.huffmancode = huffmancode;
    }
    public char getData() {
        return data;
    }
    public void setData(char data) {
        this.data = data;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }
    public HTNode getLchild() {
        return lchild;
    }
    public void setLchild(HTNode lchild) {
        this.lchild = lchild;
    }
    public HTNode getRchild() {
        return rchild;
    }
    public void setRchild(HTNode rchild) {
        this.rchild = rchild;
    }
    public boolean isLeaf() {
        return isLeaf;
    }
    public void setLeaf(boolean isLeaf) {
        this.isLeaf = isLeaf;
    }
    public Code getCode() {
        return code;
    }
    public void setCode(Code code) {
        this.code = code;
    }

    @Override
    public int compareTo(HTNode o) {
        if(this.weight<o.weight){
            return -1;
        }else{
            return 1;
        }
    }
    @Override
    public String toString() {
        return "HTNode [data=" + data + ", code=" + code + ", weight=" + weight
                + ", lchild=" + lchild + ", rchild=" + rchild + ", isLeaf="
                + isLeaf + ", huffmancode=" + huffmancode + "]";
    }

    public  byte[] string2ByteArrays(String dataHuffmanCode){
        byte[] retBytes=null;

        char[] cs = dataHuffmanCode.toCharArray();
        int len = cs.length;
        int lenByte=0;
        //1. 判断整个串的长度是否能被8整除
        if(len%8==0){
            //2 能被整除的话，就8为作为一个byte
            lenByte =len/8+1;
            retBytes = new byte[lenByte];
            for(int i=0;i<lenByte-1;i++){
                String s="";
                for(int j=i*8;j<(i+1)*8;j++){
                    s+=cs[j];
                }
                retBytes[i]=this.chars2byte(s);
            }
            //设置不灵的个数
            retBytes[lenByte-1]=0;
        }else{
            //3 不能被8整除，最后一个字符串后面补0，使其成为8为，作为一个byte,同时要记录不灵的个数
            lenByte = len/8+2;
            retBytes=new byte[lenByte];
            int zeroNum=8-len%8;
            //补0
            for(int i=0;i<zeroNum;i++){
                dataHuffmanCode+="0";
            }
            //重新计算cahr数组
            cs=dataHuffmanCode.toCharArray();
            for(int i=0;i<lenByte-1;i++){
                String s="";
                for(int j=i*8;j<(i+1)*8;j++){
                    s+=cs[j];
                }
                retBytes[i]=this.chars2byte(s);

            }
            retBytes[lenByte-1]=(byte)zeroNum;
        }
        return retBytes;
    }

    public byte chars2byte(String s){
        byte ret =0;
        char[] cs = s.toCharArray();
        for(int i=0;i<cs.length;i++){
            byte tempB = (byte)(Byte.parseByte(""+cs[i])*Math.pow(2,cs.length-i-1));
            ret=(byte)(ret+tempB);
        }
        return ret;
    }

}
