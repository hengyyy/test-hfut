package zhang.com.afterClassWorks.IsAndH.isSymmetry;



public class Main {
    public static boolean isSymmetric(BinaryTree tree) {
        if (tree.root == null) return true;
        return isMirror(tree.root.getLeftChild(), tree.root.getRightChild());
    }
    public static boolean isMirror(Node A, Node B) {

        if (A == null && B == null) return true;

        if (A == null || B ==null)return false;

        if (A.data != B.data) return false;

        return isMirror(A.left, B.right) && isMirror(A.right, B.left);
    }

    public static void main(String[] args) {
        BinaryTree bt = new BinaryTree();
//        bt.insert(1);
//        bt.insert(2);
//        bt.insert(2);
//        bt.insert(3);
//        bt.insert(3);
//        bt.insert(3);
//        bt.insert(3);
        int arr[]={1,2,2,3};//,3,3,3,3
        for(int i=0;i<arr.length;i++){
            bt.insert(arr[i]);
        }
        bt.InfixOrder(bt.root);
        System.out.println(bt.root.data);
        System.out.println("是否是对称的："+isSymmetric(bt));
    }
}
