package zhang.com.afterClassWorks.IsAndH.isSymmetry;


import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
    //根节点
    public Node root;

    public BinaryTree(){
        root=null;
    }

    //插入节点
    public void insert(int key){//按层插入
        Queue<Node> q= new LinkedList<Node>();
        if(root==null){
            root=new Node(key);
            return;
        }
        Node newnode=new Node(key);
        q.add(root);
        //层序遍历，直到寻找到一个可用的位置插入节点
        while(!q.isEmpty()){
            root=q.peek();
            q.remove();
            if(root.left==null){
                root.left=newnode;
                break;
            }else{
                q.add(root.left);
            }
            if(root.right==null){
                root.right=newnode;
                break;
            }
            else{
                q.add(root.right);
            }
        }
    }
    public boolean Insert(int data) {
        Node newNode = new Node(data);
        if (root == null) {//当前树为空树，没有任何节点
            root = newNode;
            return true;
        } else {
            Node current = root;
            Node parentNode = null;
            while (current != null) {
                parentNode = current;
                if (current.getItem() > data) {//当前值比插入值大，搜索左子节点
                    current = current.getLeftChild();
                    if (current == null) {//左子节点为空，直接将新值插入到该节点
                        parentNode.setLeftChild(newNode);
                        return true;
                    }
                } else {
                    current = current.getRightChild();
                    if (current == null) {//右子节点为空，直接将新值插入到该节点
                        parentNode.setRightChild(newNode);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //中序遍历
    public void InfixOrder(Node current) {
        if (current != null) {
            InfixOrder(current.getLeftChild());
            System.out.println(current.getItem() + " ");
            InfixOrder(current.getRightChild());
        }
    }

}


