package zhang.com.afterClassWorks.IsAndH.isSymmetry;


public class Node {
    public Node(int value) {
        this.data = value;
    }

    public int getItem() {
        return data;
    }

    public void setItem(int item) {
        data = item;
    }

    public Node getLeftChild() {
        return left;
    }

    public void setLeftChild(Node leftChild) {
        this.left = leftChild;
    }

    public Node getRightChild() {
        return right;
    }

    public void setRightChild(Node rightChild) {
        right = rightChild;
    }

    public int data;
    public Node left;
    public Node right;

    @Override
    public String toString() {
        return "(" +
                  data +
                ", " + left +
                ", " + right +
                ')';
    }
}
