package zhang.com.NoteBooks.Tree.MyTree;
//代码你要自己写
//先序遍历：根左右
//中序遍历：左根右
//后序遍历：左右根

public class MyTree {
    public static void main(String []args){
        BinaryTree tree=new BinaryTree();

        tree.insert("A",40);
        tree.insert("B",20);
        tree.insert("C",10);
        tree.insert("D",30);
        tree.insert("E",25);
        tree.insert("F",27);
        tree.insert("G",35);
        tree.insert("H",50);
        tree.insert("I",45);
        tree.insert("J",47);

        System.out.println("先序遍历：");
        tree.priprint(tree.root);

        System.out.println();
        System.out.println("中序遍历：");
        tree.midprint(tree.root);

        System.out.println();

    }
}
