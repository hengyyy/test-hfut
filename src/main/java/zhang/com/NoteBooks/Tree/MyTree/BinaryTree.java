package zhang.com.NoteBooks.Tree.MyTree;

class BinaryTree<T>{
    Node root;

    public BinaryTree(){
        root = null;
    }

    public void insert(T data,int id){
        Node newnode=new Node(data,id);
        Node parent = null;
        if(root==null){
            root=newnode;
            return;
        }else {
            Node current = root;
            while (true){
                parent=current;
                if (id >= current.getId()) {
                    current = current.getR_child();
                    if (current== null){
                        parent.setR_child(newnode);
                        break;
                    }
                } else {
                    current=current.getL_child();
                    if(current==null){
                        parent.setL_child(newnode);
                        break;//狗屎return;
                    }
                }
            }
        }
        //左旋
//        if(root.RightHeight()-root.LeftHeight()>1){
//            Node temp=new Node(root.getData(),root.getId());
//            temp.l_child=root.l_child;
//            temp.r_child=root.r_child.l_child;
//           root.l_child=temp;
//            root.setData(root.r_child.getData());
//            root.setId(root.r_child.getId());
//            root.r_child=root.r_child.r_child;
//        }
        //右旋
//       if(LeftHeight()-r_height()>1){
//
//        }
    }
    public void priprint(Node node){//先序遍历
        if(node!=null){
            System.out.print(node.getData()+""+""+node.getId()+" ");
            priprint(node.getL_child());
            priprint(node.getR_child());
        }
    }

    public void midprint(Node node){//中序遍历
        if(node!=null){
            midprint(node.getL_child());
            System.out.print(node.getData()+""+""+node.getId()+" ");
            midprint(node.getR_child());
        }
    }
    public void lastprint(Node node){//后序遍历
        if(node!=null){
            lastprint(node.getL_child());
            lastprint(node.getR_child());
            System.out.println(node.getId()+" ");
        }
    }

    public Node getMinNode(){
        Node current=root;
        Node lastNode=null;
        while(current!=null){
            lastNode=current;
            current=current.getL_child();
        }
        return lastNode;
    }
    public Node getMaxNode(){
        Node current=root;
        Node lastNode=null;
        while(current!=null){
            lastNode=current;
            current=current.getR_child();
        }
        return lastNode;
    }


    public Node getRoot(){
        return root;
    }
}
