package zhang.com.NoteBooks.Tree.MyTree;

class Node<T>{
    private T data;
    private int id;
   Node<T> l_child;
   Node<T> r_child;

    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public Node getL_child() {
        return l_child;
    }
    public void setL_child(Node l_child) {
        this.l_child = l_child;
    }
    public Node getR_child() {
        return r_child;
    }
    public void setR_child(Node r_child) {
        this.r_child = r_child;
    }

    public int height() {//这段代码为何如此之简洁
        return Math.max(l_child == null?0:l_child.height(), r_child== null ?0 :r_child.height())+1;
    }
    public int RightHeight(){
        if(r_child==null){
            return 0;
        }
        return r_child.height()+1;
    }
    public int LeftHeight(){
        if(l_child==null){
            return 0;
        }
        return l_child.height()+1;
    }

    public Node(T data, int id){
        l_child=null;
        r_child=null;
        this.data=data;
        this.id=id;
    }
    public Node(){
        l_child = r_child = null;
        data=null;
        id=0;
    }
}