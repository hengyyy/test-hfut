package zhang.com.NoteBooks.Tree.AVLTree;


public class AVLTreeDemo {
    public static void main(String[] args) {

        int[] arr = {10, 11, 7, 6, 8,9,12,13};
        AVLTree avlTree = new AVLTree();
        //循环添加节点
        for (int i = 0; i < arr.length; i++) {
            avlTree.add(new Node(arr[i]));
        }

        System.out.println("树的高度：" + avlTree.getRoot().height());
        System.out.println("左子树的高度："+avlTree.getRoot().leftHeight());
        System.out.println("右子树的高度："+avlTree.getRoot().rightHeight());
        System.out.println("中序遍历：");
        avlTree.infixOrder();
    }
}

//创建AVL树
class AVLTree{
    private Node root;

    public Node getRoot() {
        return root;
    }

    //查找要删除的节点
    public Node search(int value){
        if (root == null){
            return null;
        }else {
            return root.search(value);
        }
    }

    //查找父节点
    public Node searchParent(int value){
        if (root == null){
            return null;
        }else{
            return root.searchParent(value);
        }
    }

    //编写一个方法,查找以传入节点为根节点的二叉排序树中的最小节点的值，并删除此节点
    public int delRightTreeMin(Node node){
        Node target = node;
        //循环查找左节点，就会找到最小值
        while(target.left != null){
            target = target.left;
        }
        //删除最小节点
        delNode(target.value);
        return target.value;
    }

    //删除节点
    public void delNode(int value){
        if (root == null){
            return;
        }else {
            //1.找到要删除的节点
            Node targetNode = search(value);
            if (targetNode == null){
                return;
            }
            //如果二叉排序树只有一个节点，并且删除的此节点
            if (root.left == null && root.right == null){
                root = null;
                return;
            }

            //找到父节点
            Node parent = searchParent(value);

            //如果删除的节点是叶子节点
            if (targetNode.left == null && targetNode.right == null){//第一种，删除叶子
                if (targetNode.value < parent.value){ //比较大小判断删除的左叶子还是右叶子
                    parent.left = null;
                }else {
                    parent.right = null;
                }
            }else if (targetNode.left != null && targetNode.right != null){//删除有两棵子树的(因为第二种判断比较麻烦，所以直接写在else里)
                //从target的右子树找到最小节点,
                //用一个临时变量存储这个最小节点的value
                //删除最小节点
                int min = delRightTreeMin(targetNode.right);
                //把最小节点的值赋给targetNode
                targetNode.value = min;
            }else {//第二种，只有一棵子树
                //如果要删除的节点有左子节点
                if (targetNode.left != null){
                    //如果只有根节点和一个叶子节点，要删除根节点，那就要判断根节点的父节点是否为空
                    if (parent != null) {
                        //如果targetNode是parent的左子节点
                        if (parent.left.value == value) {
                            parent.left = targetNode.left;
                        } else {//targetNode是parent的右子节点
                            parent.right = targetNode.left;
                        }
                    }else {
                        root = targetNode.left;
                    }
                }else {//要删除的节点有右子节点
                    if (parent != null) {
                        if (parent.left.value == value) {
                            //如果targetNode是parent的左子节点
                            parent.left = targetNode.right;
                        } else {//targetNode是parent的右子节点
                            parent.right = targetNode.right;
                        }
                    }else {
                        root = targetNode.right;
                    }
                }
            }
        }
    }
    //添加节点的方法
    public void add(Node node){
        if (root == null){
            root = node;
        }else {
            root.add(node);
        }
    }

    //中序遍历
    public void infixOrder(){
        if (root != null){
            root.infixOrder();
        }else {
            System.out.println("二叉排序树为空,无法遍历");
        }
    }
}

//创建节点
class Node{
    int value;
    Node left;
    Node right;

    public Node(int value) {
        this.value = value;
    }

    //返回左子树的高度
    public int leftHeight(){
        if (left == null){
            return 0;
        }
        return left.height();
    }
    //返回右子树的高度
    public int rightHeight(){
        if (right == null){
            return 0;
        }
        return right.height();
    }

    //右旋转方法
    private void rightRotate(){
        //创建新节点，值为当前节点节点的值
        Node newNode = new Node(value);
        //让当前节点的右子树挂到新节点上的右节点
        newNode.right = right;
        //让当前节点的左子节点的右子树挂到新节点的左节点
        newNode.left = left.right;
        //把当前节点的值改成左节点的值
        value = left.value;
        //当前节点的左节点执行左节点的左节点
        left = left.left;
        //把新节点挂到当前节点的右节点
        right = newNode;
    }

    //左旋转方法
    private void leftRotate(){
        //创建新的节点,以当前根节点的值
        Node newNode = new Node(value);
        //把新的节点的左子树设置成当前节点的左子树
        newNode.left = left;
        //把新的节点的右子树 设置成 当前节点的右子树的左子树
        newNode.right = right.left;
        //把当前节点的值换成当前节点的右子节点的值
        value = right.value;
        //把当前节点的右节点设置成右节点的右子树
        right = right.right;
        //把当前节点的左节点设置成新的节点
        left = newNode;
    }

    //返回当前节点的高度，以该节点为根节点的树的高度
    public int height(){
        return Math.max(left == null ? 0 : left.height(),right == null ? 0 : right.height()) + 1;
    }

    //查找要删除的节点
    public Node search(int value){
        if (value == this.value){//找到
            return this;
        }else if(value < this.value){//应该向左继续查找
            //如果左子节点为空就不能找了，说明不存在
            if (this.left == null) {
                return null;
            }
            return this.left.search(value);

        }else {
            if (this.right == null){
                return null;
            }
            return this.right.search(value);
        }
    }

    //查找要删除节点的父节点
    public Node searchParent(int value){
        if ((this.left != null && this.left.value == value)||
                (this.right != null && this.right.value == value)){
            return this;
        }else {
            //如果查找的值小于当前节点的值，并且当前节点的左子节点不为空
            if (value < this.value && this.left != null){
                return this.left.searchParent(value);//像左子树递归查找
            }else if (value >= this.value && this.right != null){
                return this.right.searchParent(value);//像右子树递归查找
            }else {
                return null; //没有父节点
            }
        }
    }
    //添加节点的方法
    //递归的形式添加节点，满足二叉排序树
    public void add(Node node){
        if(node == null){
            return;
        }

        if (node.value < this.value){
            //当前节点的左子节点为空直接挂上
            if (this.left == null){
                this.left = node;
            }else {//不为空递归向下
                this.left.add(node);
            }
        }else { //添加的节点的值大于等于当前节点的值，右节点为空，挂到右节点
            if (this.right == null){
                this.right = node;
            }else{//不为空就挂上
                this.right.add(node);
            }
        }
        //当添加完节点，当前节点的右子树的高度比左子树的高度差大于1，就左旋
        if (rightHeight() - leftHeight() > 1){
            //如果它的右子树的左子树高度大于它的右子树的右子树高度
            if (right != null && right.leftHeight() > right.rightHeight()){
                //右子树右旋
                right.rightRotate();
                //当前节点左旋
                leftRotate();
            }else {
                leftRotate(); // 左旋转
            }
            return; //平衡了就没必要继续走
        }
        //当前节点的左子树的高度比右子树的高度差大于1，就右旋
        if (leftHeight() - rightHeight() > 1){
            //如果它的左子树的右子树高度大于它的左子树的左子树的高度
            if (left != null && left.rightHeight() > left.leftHeight()){
                //先对当前节点的左子树进行左旋
                left.leftRotate();
                //在对当前节点右旋
                rightRotate();
            }else {
                //直接进行右旋转
                rightRotate();
            }
        }
    }

    //中序遍历
    public void infixOrder(){
        if (this.left != null){
            this.left.infixOrder();
        }

        System.out.println(this);

        if (this.right != null){
            this.right.infixOrder();
        }

    }
    @Override
    public String toString() {
        return "Node{" +
                "value=" + value +
                '}';
    }
}
