package zhang.com.NoteBooks.aboutWhile;

public class while11 {
    public static void a1(){
        int i=1;
        for(i=0;i<100;i++){
            while(i<10){
                i++;
                System.out.println(i);
                if(i==5)break;
            }
            break;//break的有无决定是否能跳出for循环。
        }
    }

    public static void a2(){//for循环的i++
        for(int i=0;i<10;i++){
            System.out.println(i);  //注销下输出2 4 6 8
            i++;
            //System.out.println(i);//注销上输出1 3 5 7 9
        }
    }
    public static void a3(){
        int i=0;
        while(i<10){
            i++;
            System.out.println(i);
        }
    }

    public static void a4(){
        for(int i=0;i<100;i++){
            if(i<10){
                i++;
                System.out.println(i);
                if(i==5)break;
            }
           // break;//break的有无决定是否能跳出for循环。
        }
    }

    public static void main(String []args){
        a4();
    }
}
