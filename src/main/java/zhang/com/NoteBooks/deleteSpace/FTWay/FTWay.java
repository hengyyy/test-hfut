package zhang.com.NoteBooks.deleteSpace.FTWay;

class Node{
    int x;
    int y;
    int data;
    Node up_child;
    Node down_child;
    Node lift_child;
    Node right_child;
    public Node(int x,int y,int data){
        up_child=null;
        down_child=null;
        right_child=null;
        lift_child=null;
        this.x=x;
        this.y=y;
        this.data=data;
    }
    public Node(){
        up_child=null;
        down_child=null;
        right_child=null;
        lift_child=null;
    }
    //创建节点

}

class Tree{

}


public class FTWay {
    public static Node[][] createNode(int arr[][]){
        int arrh=arr.length;//arr一维
        int arrw=arr[0].length;
        Node node[][]=new Node[arrh][arrw];
        for(int i=0;i<arrh;i++){
            for(int j=0;j<arrw;j++){
                node[i][j]=new Node(i,j,arr[i][j]);
            }
        }
        return node;

    }
    public static void createList(Node root,Node arr[][]){
        if(root.y>=1&&root.y<4&&root.x<3&&root.x>=0&&arr[root.x][root.y-1].data==0){
            root.up_child=arr[root.x][root.y-1];
            Node temp1=root.up_child;
            temp1.down_child=null;
            createList(temp1,arr);

        }else{
            root.up_child=null;
        }
        if(root.y>=0&&root.y<3&&root.x<3&&root.x>=0&&arr[root.x][root.y+1].data==0){
            root.down_child=arr[root.x][root.y+1];
            Node temp1=root.down_child;
            temp1.up_child=null;
            createList(temp1,arr);
        }else{
            root.down_child=null;
        }
        if(root.y>=0&&root.y<4&&root.x<3&&root.x>=1&&arr[root.x-1][root.y].data==0){
            root.lift_child=arr[root.x-1][root.y];
            Node temp1=root.lift_child;
            temp1.right_child=null;
            createList(temp1,arr);
        }else{
            root.lift_child=null;
        }
        if(root.y>=0&&root.y<4&&root.x<2&&root.x>=0&&arr[root.x+1][root.y].data==0){
            root.right_child=arr[root.x+1][root.y];
            Node temp1=root.right_child;
            temp1.lift_child=null;
            createList(temp1,arr);
        }else{
            root.right_child=null;
        }
    }

    public static void main(String []args){
        int map[][] = {
                {0,0,0,0},
                {0,1,0,0},
                {0,1,0,0}
        };//1是墙  0是路
        //0.2辅助地图 记录当前试探方向，记录有没有走过
        Node root;
        Node end;
        Node arr[][]=createNode(map);
        root=arr[2][0];//开始节点

        System.out.println(root.data);
        end=arr[2][2];//目标节点
        createList(root,arr);



        }

    }
