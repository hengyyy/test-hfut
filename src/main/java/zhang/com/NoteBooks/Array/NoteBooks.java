package zhang.com.NoteBooks.Array;

import static java.util.Arrays.binarySearch;

/**
 * fill()Arrays.fill(arr,4);//给所有值赋值4    Arrays.fill(arr, 2,4,6);//给第2位（0开始）到第4位（不包括）赋值6
 *
 * 	int[] arr = new int[5];//新建一个大小为5的数组
 * 	Arrays.fill(arr, 2,4,6);//给第2位（0开始）到第4位（不包括）赋值6
 * 	String str = Arrays.toString(arr); // Arrays类的toString()方法能将数组中的内容全部打印出来
 * 	System.out.print(str);
 * 	//输出：[0, 0, 6, 6, 0]
 *
 * Arrays.sort(); //数组排序
 *  Arrays.sort(strArray, Collections.reverseOrder());
 *
 *    int[] arr = {3,2,1,5,4};
 *     Arrays.sort(arr,0,3);//给第0位（0开始）到第3位（不包括）排序
 *     String str = Arrays.toString(arr); // Arrays类的toString()方法能将数组中的内容全部打印出来
 *     System.out.print(str);
 *     //输出：[1, 2, 3, 5, 4]
 * Arrays.toString(); //将数组中的内容全部打印出来
 *
 * 	int[] arr1 = {1,2,3};
 * 	int[] arr2 = {1,2,3};
 * 	System.out.println(Arrays.equals(arr1,arr2));
 * 	//输出：true
 * 	//如果是arr1.equals(arr2),则返回false，因为equals比较的是两个对象的地址，不是里面的数，而Arrays.equals重写了equals，所以，这里能比较元素是否相等。
 *
 * 	Arrays.binarySearch(); //二分查找法找指定元素的索引值（下标）
 * 		int[] arr = {10,20,30,40,50};
 * 	System.out.println(Arrays.binarySearch(arr, 30));
 *     //输出：2 （下标索引值从0开始）
 *
 *     Arrays.copeOf() 和Arrays.copeOfRange(); //截取数组
 *     	int[] arr = {10,20,30,40,50};
 * 	int[] arr1 = Arrays.copyOf(arr, 3);
 * 	String str = Arrays.toString(arr1); // Arrays类的toString()方法能将数组中的内容全部打印出来
 * 	System.out.print(str);
 * 	//输出：[10, 20, 30] （截取arr数组的3个元素赋值给新数组arr1）
 * 		int []arr = {10,20,30,40,50};
 * 	int []arr1 = Arrays.copyOfRange(arr,1,3);
 * 	String str = Arrays.toString(arr1); // Arrays类的toString()方法能将数组中的内容全部打印出来
 * 	System.out.print(str);
 * 	//输出：[20, 30] （从第1位（0开始）截取到第3位（不包括））
 */

public class NoteBooks {

    public void right(int a[][]){

    }
    public static void main(String []args) {

        int arr[] ={1,1,0,4};
    }


}
