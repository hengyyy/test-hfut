package zhang.com.NoteBooks.stacks;


public class PostInfix {
    public String Trensfer(String str){
        StringBuffer buffer =new StringBuffer();
        MyStacks ms = new MyStacks(50);
        //将字符串转换为字符数组
        char []cs=str.toCharArray();
        for(int i=0;i<cs.length;i++){
            char c=(char)cs[i];
            if(c=='+'||c=='-'){
                doOperation(ms,buffer,c,1);
            }
            else if(c=='*'||c=='/'){
                doOperation(ms,buffer,c,2);
            }
            else if(c=='('){
                ms.push(c);
            }
            else if(c==')'){
                doRightOperation(ms,buffer);
            }
            else{//
                buffer.append(c);
            }
        }
        while(!ms.isEmpty()){
            buffer.append((char)ms.pop());
        }
        return buffer.toString();
        //判断每一种字符的情况
    }
    public void doOperation(MyStacks ms,StringBuffer buffer,char c,int level) {

        while (!ms.isEmpty()) {
            char topC = (char) ms.pop();//强制转型

            if(topC=='('){
                ms.push(topC);
                break;
            }
            else {
                int topLevel = 0;
                if (topC == '+' || topC == '-') {
                    topLevel = 1;
                } else {
                    topLevel = 2;
                }
                if (topLevel >= level) {//c
                    buffer.append(topC);

                } else {
                    ms.push(topC);
                  break;
                }
            }
        }
        ms.push(c);
    }
    public void doRightOperation(MyStacks ms,StringBuffer buffer){
        while(!ms.isEmpty()){
            char topC =(char) ms.pop();
            if(topC=='('){
                break;
            }else{
                buffer.append(topC);
            }
        }
    }
        public static void main(String []args){

        PostInfix t = new PostInfix();
         String ret= t.Trensfer("2*5+6-5*(8-3)");
         System.out.println(ret);
        }
}
