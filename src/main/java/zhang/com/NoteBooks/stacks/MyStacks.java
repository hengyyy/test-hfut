package zhang.com.NoteBooks.stacks;

/**
 * 计算机后缀表达式
 * 将中缀表达式转换为后缀表达式，并计算其结果
 */

class MyStacks{
    private int data[];
    int top=-1;

    public int[] getData() {
        return data;
    }

    public void setData(int[] data) {
        this.data = data;
    }

    public MyStacks(int len){//初始化队列
        data=new int[len];
    }
    public void push(int e){ //入栈操作
            top++;
            data[top]=e;

    }
    public int pop(){    //出栈操作
            int e=data[top];
            top--;
            return e;
    }
    public int size(){
        return top+1;
    }
    public int peek(){
        return data[top];
    }
    public boolean isEmpty(){
        return top==-1;
    }
    public boolean isFull(){
        return top==data.length-1;
    }
    public void print(){
        for(int i=0;i<size();i++){
            System.out.println(data[i]);
        }
    }
}

