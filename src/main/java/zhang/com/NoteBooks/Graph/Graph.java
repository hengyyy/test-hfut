package zhang.com.NoteBooks.Graph;

//邻接矩阵

import java.util.ArrayList;
import java.util.Arrays;

public class Graph {
    private ArrayList<String> vertexList; // 存放顶点集合
    private int[][] edges;                // 矩阵
    private int num;                     // 边的数目

    public Graph(int n) {
        edges = new int[n][n];
        vertexList = new ArrayList<String>(n);
        num = 0;
    }

    // 插入节点
    public void addVer(String vertex) {
        vertexList.add(vertex);
    }

    // 添加边
    public void addNum(int v1, int v2, int weight) {
        // v1和v2指的是点的下标。weight表示权值
        edges[v1][v2] = weight;
        edges[v2][v1] = weight;
        num++;
    }

    // 返回节点的个数
    public int nodeNum() {
        return vertexList.size();
    }

    // 返回边一共有多少条
    public int edgeNum() {
        return num;
    }

    // 通过索引返回值
    public String getValue(int i) {
        return vertexList.get(i);
    }

    // 返回v1和v2的权值
    public int getWeight(int v1, int v2) {
        return edges[v1][v2];
    }

    // 显示图对应的矩阵
    public void show() {
        for (int[] link : edges) {
            System.err.println(Arrays.toString(link));
        }
    }

    public static void main(String[] args) {
        int n = 5; // 节点的个数
        String vertexString[] = { "A", "B", "C", "D", "F" };
        // 创建图对象
        Graph gp = new Graph(5);
        // 向图对象添加节点
        for (String value : vertexString) {
            gp.addVer(value);
        }
        // 添加边AB AC AD AE BC BD CE DE
        gp.addNum(0, 0, 0);
        gp.addNum(1, 1, 1);
        gp.addNum(2, 2, 2);
        gp.addNum(3, 3, 3);
        gp.addNum(4, 4, 4);
        gp.addNum(0, 4, 5);
        gp.addNum(1, 3, 6);
        gp.addNum(0, 2, 7);
        // 显示图的矩阵
        gp.show();
    }
}

