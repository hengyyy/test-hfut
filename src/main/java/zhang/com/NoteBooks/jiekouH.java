package zhang.com.NoteBooks;
import java.util.Scanner;

public class jiekouH {

        public static void main(String[] args) {
            // TODO Auto-generated method stub
            people p=new people();
            int i;
            Scanner input=new Scanner(System.in);
            Soundable[] a=new Soundable[3];
            a[0]=new Radio();
            a[1]=new Walkman();
            a[2]=new Mobilephone();
            System.out.println("请用户输入想要收听的设备：");
            i=input.nextInt();
            p.listen(a[i]);
            a[i].increaseVolume();
            a[i].decreaseVolume();
            a[i].stopSound();
        }

    }
    interface Soundable
    {
        void increaseVolume();
        void decreaseVolume();
        void stopSound();
        void playSound();
    }
    class Radio implements Soundable
    {
        public void increaseVolume()
        {
            System.out.println("增大收音机音量！");
        }
        public void decreaseVolume()
        {
            System.out.println("降低收音机音量！");
        }
        public void stopSound()
        {
            System.out.println("停止播放收音机！");
        }
        public void playSound()
        {
            System.out.println("开始播放收音机！");
        }
    }
    class Walkman implements Soundable
    {
        public void increaseVolume()
        {
            System.out.println("增大随身听音量！");
        }
        public void decreaseVolume()
        {
            System.out.println("降低随身听音量！");
        }
        public void stopSound()
        {
            System.out.println("停止播放随身听！");
        }
        public void playSound()
        {
            System.out.println("开始播放随身听！");
        }
    }
    class Mobilephone implements Soundable
    {
        public void increaseVolume()
        {
            System.out.println("增大手机音量！");
        }
        public void decreaseVolume()
        {
            System.out.println("降低手机音量！");
        }
        public void stopSound()
        {
            System.out.println("停止播放手机！");
        }
        public void playSound()
        {
            System.out.println("开始播放手机！");
        }
    }

    class people
    {
        public int age;
        public String name;
        public void listen(Soundable s)
        {
            s.playSound();
        }
    }

