package zhang.com.NoteBooks;

    /*泛型实现单链表*/
    class GenericLink<T> {
        public Entry<T> head;//定义头结点

        class Entry<T>{
            private T data;//数据
            private Entry<T> next;

            public Entry(){//初始化结点
                data = null;
                next = null;
            }
            public Entry(T val){
                data = val;
                next = null;
            }
        }
        GenericLink(){
            this.head = new Entry<T>();//初始化头结点
        }
        public void inserHead(T data){
            Entry<T> entry = new Entry<T>(data);
            entry.next = head.next;
            head.next = entry;
        }
        public void inserTail(T data){
            Entry<T> entry = new Entry<T>(data);
            Entry<T> var = head;
            while(var.next != null){
                var = var.next;
            }
            var.next = entry;
            entry.next = null;
        }
        public int getLength(){//获取长度
            Entry<T> var = head;
            int length = 0;
            while(var != null){
                length++;
                var = var.next;
            }
            return length;
        }
        public void inserPos(int pos,T data){//指定位置插入
            if(pos < 0 || pos > getLength()){
                return;
            }
            Entry<T> entry = new Entry<T>(data);
            Entry<T> var = head;
            for(int i = 0;i <= pos-1;i++){
                var = var.next;
            }
            entry.next = var.next;
            var.next = entry;


        }
        public void pop(T data){//删除
            Entry<T> entry = head;
            Entry<T> var = entry.next;
            while(var != null){
                if(var.data == data){
                    entry.next = var.next;
                    break;
                }
                entry = entry.next;
                var = entry.next;
            }
        }

        public void show(){
            Entry<T> tmp;
            tmp = head;
            //遍历打印链表
            while(tmp.next != null){
                System.out.println("data :"+tmp.next.data);
                tmp = tmp.next;
            }
        }
    }
    public class GenericLinkDemo {
        public static void main(String[] args) {
            GenericLink<Integer> l=new GenericLink<Integer>();
            l.inserHead(11);
            l.inserHead(17);
            l.inserTail(9);
            l.inserTail(18);
            l.inserPos(3,23);
            l.show();
            l.pop(13);
            System.out.println(l.getLength());
        }
    }

