package zhang.com.NoteBooks.String;

/**
 * int length() 返回此字符串的长度
 * char charAt(int index) 返回 char 指定索引处的值
 * boolean isEmpty() 判断是否是空字符串
 * String toLowerCase() 将 String 中的所有字符转换为小写
 * String toUpperCase() 将 String 中的所有字符转换为大写
 * boolean equalsIgnoreCase(String anotherString) 判断是否相等，忽略大小写
 * boolean equals(Object obj) 比较字符串的内容是否相同
 * String trim() 返回一个字符串，其值为此字符串，并删除任何前导和尾随空格
 * String concat(String str) 将指定的字符串连接到该字符串的末尾
 * String substring(int beginIndex) 返回一个字符串，该字符串是此字符串的子字符串
 * String substring(int beginIndex, int endIndex) 返回一个新字符串，它是此字符串从 beginIndex 开始截取到 endIndex（不包含）的一个子字符串
 * int compareTo(String anotherString) 按字典顺序比较两个字符串
 *
 * boolean contains(CharSequence s) 当且仅当此字符串包含指定的 char 值序列时才返回 true
 * int indexOf(String str) 返回指定子字符串第一次出现的字符串内的索引
 * int indexOf(String str, int fromIndex) 返回指定子串的第一次出现的字符串中的索引，从指定的索引开始
 * int lastIndexOf(String str) 返回指定子字符串最后一次出现的字符串中的索引
 * int lastIndexOf(String str, int fromIndex) 返回指定子字符串的最后一次出现的字符串中的索引，从指定索引开始反向搜索
 * boolean startsWith(String prefix) 测试此字符串是否以指定的前缀开头
 * boolean startsWith(String prefix, int toffset) 测试在指定索引处开始的此字符串的子字符串是否以指定的前缀开头
 * boolean endsWith(String suffix) 测试此字符串是否以指定的后缀结尾
 */


public class StringNoteBook {
    public static void main(String[] args) {

        String str1 = "yveshe";

        String str2 = "hello";

        System.gc();


        long startTime2 = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {

            str1 = str1 + str2;

        }

        long endTime2 = System.currentTimeMillis();

        System.out.println("+: " + (endTime2 - startTime2));

        /**
         * concat
         */

        System.gc();

        long startTime1 = System.currentTimeMillis();

        for (int i = 0; i < 10000; i++) {
            str1 = str1.concat(str2);
        }

        long endTime1 = System.currentTimeMillis();

        System.out.println("concat：" + (endTime1 - startTime1));



    }
}