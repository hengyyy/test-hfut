package zhang.com.NoteBooks.Object;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Object的常用方法
 * toString(); 将对象的引用变为字符串的
 *
 * Object拓展：
 * 语法糖（Syntactic Sugar），也称糖衣语法，是由英国计算机学家 Peter.J.Landin（彼得·兰丁）
 * 发明的一个术语，指在计算机语言中添加的某种语法，这种语法对语言的功能并没有影响，但是更方便程序员使用。
 * 语法糖就是对现有语法的一个封装。简而言之，语法糖让程序更加简洁，有更高的可读性。
 *
 * 语法糖的存在主要是方便开发人员使用。但是，Java虚拟机并不支持这些语法糖。
 * 这些语法糖在编译阶段就会被还原成简单的基础语法结构，这个过程就是解语法糖。
 *
 * Java 中最常用的语法糖主要有switch语句支持String与枚举、泛型和类型擦除、
 * 自动装箱与拆箱、方法边长参数、枚举、内部类、条件编译、断言、数值字面量、
 * 增强for循环、try-with-resources语句、Lambda表达式等。
 * 本文主要来分析下这些语法糖背后的原理。一步一步剥去糖衣，看看其本质。
 */
import java.util.Optional;


interface Person{
    public void go();

}

class a1 implements Person{

    @Override
    public void go() {
        System.out.println("a1");
    }
}

class a2 implements Person{

    @Override
    public void go() {
        System.out.println("a2");
    }
}

public class A {

    public static void main(String[] args) {

        Person p1 = new a1();
        p1.go();
        Person p2 = new a2();
        p2.go();



    }
}