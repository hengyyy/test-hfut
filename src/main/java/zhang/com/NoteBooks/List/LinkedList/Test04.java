package zhang.com.NoteBooks.List.LinkedList;

public class Test04 implements Runnable{

    private int ticketNums = 10;

    public void run(){
        while(true){
            if(ticketNums<=0){
                break;
            }
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(Thread.currentThread().getName()+"拿到了第"+ticketNums--+"票");
        }
    }

    public static void main(String []args){
        Test04 t = new Test04();
        new Thread(t,"小明").start();
        new Thread(t,"老师").start();
        new Thread(t,"黄牛").start();
    }
}
