package zhang.com.NoteBooks.first;


 class List<T>{

     class Node<T>{
         T data;
         Node<T> next;
         Node<T> prior;
         public Node(){

             next=null;
             prior=null;

         }
         public Node(T e){
             this.data = e;
             this.next=null;
             this.prior=null;
         }
     }
     Node<T> Head;
     public List(){
         this.Head = new Node();
         Head.next=null;
         Head.prior=null;
     }

     //头茬法
     public void insertList(T e){
         Node<T> newNode = new Node<T>(e);
         newNode.next=Head.next;
         Head.next=newNode;
     }

     //遍历链表
     public void print(){
         Node<T> temp = Head;
         while(temp.next!=null){
             System.out.println(temp.next.data);
             temp=temp.next;
         }
     }


}

public class lianLiao1 {

     public static<A> void print1(A content){
         System.out.println(content);
     }


    public static void main(String []args){
        List<Integer> L = new List<>();
        List<String> L1 = new List<>();
        L.insertList(1);
        L.insertList(2);
        L.insertList(3);
        L.insertList(4);
        L1.insertList("a");
        L1.print();
        L.print();

        }
    }
