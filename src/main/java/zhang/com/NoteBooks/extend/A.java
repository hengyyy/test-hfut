package zhang.com.NoteBooks.extend;

/**
 * 向上转型和，向下转型
 */
class Father{
    String name="父类";

    void sleep(){
        System.out.println(name+"睡觉");
              }
        }

        class Son extends Father{
            String name = "子类";
            void song(){
                System.out.println(name+"唱歌");
            }
        }

public class A {
    public static void main(String []args){
            Father a = new Son();   //a是类对象，只有向上转型之后，才能向下转型
            a.sleep();
            System.out.println(a instanceof Father);

              ((Son)a).song();        //实现向下转型,将父类类型转换成子类类型
    }
}
