package zhang.com.NoteBooks.StringBuffer;


/**StringBuffer的常用方法：
 * append() 方法
 * toString() 转换为字符串
 *deleteCharAt()删除指定索引的字符
 * insert()在指定的位置插入指定的数据
 * reverse()将顺序反置
 * setCharAt()替换
 */

public class NoteBooks {

    public static void main(String []args){

        StringBuffer buffer=new StringBuffer();
        buffer.append(1);
        buffer.append(2);
        buffer.append(3);
        buffer.append(4);
        buffer.append(5);
        buffer.deleteCharAt(1);
        buffer.insert(2,4);
        buffer.setCharAt(1,'6');
        buffer.reverse();
        System.out.println(buffer.toString());


    }

}
