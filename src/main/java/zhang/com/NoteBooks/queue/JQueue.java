package zhang.com.NoteBooks.queue;


import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

/**NoteBooks
 * add()   offer()
 * 区别：add,如果队列的容量已满，则会抛出异常IllegalStateException异常。
 *      offer，如果队列已满则不会抛出异常，而是返回false。
 *
 * remove()  poll()  移除并返问队列头部的元素
 * 区别：队列为空时，poll方法返回null,remove则会抛出异常NoSuchElementException,需要用try-catch语句来处理异常。
 *
 * element()  peek()  peek       返回队列头部的元素
 *
 * PriorityQueue()  PriorityQueue  不是线程安全的
 * 优先队列中元素默认排列顺序是升序排列。
 *
 * boolean add（object）：将指定的元素插入此优先级队列。
 * boolean offer（object）：将指定的元素插入此优先级队列。
 * Comparator comparator（）：返回用于对此队列中的元素进行排序的比较器，如果此队列根据其元素的自然顺序排序，则返回null。
 * clear()
 * contains()
 * iterator()
 * offer()
 * toArray()
 */

class User{
    public final String name;
    public final String number;

    public User(String name ,String number){
        this.name=name;
        this.number=number;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}

public class JQueue  {
    class UserComparator implements Comparator<User>{
        public int compare(User u1 , User u2){
            if(u1.number.charAt(0)==u1.number.charAt(0)){
                //如果两个人的号都是A开头或者V开头，比较号的大小
                return u1.number.compareTo(u2.number);
            }
            if(u1.number.charAt(0)=='V'){
                return -1;

            }else{
                return 1;
            }
        }
    }
    //
    public static void main(String []args){
        Queue<Integer> q2 =new PriorityQueue<>();

        q2.add(2);
        q2.add(1);
        q2.add(4);
        q2.add(3);

        while(true){
            Integer p=q2.poll();
            if(p==null)break;
            System.out.println(p);
        }

        User u1 = new User("z","A1");
        User u2 = new User("z","V3");
        User u3 = new User("z","V1");
        Queue<User> q1 =new PriorityQueue<User>();

        q1.offer(u2);
        q1.offer(u1);
        q1.offer(u3);

        while(true){
            User p=q1.poll();
            if(p==null)break;
            System.out.println(p);
        }

    }
}
