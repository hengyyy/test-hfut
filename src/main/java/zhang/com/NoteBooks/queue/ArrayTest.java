package zhang.com.NoteBooks.queue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class ArrayTest {
    public static void main(String []args){
        String str = "hello,world";


        //Optional.ofNullable(str).map(String::toUpperCase).ifPresent(System.out::println);
//        Optional.of(str).map(String::toUpperCase).ifPresent(System.out::println);
//        Optional.of(str).map(String::toUpperCase).ifPresent(System.out::println);

        Optional.ofNullable(str).ifPresent(System.out::println);
        Optional.ofNullable(str).ifPresent(s->System.out.println(s));

    }

}
