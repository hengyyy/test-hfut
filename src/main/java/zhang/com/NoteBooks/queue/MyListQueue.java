package zhang.com.NoteBooks.queue;

/**
 * 链式队列
 */
class Node{
    int data;
    int arr[]=new int[5];
    Node prior;
    Node next;
    public Node(){
        prior=next=null;

    }
    public Node(int e){
        data=e;
        next=null;
        prior=null;
    }
    //pubilc Node()
}
public class MyListQueue {
    Node head;
    Node tail;
    int size;
    public MyListQueue(){
        size=0;
        head=new Node();
        tail=new Node();
        head.next=tail;
        tail.prior=head;
    }
    public void push(int e){
        Node newnode=new Node(e);   //画图更清晰
        tail.prior.next=newnode;
        newnode.prior=tail.prior;
        newnode.next=tail;//尾插法
        tail.prior=newnode;

        size++;
    }
    public int pop(){
        int ret=head.next.data;
        head.next=head.next.next;
        size--;
        return ret;
    }

    public void print(){
        Node temp=head.next;
        while(temp!=tail){
            System.out.println(temp.data);
            temp=temp.next;
        }
    }
    public static void main(String []args){
        MyListQueue queue =new MyListQueue();
        queue.push(1);
        queue.push(2);
        queue.push(3);
        queue.push(4);
        queue.print();
//        System.out.printf("queue的size=%d\n",queue.size);
        System.out.println("====================");
        queue.pop();
        queue.print();
    }

}
