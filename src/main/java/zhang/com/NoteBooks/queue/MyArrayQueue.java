package zhang.com.NoteBooks.queue;

/**
 * RuntimeException("栈满");
 *
 */
public class MyArrayQueue {
    int prior;
    int data[];
    int basic;
    public MyArrayQueue() {
        data = new int[5];
        prior = 0;
        basic = 0;
    }
    public void push(int e) {
        if ((prior+1)%5 == basic) throw new IllegalArgumentException("栈满");
        data[prior] = e;
        prior = (prior + 1) % 5;
    }
    public int pop() {
        if(prior==basic) throw new IllegalArgumentException("栈为空");
        int ret = data[basic];
        basic = (basic + 1) % 5;
        return ret;
    }
    public int size() {
        return (5-basic+prior)%5;
    }
    public void print() {
            for (int i = basic; i <basic+(5-basic+prior)%5; i++) {
                System.out.println(data[i%5]);
            }
    }
    public static void main(String []args){
        MyArrayQueue arrqueue=new MyArrayQueue();
        arrqueue.push(1);
        arrqueue.push(2);
        arrqueue.push(3);
        arrqueue.push(4);
        arrqueue.pop();
        System.out.println(arrqueue.prior);
        System.out.println(arrqueue.basic);
        System.out.println("打印！！！"+arrqueue.size());
        arrqueue.print();
    }
}
