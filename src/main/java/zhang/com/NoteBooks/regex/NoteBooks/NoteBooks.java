package zhang.com.NoteBooks.regex.NoteBooks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**正则表达式
 * patern类
 *  //将规则封装成对象。
 *  Pattern p = Pattern.compile(regex);
 *
 * matcher类
 *  //让正则对象和要作用的字符串相关联。获取匹配器对象。
 *         Matcher m  = p.matcher(str);
 *
 * 匹配  matches（）方法   "[1-9]\\d{4,14}" 开头是1~9，后面有4~14个数字
 * 切割  split()方法 “ +”是按照多个空格来切割
 *
 * 替换 replaceAll(regex,replacement)    "\\d{5,}";//String regex = "\\d{5,}";用于匹配至少包含5个数字的字符串
 *
 *  String regex = "\\b[a-z]{3}\\b";//匹配只有三个字母的单词
 *
 *  find() 方法
 *  group() 方法
 *  //将规则封装成对象。
 *  Pattern p = Pattern.compile(regex);
 *
 */


/**
 * String str = "mo yu xie bo ker";
 *
 *         String regex = "\\b[a-z]{3}\\b";//匹配只有三个字母的单词
 *
 *         //将规则封装成对象。
 *         Pattern p = Pattern.compile(regex);
 *
 *         //让正则对象和要作用的字符串相关联。获取匹配器对象。
 *         Matcher m  = p.matcher(str);
 *
 *         //将规则作用到字符串上，并进行符合规则的子串查找。
 *         boolean b = m.find();
 *
 *         while(b)
 *         {
 *             //获取匹配后结果。
 *             System.out.println(m.group());
 *             // start()  字符的开始下标（包含）
 *             //end()  字符的结束下标（不包含）
 *             System.out.println(m.start()+"...."+m.end());
 *         }
 * */
public class NoteBooks {

    public static void main(String []args){


  String str = "mo yu xie bo ker";

          String regex = "\\b[a-z]{3}\\b";//匹配只有三个字母的单词

       //将规则封装成对象。
          Pattern p = Pattern.compile(regex);
          // Pattern.compile("((13\\d)|(15\\d))\\d{8}").matcher(str1);
          //让正则对象和要作用的字符串相关联。获取匹配器对象。
          Matcher m  = p.matcher(str);

          //将规则作用到字符串上，并进行符合规则的子串查找。
          while(m.find())
          {
              //获取匹配后结果。
              System.out.println(m.group());
              // start()  字符的开始下标（包含）
              //end()  字符的结束下标（不包含）
              System.out.println(m.start()+"...."+m.end());
          }

        // 使用字符串模拟从网络上得到的网页源码
        String str1 = "出售JAVA教程，联系电话：13600000001" + "毕业代做，联系电话：13600000002" + "出售二手电脑，联系电话：15800000001";
        // 创建一个Pattern对象，并用它建立一个Matcher对象
        // 该正则表达式只抓取13X和15X段的手机号
        // 实际要抓取哪些电话号码，只要修改正则表达式即可
        Matcher m1 = Pattern.compile("((13\\d)|(15\\d))\\d{8}").matcher(str1);
        /**"((13\\d)|(15\\d))\\d{8}"(13d)：匹配以13开头，后面跟着一个数字的字符串；
         |：或者；
         (15\d)：匹配以15开头，后面跟着一个数字的字符串；
         \d{8}：匹配8个数字。*/
        // 将所有符合正则表达式的子串（电话号码）全部输出
        while (m1.find())
        {
            System.out.println(m1.group());
        }
    }
}
