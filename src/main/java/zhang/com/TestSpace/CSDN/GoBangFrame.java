package zhang.com.TestSpace.CSDN;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

//游戏窗口类
public class GoBangFrame extends JFrame{

    private GoBangPanel panel;
    private JButton nudo;
    private JButton newGame;
    private JLabel label1,label2;
    private JPanel panel4,panel5,panel6;
    private JRadioButton robot,radioButton4,first;
    private ButtonGroup buttonGroup3;
    private JComboBox<Integer> depth,nodeCount;
    //启动游戏窗口
    public void start(){

        panel=new GoBangPanel();
        add(panel,BorderLayout.WEST);

        JPanel rightPanel=new JPanel();
//设置垂直布局
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));


//单选框
        robot=new JRadioButton("人机对战");
        robot.setSelected(true);
//单选框
        radioButton4=new JRadioButton("估值函数+搜索树");
        radioButton4.setSelected(true);

//搜索树
        panel4=new JPanel();
        panel4.setBorder(new TitledBorder("搜索树"));
        label1=new JLabel("搜索深度");
        depth=new JComboBox<>(new Integer[] {2});
        label2=new JLabel("每层节点");
        nodeCount=new JComboBox<>(new Integer[] {2});

        panel4.add(label1);
        panel4.add(depth);
        panel4.add(label2);
        panel4.add(nodeCount);
        rightPanel.add(panel4);

//其他
        panel5=new JPanel();
        panel5.setBorder(new TitledBorder("其他"));
        nudo=new JButton("悔棋");
        newGame=new JButton("新游戏");

        nudo.addMouseListener(mouseListener);
        newGame.addMouseListener(mouseListener);

        panel5.add(nudo);
        panel5.add(newGame);
        rightPanel.add(panel5);

//人机模式
        panel6=new JPanel();
        panel6.setBorder(new TitledBorder("人机模式"));
//单选框
        first=new JRadioButton("人类先手");
        first.setSelected(true);

//单选框互斥
        buttonGroup3=new ButtonGroup();
        buttonGroup3.add(first);

        panel6.add(first);

        rightPanel.add(panel6);

        add(rightPanel);

//游戏窗口设置
        setSize(GoBangContantes.GAME_WIDTH,GoBangContantes.GAME_HEIGHT);
        setLocation(200, 200);
        setTitle("五子棋");
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    //监听器
    private MouseListener mouseListener=new MouseListener() {

        @Override
        public void mouseReleased(MouseEvent e) {
// TODO Auto-generated method stub

        }

        @Override
        public void mousePressed(MouseEvent e) {
// TODO Auto-generated method stub

        }

        @Override
        public void mouseExited(MouseEvent e) {
// TODO Auto-generated method stub

        }

        @Override
        public void mouseEntered(MouseEvent e) {
// TODO Auto-generated method stub

        }

        @Override
        public void mouseClicked(MouseEvent e) {
            Object object=e.getSource();
            if(object==nudo){
                if(robot.isSelected())
                    panel.huiqi();
            }else if(object==newGame){
                panel.newGame();
            }
        }
    };

}
