package zhang.com.TestSpace.Javaextends;


class BinaryGrade extends Grade{
    private String biscore;

    public String getBiscore() {
        return biscore;
    }

    public void setBiscore(String biscore) {
        this.biscore = biscore;
    }
}

class FiveGrade extends Grade{
    private String fscore;

    public String getFscore() {
        return fscore;
    }

    public void setFscore(String biscore) {
        this.fscore = biscore;
    }
}

public class TestInherit {
    public static void main(String[] args) {
        Grade cgrade = new Grade();
        cgrade.setScore(50.0f);
        System.out.println(cgrade.getScore());

        BinaryGrade egrade = new BinaryGrade();
        egrade.setBiscore("及格");
        System.out.println(egrade.getBiscore());
        FiveGrade fgrade = new FiveGrade();
        fgrade.setFscore("良好");
        System.out.println(fgrade.getFscore());
    }
}
