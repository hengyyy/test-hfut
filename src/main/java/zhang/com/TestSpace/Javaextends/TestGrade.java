package zhang.com.TestSpace.Javaextends;

public class TestGrade {
    public static void main(String[] args) {
        Grade grade;
        grade = new Grade("01","高数",6,Grade.CENTI_SCALE,89);
        System.out.println(grade.getScore());
        grade = new Grade("02","IT",3,Grade.BINARY_SCALE,1);
        System.out.println(grade.getScore());
        grade = new Grade("03","Java",4,Grade.FIVE_SCALE,3);
        System.out.println(grade.getScore());
    }
}
