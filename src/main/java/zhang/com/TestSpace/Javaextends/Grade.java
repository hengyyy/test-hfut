package zhang.com.TestSpace.Javaextends;


public class Grade {
    public final static int CENTI_SCALE = 1;
    public final static int BINARY_SCALE = 2;
    public final static int FIVE_SCALE = 3;
    private String sid;
    private String cname;
    private float credit;
    private int gType;
    private float score;

    public Grade(String sid, String cname, float credit, int gType, float score) {
        this.sid = sid;
        this.cname = cname;
        this.credit = credit;
        this.gType = gType;
        this.score = score;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public int getgType() {
        return gType;
    }

    public void setgType(int gType) {
        this.gType = gType;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Grade() {
    }


    public String getScore() {
        String result = String.valueOf(score);
        switch (gType) {
            case CENTI_SCALE:
                result = Float.toString(score);
                break;
            case BINARY_SCALE:
                if(score==0){
                    result="不及格";
                }else {
                    result = "及格";
                }
                break;
            case FIVE_SCALE:
                if(score==0){
                    result="不及格";
                }else if(score==1){
                    result="及格";
                }else if(score==2){
                    result="中";
                } else if (score == 3) {
                    result="良";
                } else if (score == 4) {
                    result="优秀";
                }
                break;
        }
        return result;
    }
}
