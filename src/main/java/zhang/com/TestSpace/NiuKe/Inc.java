package zhang.com.TestSpace.NiuKe;

public class Inc {
    public static void main(String[] args) {
        Inc inc = new Inc();
        int a = 0;
        inc.fermin(a);
        a = a++;   //字节指令码有必要看嘛？
        System.out.println("a:"+a);

    }
    void fermin(int i){
        i++;
        System.out.println("i:"+i);
    }
}

/*
* 以下是i= i ++;字节码指令:
15: iload_2       //将i从 局部变量表 加载到 栈顶, i = 0
16: iinc     2, 1  //将 局部变量表 中的i 加1, 局部变量表中变为1
19: istore_2      //将栈顶的0 存回到 局部变量表, i变回0

作者：Tothink
链接：https://www.nowcoder.com/exam/test/79161595/submission?pid=56259188&pageSource=testHistory
来源：牛客网
* */
