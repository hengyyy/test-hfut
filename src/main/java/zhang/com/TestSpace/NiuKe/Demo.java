package zhang.com.TestSpace.NiuKe;

import java.util.*;

public class Demo {
    public static void main(String[] args) {
        Collection<?>[] collections =
                {new HashSet<String>(), new ArrayList<String>(), new HashMap<String, String>().values()};
        Super subToSuper = new Sub();
        for(Collection<?> collection: collections) {
            System.out.println(subToSuper.getType(collection));
        }
    }
    abstract static class Super {
        public String getType(Collection<?> collection) {
            return "Super:collection"+"：1";
        }
        public String getType(List<?> list) {
            return "Super:list";
        }
        public String getType(ArrayList<?> list) {
            return "Super:arrayList";
        }
        public String getType(Set<?> set) {
            return "Super:set";
        }
        public String getType(HashSet<?> set) {
            return "Super:hashSet";
        }
    }
    static class Sub extends Super {
        public  String getType(Collection<?> collection) {
            return "Sub"; }
    }
}
