package zhang.com.TestSpace.NiuKe;

public class Test {
    private String name = "abc";
    public static void main(String[] args) {
        Test test = new Test();
        Test testB = new Test();
        String result = test.equals(testB) + ",";
        result += test.name.equals(testB.name) + ",";
        result += test.name == testB.name;
        System.out.println(result);

        /*
 首先test.equals(testB)，Test类并没有重写equals方法，那么在比较时，
 调用的依然是Object类的equals()方法，比较的是两个对象在内存中的地址，结果为false。
test.name.equals(testB.name)比较name字符串是否相等，二者都是"abc"，结果返回true。
name成员变量并没有被static关键字修饰，那么每个Test类的对象被创建时，
都会先执行private String name = "abc";（注意这里使用了直接赋值的方式，
没有使用new关键字）。在test对象初始化时，JVM会先检查字符串常量池中是否存在该字符串的对象，
此时字符串常量池中并不存在"abc"字符串对象，JVM会在常量池中创建这一对象。当testB被创建时，
同样会执行private String name = "abc";，那么此时JVM会在字符串常量池中找到test对象初始化时创建的"abc"字符串对象，
那么JVM就会直接返回该字符串在常量池中的内存地址，而不会新建对象。也就是说test.name和testB.name指向的内存是同一个，
那么test.name == testB.name返回true。本题正确选项为false，true，true。题目要求选出错误选项，答案为A B C。
*/
    }
}
