package zhang.com.TestSpace.Table;
import javax.swing.*;
public class table {
        //创建一个JFrame窗口
        JFrame jf=new JFrame("生命游戏");
        //声明JTable类型的变量table
        JTable table;
        //定义一维数组作为标题
        Object[] columnTitle={"姓名","年龄","性别"};
        Object[][] tableDate={
                new Object[] {"nb" ,20,"男"},
                new Object[] {"波斯客",25,"男"},
                new Object[] {"李小白",20,"男"},
                new Object[] {"张小骞",20,"男"},
                new Object[] {"唐小妃",20,"男"},
                new Object[] {"卫小青",20,"男"},
                new Object[] {"韩小信",20,"男"}
        };
        //使用JTable对象创建表格
        public void init(){
            //用二维数组和一维数组来创建一个JTable对象
            table=new JTable(tableDate,columnTitle);
            //将JTable对象放在JScrollPane中，并将该JScrollPane放在窗口中显示出来
            jf.add(new JScrollPane(table));
            //设置自适应JFrame窗口的大小
            jf.pack();
            //单击关闭按钮时默认为退出
            jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            jf.setVisible(true);
        }
        public static void main(String[] args) {
            new table().init();
        }

}
