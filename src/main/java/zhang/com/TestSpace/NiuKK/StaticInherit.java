package zhang.com.TestSpace.NiuKK;

import java.util.*;

public class StaticInherit {
    public static void main(String[] args) {
        Collection<?>[] collections =
                {new HashSet<String>(), new ArrayList<String>(), new HashMap<String, String>().values()};
        System.out.println("静态绑定，看编译类型(左边)");

        System.out.println("----------> 通过对象调用方法 <--------------");
        System.out.println("左边为父类，调用父类");
        Super subToSuper = new Sub();
        for(Collection<?> collection: collections) {
            System.out.println(subToSuper.getType(collection));
        }

        System.out.println("-------------------------");
        System.out.println("左边为子类，调用子类");
        Sub sub = new Sub();
        for(Collection<?> collection: collections){
            System.out.println(sub.getType(collection));
        }

        System.out.println("----------> 通过类调用方法 <--------------");
        System.out.println("Super调用方法");
        for(Collection<?> collection: collections){
            System.out.println(Super.getType(collection));
        }
        System.out.println("---------------------");
        System.out.println("Sub调用方法");
        for(Collection<?> collection: collections){
            System.out.println(Sub.getType(collection));
        }

        System.out.println("------------> 通过对象调用属性 <-------------");
        System.out.println(subToSuper.name);
        System.out.println(sub.name);
        System.out.println("------------> 通过类调用属性 <-------------");
        System.out.println(Super.name);
        System.out.println(Sub.name);
    }

    abstract static class Super {
        static String name = "Super";

        public static String getType(Collection<?> collection) {
            return "Super:collection";
        }
        public static String getType(List<?> list) {
            return "Super:list";
        }
        public String getType(ArrayList<?> list) {
            return "Super:arrayList";
        }
        public static String getType(Set<?> set) {
            return "Super:set";
        }
        public String getType(HashSet<?> set) {
            return "Super:hashSet";
        }
    }

    static class Sub extends Super {
        static String name = "Sub";

        public static String getType(Collection<?> collection) {
            return "Sub"; }
    }
}
