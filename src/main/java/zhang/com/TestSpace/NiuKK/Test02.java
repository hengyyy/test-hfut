package zhang.com.TestSpace.NiuKK;



public class Test02 {// ClassD 为外部类，可以没有 public , 且只能带上 public， 不可以使用 protected,private 等其他

    // public， protected
    private static final class E {// E 为成员内部类, 可以加上 public，protected，private， 等和 static final

    }

    public static void main(String[] args) {

        class G {// 局部内部类

        }

    }

    public void test() {
        final class H {// 局部内部类 ， 不可以加上 static 和 public，protected，private 等, 可以带上 final

        }
    }
}
