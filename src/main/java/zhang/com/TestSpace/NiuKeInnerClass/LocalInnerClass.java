package zhang.com.TestSpace.NiuKeInnerClass;

interface Animal{
    void eat();
    void sleep();
}

class OuterC {
    private int count;
    public Animal getAnimal (){
        final int count = 1000;
        class Person implements Animal{

            @Override
            public void eat() {
                System.out.println(count);
                System.out.println(OuterC.this.count);
                System.out.println("person eat");
            }


            @Override
            public void sleep() {
                System.out.println("person eat");
            }
        }
        return new Person();
    }
}
public class LocalInnerClass {
    public static void main(String[] args) {
        OuterC outerC = new OuterC();
        Animal a = outerC.getAnimal();
        a.eat();
        a.sleep();
    }
}
