package zhang.com.TestSpace.NiuKeInnerClass;

abstract class Animal1 {
    public abstract void eat();
    public abstract void sleep();
}

class Zoo{
    public Animal getAnimal() {
        return new Animal() {
            @Override
            public void eat() {
                System.out.println("Animal eat");
            }

            @Override
            public void sleep() {
                System.out.println("Animal sleep");
            }
        };
    }
}
public class AnonymousInnerClass {
    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        Animal animal = zoo.getAnimal();
        animal.eat();
        animal.sleep();
    }
}
