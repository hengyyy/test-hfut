package zhang.com.TestSpace.NiuKeInnerClass;


class OuterA {
    private String name;
    private static int count = 10;
    static class InnerA {
        int count = 100;
        public void print(int count){
            System.out.println(count);
            System.out.println(this.count);
            System.out.println(OuterA.count);
        }
    }
}
public class StaticInnerClass {
    public static void main(String[] args) {
        OuterA.InnerA innerA = new OuterA.InnerA();
        innerA.print(1000);
    }




}