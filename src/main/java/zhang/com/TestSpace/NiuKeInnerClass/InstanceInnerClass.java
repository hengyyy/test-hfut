package zhang.com.TestSpace.NiuKeInnerClass;

class OuterB {
    private int s = 10;
    public class InnerB {                       //内部类InnerB为实例内部类
        int s =100;
        public void print(int s){
            System.out.println(s);              //访问同名局部类
            System.out.println(this.s);         //访问同名实例变量
            System.out.println(OuterB.this.s);  //访问同名外部类实例变量
        }
    }

    //该方法返回内部类实例
    public InnerB getInnerB() {
        return new InnerB();
    }
}


public class InstanceInnerClass {
    public static void main(String[] args) {
        //外部类的外部直接使用实例内部类
        OuterB.InnerB inner = (new OuterB()).new InnerB();
        inner.print(1000);
        //通过外部类的方法返回内部类实例
        OuterB outerB = new OuterB();
        outerB.getInnerB().print(1000);
    }
}


