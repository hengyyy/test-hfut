package zhang.com.TestSpace.NiuKe01;

public class Test01 {
    public Test01(String s){
        System.out.println("Parent");
    }

    public Test01(){
        System.out.println("无参");
    }
}

class Child extends Test01{

    public Child(String s) {
        //super(s);
        System.out.println("Child");
    }

    public static void main(String[] args) {
        new Child("Hello World");
    }
}
