package zhang.com.TestSpace.NiuKe01;

class B extends Test04{
    @Override
    public Test04 foo() {
        return this;
    }
}

class C extends B{

//    public void foo(){}  //方法重写返回值类型不正确
}
public class Test04 {
    String ele;
    public Test04 foo(){
        ele = "e";
        return this;
    }



    public static void main(String[] args) {
        Test04 test04 = new Test04();
        System.out.println(test04.foo().ele);
    }
}
