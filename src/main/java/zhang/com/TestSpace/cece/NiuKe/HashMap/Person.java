package zhang.com.TestSpace.cece.NiuKe.HashMap;

public class Person{
    private String name = "Person";
    int age=0;
}

class Child extends Person{
    public String grade = " a ";
    public static void main(String[] args){
        Person p = new Child();
        Child c = new Child();
        System.out.println(c.grade);
        //System.out.println(p.name);
        //private的成员变量，根据权限修饰符的访问控制范围，只有在类内部才能被访问，就算是他的子类，也不能访问。
    }
}
