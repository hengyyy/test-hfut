package zhang.com.TestSpace.cece.NiuKe.Niu;

 class T {
        private int data;
        int result = 0;
        public void m()
        {
            result += 2;
            data += 2;
            System.out.print(result + "  " + data);
        }
    }
    class ThreadExample extends Thread
    {
        private T mv;
        public ThreadExample(T mv)
        {
            this.mv = mv;
        }
        public void run()
        {
            synchronized(mv)        //使用synchronized关键字加同步锁线程依次操作m()
            {
                mv.m();
            }
        }
    }
    class ThreadTest {
        public static void main(String args[]) {
            T mv = new T();
            Thread t1 = new ThreadExample(mv);
            Thread t2 = new ThreadExample(mv);
            Thread t3 = new ThreadExample(mv);
            t1.start();
            t2.start();
            t3.start();
        }
}