package zhang.com.TestSpace.cece.NiuKe.Niu;

public class N1Static {
    static{
        int x=5;                //静态语句块中的变量是局部变量，在静态代码块执行完毕后，这个局部变量就被销毁了。
        System.out.println(x);
    }
    static int x,y;             //静态变量属于当前类

    public static void main(String args[]){
        System.out.println(x+":y:"+y);
        x--;
        System.out.println(x);
        myMethod();
        System.out.println(x+y+ ++x);
    }
    public static void myMethod(){
        y=x++ + ++x;
    }
}
