package zhang.com.TestSpace.cece.NiuKe.Niu;


public class 静态方法的调用 {
    public int i;
    static String s;
    void method1(){}
    static void method2(){}

    public static void main(String[] args) {
        静态方法的调用 a=new 静态方法的调用();
        System.out.println(a.i);
        a.method1();//静态成员变量和静态方法可以通过类名直接调用，也可以通过创建类的对象调用，非静态的成员变量和方法只能通过创建类对象的方式调用。
        //A.method1();//类名无法直接调用非静态方法    非静态可以调用静态，静态不能调用非静态
        静态方法的调用.method2();
    }
}
