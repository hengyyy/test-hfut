package zhang.com.TestSpace.cece.NiuKe.HashMap;

public class 字符串堆区 {
    public static void main(String[] args) {
        //引用类型判断时，==判断的是地址是否相等。如果用常量字符串来定义的（如s1，s2，s4）会存在常量池里面；用变量定义的就不会（如s3，s5）
        String s1 = "coder";
        String s2 = "coder";
        String s3 = "coder" + s2;//堆区1
        String s4 = "coder" + "coder";
        String s5 = s1 + s2;//堆区2
        System.out.println(s3 == s4);
        System.out.println(s3 == s5);
        System.out.println(s4 == "codercoder");
    }
}
