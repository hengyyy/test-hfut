package zhang.com.TestSpace.cece.NiuKe.Niu;

public class HelloB extends HelloA//继承代码块
{
    public HelloB()
    {
    }
    {
        System.out.println("I’m B class");
    }
    static {//2、执行子类的静态代码块
        System.out.println("static B");
    }
    public static void main(String[] args)
    {
        new HelloB();
    }
}
class HelloA
{
    public HelloA()
    {
    }
    {
        System.out.println("I’m A class");//3、执行父类的构造代码块//4、执行父类的构造函数 5、执行子类的构造代码块 6、执行子类的构造函数
    }
    static
    {//当涉及到继承时，按照如下顺序执行：
        System.out.println("static A");//1、执行父类的静态代码块  2、执行子类的静态代码块
    }
}
