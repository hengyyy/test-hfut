package zhang.com.DarkHouse.ShijianFuzadu;


/**
 * 算法时间复杂度分析
 * 事后分析估算方法
 *
 * 事前分析估算方法
 *
 * T(n)=O(f(n))
 */
public class ShijianFuzadu {
    public static void main(String []args){
        long start = System.currentTimeMillis();
        int sum=0;
        int n=100000;
        for(int i=0;i<n;i++){
            sum+=i;
        }
        System.out.println(sum);

        long end=System.currentTimeMillis();
        System.out.println(end-start);
    }
}
