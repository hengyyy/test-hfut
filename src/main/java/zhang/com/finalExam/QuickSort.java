package zhang.com.finalExam;

public class QuickSort {
    /**
     * 快速排序:交换排序，时间复杂度为:O(nlogn) 不稳定
     *
     * @param arr
     * @param l
     * @param r
     */
    static void quicksort(int arr[],int l , int r){
        if(l<r){
            int temp = arr[l];
            int i = l,j=r;
            while(i<j){
                while(i<j&&temp<arr[j])j--;
                if(i<j){
                    arr[i++]=arr[j];;
                }
                while(i<j&&temp>arr[i])i++;
                if(i<j){
                    arr[j--]=arr[i];
                }
            }
            arr[i]=temp;
            quicksort(arr,l,i-1);
            quicksort(arr,i+1,r);
        }
    }

    /**
     * 冒泡排序  时间复杂度O(n^2)  稳定
     * @param arr
     */
    static void MaoPaoSort(int arr[]){
        for(int j=0;j<arr.length-1;j++)
        for(int i = 0;i<arr.length-1;i++){
                if(arr[i]>arr[i+1]){
                    int temp = arr[i];
                    arr[i]=arr[i+1];
                    arr[i+1]=temp;
                }
        }
    }

    /**
     *   希尔排序   算法最后一趟对所有元素进行了直接插入排序，所以结果一定是正确的。  时间复杂度:O(n^1.58)不稳定
     *
     */

    static void shell_sort(int[] a, int len)
    {
        int temp; int j;
        int step = len/2;
        while(step>0){
            for(int i = step;i<len;i++){
                temp=a[i];
                j = i-step;
                while(j>=0&&a[j]>temp){
                    a[j+step]=a[j];
                    j-=step;
                }
                a[j+step]=temp;
            }
            step/=2;
        }
    }

    /**
     * 插入排序   时间复杂度O(n^2)  稳定
     * @param arr
     */
    static void InsertSort(int arr[]){
        int j;
        int temp;
        for(int i=1;i<arr.length;i++){
            temp = arr[i];
            j=i-1;
            while(j>=0&&arr[j]>temp){
                arr[j+1]=arr[j];
                j--;
            }
            arr[j+1]=temp;
        }
    }

    /**
     * 选择排序   时间复杂度O(n^2)  不稳定
     * @param arr
     */
    static void selectSort(int arr[]){
        int min_index,temp;

        for(int i =0;i<arr.length-1;i++){
            min_index=i;
            for(int j=i+1;j<arr.length;j++)
            min_index = (arr[min_index]>arr[j])?j:min_index;

            temp = arr[i];
            arr[i]=arr[min_index];
            arr[min_index]=temp;
        }
    }

    public static void Radixsort(int[] number, int d) //d表示最大的数有多少位
    {
        int k = 0;
        int n = 1;
        int m = 1; //控制键值排序依据在哪一位
        int[][] temp = new int[10][number.length]; //数组的第一维表示可能的余数0-9
        int[] order = new int[10]; //数组order[i]用来表示该位是i的数的个数
        while (m <= d) {
            for (int i = 0; i < number.length; i++) {
                int lsd = ((number[i] / n) % 10);
                temp[lsd][order[lsd]] = number[i];
                order[lsd]++;
            }
            for (int i = 0; i < 10; i++) {
                if (order[i] != 0)
                    for (int j = 0; j < order[i]; j++) {
                        number[k] = temp[i][j];
                        k++;
                    }
                order[i] = 0;
            }
            n *= 10;
            k = 0;
            m++;
        }
    }


    /**
     * 归并排序   时间复杂度：O(nlogn)   稳定
     * @param a
     * @param low
     * @param hight
     */
    static void mergeSort(int []a,int low,int hight){
        if(hight>low){
            int mid = (hight+low)/2;
            mergeSort(a,low,mid);
            mergeSort(a,mid+1,hight);
            merge(a,low,mid,hight);
        }
    }

    static void merge(int []a,int low,int mid,int hight){
        int b []= new int[hight-low+1];
        int i=low,j=mid+1,k=0;
        while(i<=mid&&j<=hight){
            if(a[i]<=a[j])b[k++]=a[i++];
            else b[k++]=a[j++];
        }
        while(i<=mid)b[k++]=a[i++];
        while(j<=hight)b[k++]=a[j++];

        k=0;
        for(int m=low;m<=hight;m++)a[m]=b[k++];
    }


    /**
     *     堆排序函数  希尔排序  快速排序  选择排序   堆排序是 不稳定的
     *     堆排序的时间复杂度是:O(nlogn)
     *
     */

    static void heapSort(int []arr){
        // 从最后一个非叶子节点开始，对每个节点进行调整，使其满足大顶堆的性质
        for(int i=arr.length-1;i>=0;i--){
            adjestSort(arr,i,arr.length);
        }
        // 将堆顶元素（最大值）与末尾元素交换，然后调整剩余元素为大顶堆
        for(int i=arr.length-1;i>=0;i--){
            int temp = arr[0]; arr[0]=arr[i]; arr[i]=temp; adjestSort(arr,0,i);
        }
    }

    // 调整函数，用于调整指定节点及其子树，使其满足大顶堆的性质
    static void adjestSort(int arr[],int parent,int length){
        int temp = arr[parent];int Child = 2*parent+1;
        while(Child<length){
            if(Child+1<length && arr[Child]<arr[Child+1]){
                Child++;
            }
            if(temp>=arr[Child]){
                break;
            }
            arr[parent]=arr[Child];
            parent=Child;
            Child=parent*2+1;
        }
        arr[parent]=temp;
    }


    public static void main(String []args){
        int arr[] = new int[]{9,80,70,60,5,400,3,2,1};
        heapSort( arr);
        for(int e:arr)System.out.print(e+" ");

    }
}
