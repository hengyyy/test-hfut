package zhang.com.finalExam;

public class SeqSearch {

    /**
     * 简单查找，设置”哨兵“，不用比较是否越界。把尾数据设置为要查找的元素。
     * @param arr
     * @param k
     * @return
     */
    static int searchline(int arr[],int k){
        int i=0;
        while(i<arr.length&&arr[i]!=k)
            i++;
            if(i>=arr.length)return -1;
            else return i;
    }

    /**
     * 二分查找
     * @param arr
     * @param k
     * @return
     */
    static int binarySearch(int arr[],int k){

        int l=0,r=arr.length-1;
        while(l<=r){
            int mid = (r-l)/2+l;
            int num = arr[mid];
            if(k==num) return mid;
            else if(num>k) r=mid-1;
            else l= mid+1;
        }
        return -1;
    }


    //查找k的插入点
    static int goek(int arr[],int k){
        int l = 0, r=arr.length-1;
        while(l<=r){
            int mid=(r+l)/2;
            int num =arr[mid];
            if(num>=k)r=mid-1;
            else l=mid+1;
        }

        return l+1;
    }

    //查找第一个k元素


    //查找第一个k元素
    static int firstequalsK(int arr[],int k){
        int mid ,l=0,r=arr.length-1;
        while(l<r){
            mid = (l+r)/2;
            if(k<=arr[mid])r=mid;
            else l =mid+1;
        }
        if(k==arr[l]) return l;
        else
        return -1;
    }

    //查找最后一个k元素
    static int lastequalsK(int arr[],int k){
        int l=0,r=arr.length-1,mid;
        while(l<r){
            mid=(l+r+1)/2;
            if(k>=arr[mid])l=mid;
            else r = mid-1;
        }

        if(k==arr[l])return l;

        else return 0;
    }

    public static void main(String []args){
        int arr[]=new int[]{1,2,2,2,5,6,7,8,9};
        int k=2;
        int result1 = lastequalsK(arr,k);

        System.out.println(result1);
    }
}
