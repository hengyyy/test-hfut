package zhang.com.finalExam;

class TreeNode{
    TreeNode left,right;
    int val;

    public TreeNode(int data){
        this.val = data;
    }
}

class Solution{//验证二叉搜索树   树的中序遍历顺序，如果这个节点的值大于前面一个节点的值。就不是二叉搜索树。
    private long pre = Long.MIN_VALUE;
    public boolean isValidBST(TreeNode root){
        if(root==null) return true;
        if(!isValidBST(root.left)&&root.val<=pre)return false;

        pre = root.val;
        return isValidBST(root.right);
    }

}

public class isBalanced {

    public static int height(TreeNode root){
        if(root==null){
            return 0;
        }
        else return Math.max(height(root.left),height(root.right))+1;
    }
    public static boolean isbalanced(TreeNode root){
        if(root == null) return true;
        else return Math.abs(height(root.left) - height(root.right))<=1&&isbalanced(root.left)&&isbalanced(root.right);
    }



    public static void main(String []args){
        TreeNode root = new TreeNode(8);
        root.left=new TreeNode(4);
        root.right = new TreeNode(12);
        boolean result = isbalanced(root);
        System.out.println(result);


    }
}
