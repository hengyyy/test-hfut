package zhang.com.Testspace001.TestSpace002;


import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
public class Test {
	public static void main(String[] args) {
//		PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {
//			@Override
//			public int compare(Integer o1, Integer o2) {
//				return o2-o1;
//			}
//		});

		PriorityQueue<Integer> queue = new PriorityQueue<>((p1, p2)->p2 - p1);//上面的lambda表达式
		queue.add(9);
		queue.add(44);
		queue.add(91);
		queue.add(92);
		queue.add(3);


		System.out.println("f:"+queue.poll());

		while (!queue.isEmpty()) {
			System.out.println(queue.poll());
		}
	}
}
