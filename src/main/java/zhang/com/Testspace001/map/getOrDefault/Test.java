package zhang.com.Testspace001.map.getOrDefault;


import java.util.HashMap;
import java.util.Map;

public class Test {
    public static void main(String []args){
        Map<Integer,Integer> map = new HashMap<>();
        map.put(1,110);
        map.put(2,200);
        map.put(3,300);
        map.put(4,400);

        int a = map.get(1);
        System.out.println(a);

        int b = map.getOrDefault(11,111);
        System.out.println(b);

    }
}
