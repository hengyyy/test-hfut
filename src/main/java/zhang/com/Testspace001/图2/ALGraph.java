package zhang.com.Testspace001.图2;

import java.util.*;
public class ALGraph implements IGraph{
    private GraphKind kind;         //枚举类型
    private int vexNum, arcNum;     //顶点数和边数
    public VNode[] vexs;
    public ALGraph(GraphKind kind, int vexNum, int arcNum, VNode[] vexs) {
        this.kind = kind;
        this.arcNum = arcNum;
        this.vexNum = vexNum;
        this.vexs = vexs;
    }

    public ALGraph() {
        this(null, 0, 0, null);
    }

    public void createGraph() {     //创建图
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入图的类型");

        GraphKind kind = GraphKind.valueOf(sc.next());
        switch (kind) {
            case DG:
                createDG();
                return;
        }
    }

    public void createDG() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请分别输入图的顶点数，图的边数：");
        vexNum = sc.nextInt();
        arcNum = sc.nextInt();
        vexs = new VNode[vexNum];                //节点
        System.out.println("请分别输入图的各顶点：");
        for (int v = 0; v < vexNum; v++) {       //初始化节点
            vexs[v] = new VNode(sc.next());
        }
        System.out.println("请输入各边顶点");
        for (int k = 0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            addArc(v, u);
        }
    }

    public void addArc(int v, int u) {
        ArcNode arc = new ArcNode(u);
        arc.nextArc = vexs[v].firstArc;
        vexs[v].firstArc = arc;
    }

    public int locateVex(Object vex) {     //
        for (int v = 0; v < vexNum; v++)
            if (vexs[v].data.equals(vex))
                return v;
        return -1;
    }

    public int getVexNum() {    //顶点数
        return vexNum;
    }

    public int getArcNum() {    //边数
        return arcNum;
    }

    public VNode[] getVexs(){
        return vexs;
    }

    public GraphKind getKind(){
        return kind;
    }

    public Object getVex(int v){
        return vexs[v].data;
    }

    public Object nextAdjVex() {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点及其一个邻接点求下一个邻接点");
        Object V = sc.next();
        Object W = sc.next();
        int v = locateVex(V);
        int w = locateVex(W);
        try {
            int s = nextAdjVex(v,w);
            return vexs[s].data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public int nextAdjVex(int v, int w) throws Exception{
        if (v<0&&v>=vexNum)
            throw new Exception("不存在");
        VNode vex=vexs[v];
        ArcNode arcvw=null;
        for (ArcNode arc=vex.firstArc;arc!=null;arc=arc.nextArc){
            if (arc.adjVex==w){
                arcvw=arc;
                break;
            }
        }
        if (arcvw!=null&&arcvw.nextArc!=null)
            return arcvw.nextArc.adjVex;
        else
            return -1;
    }

    public Object firstAdjVex() {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点求第一个邻接点");
        Object V = sc.next();
        int v = locateVex(V);
        int w=firstAdjVex(v);
        VNode vex=vexs[w];
        return vex.data;
    }
    public int firstAdjVex(int v){
        VNode vex=vexs[v];
        if (vex.firstArc!=null)
            return vex.firstArc.adjVex;
        else
            return -1;
    }

    //广度优先遍历
    private static boolean[]visited;
    public static void BFSTraverse(ALGraph G) {
        visited = new boolean[G.getVexNum()];
        for (int v=0;v<G.getVexNum();v++){
            visited[v]=false;
        }
        for (int v=0;v<G.getVexNum();v++){
            if(!visited[v])
                BFS(G,v);
        }
    }

    private static void BFS(ALGraph G,int v){
        visited[v]=true;
        System.out.print(G.getVex(v).toString()+" ");
        LinkQueue Q=new LinkQueue();
        Q.offer(v);
        while(!Q.isEmpty()){
            int u=(Integer)Q.poll();
            try {
                for(int w=G.firstAdjVex(u); w>=0; w=G.nextAdjVex(u,w))
                    if (visited[w]==false){
                        System.out.print(G.getVex(w).toString() +" ");
                        visited[w]=true;
                        Q.offer(w);
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //深度优先遍历
    public static void DFSTraverse(ALGraph G){
        visited = new boolean[G.getVexNum()];
        for (int v=0;v<G.getVexNum();v++){
            visited[v]=false;
        }
        for (int v=0;v<G.getVexNum();v++){
            if(!visited[v])
                DFS(G,v);
        }
    }

    private static void DFS(ALGraph G,int v) {
        visited[v] = true;
        System.out.print(G.getVex(v).toString() + " ");
        try {
            for(int w=G.firstAdjVex(v); w>=0; w=G.nextAdjVex(v,w))
                if (visited[w]==false)
                    DFS(G,w);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int Degree(){
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点求度");
        Object d = sc.next();
        int v = locateVex(d);
        return outDegree(v)+inDegree(v);
    }

    public int outDegree(int v) {
        int count = 0;
        for (ArcNode arc=vexs[v].firstArc;arc!=null;arc=arc.nextArc){
            count++;
        }
        return count;
    }

    public int inDegree(int v) {
        int count = 0;
        for(int u = 0; u < vexNum; u++) {
            for (ArcNode arc=vexs[u].firstArc;arc!=null;arc=arc.nextArc) {
                if (arc.adjVex==v)
                    ++count;
            }
        }
        return count;
    }

    public void print(){
        System.out.println("邻接表为");
        for(int u = 0; u < vexNum; u++) {
            System.out.print(vexs[u].data+"->");
            for (ArcNode arc = vexs[u].firstArc; arc != null; arc = arc.nextArc) {
                System.out.print(arc.adjVex+"->");
            }
            System.out.println();
        }
    }


    public void exchange(ALGraph G) {
        int vexNum,arcNum;
        vexNum = G.getVexNum();
        arcNum = G.getArcNum();
        Object[] vexs;
        int[][] arcs;
        vexs = new Object[vexNum];
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = G.vexs[v];
        }
        arcs = new int[vexNum][vexNum];
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                arcs[v][u] = 0;
            }
        }
        for (int v=0; v < arcNum; v++) {
            if (G.vexs[v].firstArc == null)
                break;
            else
                for (ArcNode arc=G.vexs[v].firstArc;arc!=null;arc=arc.nextArc) {
                    arcs[v][arc.adjVex] = 1;
                }
        }
        System.out.println("转化后邻接矩阵为");
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                System.out.print(arcs[v][u]+" ");
            }
            System.out.println();
        }
    }
}
