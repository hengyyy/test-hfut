package zhang.com.Testspace001.图2;

public class LinkQueue2<T> implements IQueue<T> {
    Node<T> front;
    Node<T> rear;
    int size;

    static class Node<T> {
        T data;
        Node<T> next;

        Node(T e, Node<T> p) {
            data = e;
            next = p;
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int length() {
        return size;
    }

    public T peek() {
        if (size == 0)
            return null;
        else
            return front.data;
    }

    public void offer(T x) {
        if (size == 0) {
            front = rear = new Node<>(x, null);
        } else {
            rear = rear.next = new Node<>(x, null);
            //rear = rear.next;
        }
        size++;
    }

    public T poll() {
        if (size == 0)
            return null;
        else {
            Node<T> p = front;
            front = front.next;

            T data = p.data;
            p.data = null;
            p.next = null;

            size--;
/*
            if (size == 0) {// 删除的是最后1个元素,不要这一段也可以，因为总是用size==0判断队空
                rear = null;// front肯定是null
                assert (front == null);
            }
*/

            return data;
        }
    }

    public void clear() {
        for(; size != 0; --size) {//没有用front != null
            Node<T> p = front;
            front = front.next;

            p.data = null;
            p.next = null;
        }
        //size == 0,front == null, rear=?
    }

    public String toString() {
        Node<T> p = front;
        int i = size;
        String result = getClass().getName() + ":";
        while (i-- != 0) {
            result += p.data + " ";
            p = p.next;
        }
        return result;
    }


}
