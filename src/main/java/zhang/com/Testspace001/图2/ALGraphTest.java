package zhang.com.Testspace001.图2;

public class ALGraphTest {
    public static void main(String[] args) {
        ALGraph t=new ALGraph();
        t.createGraph();
        t.print();
        Object m=t.firstAdjVex();
        System.out.println("第一个邻接点是"+m);//0 1 0 3 1 5 2 1 2 5 3 4 4 1 4 5
        Object n = t.nextAdjVex();
        System.out.println("下一个邻接点是"+n);
        System.out.println("广度优先遍历结果为：");
        ALGraph.BFSTraverse(t);
        System.out.println();
        System.out.println("深度优先遍历结果为：");
        ALGraph.DFSTraverse(t);
        System.out.println();
        System.out.println("度为"+t.Degree());
        t.exchange(t);
    }
}
