package zhang.com.Testspace001.图2;

//空队列时，size = 0 && front = null && rear = null

public class LinkQueue<T> implements IQueue<T> {
    Node<T> front;
    Node<T> rear;
    int size;

    static class Node<T> {
        T data;
        Node<T> next;

        Node(T e, Node<T> p) {
            data = e;
            next = p;
        }
    }


    public boolean isEmpty() {
        return size == 0;
    }

    public int length() {
        return size;
    }

    public T peek() {
        if (front == null)
            return null;
        else
            return front.data;
    }

    public void offer(T x) {
        if (front == null) {
            front = rear = new Node<>(x, null);
        } else {
            rear = rear.next = new Node<>(x, null);
            // rear = rear.next;
        }
        size++;
    }

    public T poll() {
        if (front == null)
            return null;
        else {
            Node<T> p = front;
            front = front.next;

            T data = p.data;
            p.data = null;
            p.next = null;

            size--;

            if (front == null) {// 删除的是最后1个元素
                rear = null;// front肯定是null
                assert (size == 0);
            }

            return data;
        }
    }

    public void clear() {
        while (front != null) {
            Node<T> p = front;
            front = front.next;

            p.data = null;
            p.next = null;
        }
        size = 0;
        rear = null;
    }

    public String toString() {
        Node<T> p = front;
        String result = getClass().getName() + ":";
        while (p != null) {
            result += p.data + " ";
            p = p.next;
        }
        return result;
    }


}
