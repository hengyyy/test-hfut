package zhang.com.Testspace001.图2;

public class MGraphTest {
    public static void main(String[] args){
        System.out.println("深度优先遍历结果为："+0x3f3f3f3f);
        double positiveInfinity = Double.POSITIVE_INFINITY;
        double negativeInfinity = Double.NEGATIVE_INFINITY;

        System.out.println("正无穷大： " + positiveInfinity);
        System.out.println("负无穷大： " + negativeInfinity);
        MGraph t=new MGraph();
        t.createGraph();
        t.print();
        System.out.println();
        Object m=t.firstAdjVex();
        System.out.println("该结点的第一个邻接点的是"+m);
        Object n=t.nextAdjVex();
        System.out.println("下一个邻接点是"+n);
        System.out.println("此节点的度为"+t.Degree());
        System.out.println("广度优先遍历结果为：");
        MGraph.BFSTraverse(t);
        System.out.println();
        System.out.println("深度优先遍历结果为：");

        MGraph.DFSTraverse(t);
        System.out.println();
        t.exchange(t);
    }
}
