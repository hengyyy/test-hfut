package zhang.com.Testspace001.图2;

public interface IQueue<T> {
    void clear();

    boolean isEmpty();

    int length();

    T peek();

    void offer(T x);

    T poll();
}
