package zhang.com.Testspace001.图2;

import java.util.*;


public class MGraph {
    public final static int INFINITY = Integer.MAX_VALUE;
    private GraphKind kind;
    private int vexNum, arcNum;
    private Object[] vexs;
    private int[][] arcs;

    public MGraph(GraphKind kind, int vexNum, int arcNum, Object[] vexs, int[][] arcs) {
        this.kind = kind;
        this.vexNum = vexNum;
        this.arcNum = arcNum;
        this.vexs = vexs;
        this.arcs = arcs;
    }

    public MGraph() {
        this(null, 0, 0, null, null);
    }

    public void createGraph() {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入图的类型");
        GraphKind kind = GraphKind.valueOf(sc.next());
        switch (kind) {
            case DG:
                createDG();
                return;
        }
    }

    private void createDG() {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点数，边数");
        vexNum = sc.nextInt();
        arcNum = sc.nextInt();
        vexs = new Object[vexNum];
        System.out.println("输入各个顶点");
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = sc.next();
        }
        arcs = new int[vexNum][vexNum];
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                arcs[v][u] = 0;
            }
        }
        System.out.println("输入矩阵为1的各个边的两个顶点");
        for (int k=0; k < arcNum; k++) {
            int v = locateVex(sc.next());
            int u = locateVex(sc.next());
            arcs[v][u] = 1;
        }
    }

    public int locateVex(Object vex){
        for(int v=0;v<vexNum;v++)
            if(vexs[v].equals(vex))
                return v;
        return -1;
    }

    public Object getVex(int v){
        return vexs[v];
    }

    //求第一个邻接点
    /*
    //第一种写法因为忘记已经给出的函数locateVex()而导致代码冗杂
    public Object firstAdjVex(){
        Scanner sc=new Scanner(System.in);
        System.out.println("输入顶点");
        Object V=sc.next();
        int s;
        for (int v = 0; v < vexNum; v++) {
            if(vexs[v].equals(V)) {
                try {
                    s=firstAdjVex(v);
                    return vexs[s];
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }
    */
    //第二种写法使用了函数locateVex()
    public Object firstAdjVex() {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点求第一个邻接点");
        Object V = sc.next();
        int v = locateVex(V);
        try {
            int s=firstAdjVex(v);
            return vexs[s];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public int firstAdjVex(int v)throws Exception{
        if(v<0&&v>=vexNum)
            throw new Exception("不存在");
        for (int j=0;j<vexNum;j++)
            if (arcs[v][j]==1)
                return j;
        return -1;
    }


    //求下一个邻接点
    public Object nextAdjVex() {
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点及其一个邻接点求下一个邻接点");
        Object V = sc.next();
        Object W = sc.next();
        int v = locateVex(V);
        int w = locateVex(W);
        try {
            int s=nextAdjVex(v,w);
            return vexs[s];
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public int nextAdjVex(int v,int w)throws Exception{
        if (v<0 && v>=vexNum)
            throw new Exception("不存在");
        for (int j=w+1;j<vexNum;j++)
            if (arcs[v][j]==1)
                return j;
        return -1;
    }

    //度
    public int Degree(){
        Scanner sc = new Scanner(System.in);
        System.out.println("输入顶点求度");
        Object d = sc.next();
        int v = locateVex(d);
        return outDegree(v)+inDegree(v);
    }

    //出度
    public int outDegree() {
        Scanner sc = new Scanner(System.in);
        //System.out.println("输入顶点求出度");
        Object a = sc.next();
        int v = locateVex(a);
        return outDegree(v);
    }
    public int outDegree(int v) {
        int count = 0;
        for(int i = 0 ; i < vexNum; i++) {
            if(arcs[v][i] != 0 )
                count++;
        }
        return count;
    }

    //入度
    public int inDegree() {
        Scanner sc = new Scanner(System.in);
        //System.out.println("输入顶点求入度");
        Object b = sc.next();
        int v = locateVex(b);
        return inDegree(v);
    }
    public int inDegree(int v) {
        int count = 0;
        for(int i = 0 ; i < vexNum; i++) {
            if(arcs[i][v] != 0 )
                count++;
        }
        return count;
    }

    //广度优先遍历
    private static boolean[]visited;
    public static void BFSTraverse(MGraph G) {
        visited = new boolean[G.getVexNum()];
        for (int v=0;v<G.getVexNum();v++){
            visited[v]=false;
        }
        for (int v=0;v<G.getVexNum();v++){
            if(!visited[v])
                BFS(G,v);
        }
    }
    private static void BFS(MGraph G,int v){
        visited[v]=true;
        System.out.print(G.getVex(v).toString()+" ");
        LinkQueue Q=new LinkQueue();
        Q.offer(v);
        while(!Q.isEmpty()){
            int u=(Integer)Q.poll();
            try {
                for(int w=G.firstAdjVex(u); w>=0; w=G.nextAdjVex(u,w))
                    if (visited[w]==false){
                        System.out.print(G.getVex(w).toString() +" ");
                        visited[w]=true;
                        Q.offer(w);
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //深度优先遍历
    public static void DFSTraverse(MGraph G){
        visited = new boolean[G.getVexNum()];
        for (int v=0;v<G.getVexNum();v++){
            visited[v]=false;
        }
        for (int v=0;v<G.getVexNum();v++){
            if(!visited[v])
                DFS(G,v);
        }
    }
    private static void DFS(MGraph G,int v) {
        visited[v] = true;
        System.out.print(G.getVex(v).toString() + " ");
        try {
            for(int w=G.firstAdjVex(v); w>=0; w=G.nextAdjVex(v,w))
                if (visited[w]==false)
                    DFS(G,w);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GraphKind getKind(){
        return kind;
    }

    public int[][] getArcs(){
        return arcs;
    }

    public Object[] getVexs(){
        return vexs;
    }

    public int getVexNum(){
        return vexNum;
    }

    public int getArcNum(){
        return arcNum;
    }

    //打印邻接矩阵检验
    public void print(){
        System.out.println("邻接矩阵为");
        for (int v = 0; v < vexNum; v++) {
            for (int u = 0; u < vexNum; u++) {
                System.out.print(arcs[v][u]+" ");
            }
            System.out.println();
        }
    }

    public void exchange(MGraph G) {
        int vexNum, arcNum;
        vexNum = G.getVexNum();
        arcNum = G.getArcNum();
        VNode[] vexs;
        vexs = new VNode[vexNum];
        for (int v = 0; v < vexNum; v++) {
            vexs[v] = new VNode(G.vexs[v]);
        }
        for (int v = 0; v < arcNum; v++) {
            for (int u = 0; u < arcNum; u++) {
                if (arcs[v][u] == 1) {
                    ArcNode arc = new ArcNode(u);
                    arc.nextArc = vexs[v].firstArc;
                    vexs[v].firstArc = arc;
                }
            }
        }
        System.out.println("转换后的邻接表为");
        for(int u = 0; u < vexNum; u++) {
            System.out.print(vexs[u].data+"->");
            for (ArcNode arc = vexs[u].firstArc; arc != null; arc = arc.nextArc) {
                System.out.print(arc.adjVex+"->");
            }
            System.out.println();
        }

    }

}
