package zhang.com.Testspace001.图4;

public class Graph {

	private Vertex vertexList[];

	private int[][] matrix;

	private int numVertex;

	private MyQueue queue;
	
	public Graph(int n){
		this.vertexList = new Vertex[n];
		matrix = new int[n][n];
		this.queue = new MyQueue(100);
	}

	public void addVertex(String label){
		this.vertexList[numVertex] = new Vertex(label);
		numVertex++;
	}

	public void addEdge(int start,int end){
		this.matrix[start][end] = 1;
		this.matrix[end][start] = 1;
	}

	public void bfs(){
		//1. 获取所有子节点的文本内容
		this.vertexList[0].setWasVisited(true);
		this.queue.insert(0);
		System.out.print(this.vertexList[0].getLabel()+" ");
		
		//2:3:
		while(!queue.isEmpty()){
			// 找到第一个未被加载的子节点
			int v1 = queue.remove();
			int v = -1;
			while((v=this.getNextUnvisitedVertex(v1))!=-1){
				this.vertexList[v].setWasVisited(true);
				this.queue.insert(v);
				System.out.print(this.vertexList[v].getLabel()+" ");
			}
		}

		//
		for(int i=0;i<numVertex;i++){
			this.vertexList[i].setWasVisited(false);
		}
	}
	/**
	 * 查找第一个未被加载的子节点
	 * @param index
	 * @return
	 */
	private int getNextUnvisitedVertex(int index){
		for(int i=0;i<numVertex;i++){
			if(this.matrix[index][i] == 1
					&& !this.vertexList[i].isWasVisited()
			){
				return i;
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		Graph t = new Graph(10);
		
		t.addVertex("A");
		t.addVertex("B");
		t.addVertex("C");
		t.addVertex("D");
		t.addVertex("E");
		t.addVertex("F");
		t.addVertex("G");
		t.addVertex("H");
		t.addVertex("I");
		t.addVertex("J");
		
		t.addEdge(0, 1);
		t.addEdge(0, 2);
		t.addEdge(0, 3);
		t.addEdge(1, 2);
		t.addEdge(1, 3);
		t.addEdge(2, 4);
		t.addEdge(4, 5);
		t.addEdge(2, 6);
		t.addEdge(5, 7);
		t.addEdge(7, 8);
		t.addEdge(1, 9);
		
		t.bfs();
		
	}
}
