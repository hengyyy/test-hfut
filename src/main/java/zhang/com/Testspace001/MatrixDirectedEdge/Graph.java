	package zhang.com.Testspace001.MatrixDirectedEdge;


    public class Graph {

	private Vertex vertexList[];

	private int[][] matrix;

	private int numVertex;

	private MyStack theStack;

	public Graph(int n){
		this.vertexList = new Vertex[n];
		matrix = new int[n][n];
		this.theStack = new MyStack(100);
	}

	public void addVertex(String label){
		this.vertexList[numVertex] = new Vertex(label);
		numVertex++;
	}

	public void addEdge(int start,int end){
		this.matrix[start][end] = 1;
	}

	public void dfs(int begin,int end){
		//1.将当前顶点标记为已访问，并将其压入栈中
		this.vertexList[begin].setWasVisited(true);
		this.theStack.push(begin);
		System.out.print(this.vertexList[begin].getLabel()+" ");

		//2:3:
		while(!theStack.isEmpty()){
			//找到下一个未访问的相邻顶点
			int v = this.getNextUnvisitedVertex(theStack.peek());
			if(v==-1){
				//没有找到未访问的相邻顶点，弹出栈顶元素
				this.theStack.pop();
			}else{
				//找到了未访问的相邻顶点，将其标记为已访问，并将其压入栈中
				this.vertexList[v].setWasVisited(true);
				this.theStack.push(v);
				System.out.print(this.vertexList[v].getLabel()+" ");
				//if(v==end)break;
			}
		}

		//3:重置所有顶点的访问状态
		for(int i=0;i<numVertex;i++){
			this.vertexList[i].setWasVisited(false);
		}
	}

	private int getNextUnvisitedVertex(int index){
		for(int i=0;i<numVertex;i++){
			if(this.matrix[index][i] == 1
					&& !this.vertexList[i].isWasVisited()
			){
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		Graph t = new Graph(20);

		t.addVertex("A");
		t.addVertex("B");
		t.addVertex("C");
		t.addVertex("D");
		t.addVertex("E");
		t.addVertex("F");
		t.addVertex("Y");
		t.addVertex("Z");

		t.addEdge(0, 1);
		t.addEdge(0, 6);
		t.addEdge(6, 5);
		t.addEdge(7, 5);
		t.addEdge(0, 2);
		t.addEdge(0, 3);
		t.addEdge(1, 2);
		t.addEdge(1, 3);
		t.addEdge(2, 4);
		t.addEdge(4, 5);

			t.dfs(0,5);

	}
}