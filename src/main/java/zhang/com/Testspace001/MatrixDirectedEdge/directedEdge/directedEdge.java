package zhang.com.Testspace001.MatrixDirectedEdge.directedEdge;

import java.util.Stack;

class Vertex{
    String data;
    boolean wasvisited;
    public Vertex(String e){
        this.data=e;
        wasvisited=false;
    }


}


public class directedEdge {
    Vertex vertexlist[];
    int numVertex;
    int matrix[][];
    Stack<Integer> s=new Stack<>();

    public directedEdge(int n){
        vertexlist=new Vertex[n];
        matrix=new int[n][n];
    }

    public void addVertex(String e){
        this.vertexlist[numVertex]=new Vertex(e);
        numVertex++;
    }
    public void addEdge(int start,int end){
        matrix[start][end]=1;
    }

    public void dfs(int begin){
        s.add(begin);
        vertexlist[begin].wasvisited=true;
        System.out.print(vertexlist[begin].data+" ");
        while(!s.isEmpty()){
            int v=getNextUnvisitedVertex(s.peek());
            if(v==-1){
                s.pop();
            }else{
                s.push(v);
                System.out.print(vertexlist[v].data+" ");
                vertexlist[v].wasvisited=true;
            }

        }
        for(int i=0;i<numVertex;i++){
            vertexlist[i].wasvisited=false;
        }
    }

    public int getNextUnvisitedVertex(int index){
        for(int i=0;i<numVertex;i++){
            if(matrix[index][i]==1&& !vertexlist[i].wasvisited){
                return i;
            }
        }
        return -1;
    }


    public static void main(String []args){
        directedEdge e= new directedEdge(20);
        e.addVertex("A");
        e.addVertex("B");
        e.addVertex("C");
        e.addVertex("D");
        e.addVertex("E");
        e.addVertex("F");
        e.addVertex("G");
        e.addVertex("H");
        e.addVertex("I");
        e.addVertex("J");
        e.addVertex("wu");
        e.addVertex("w");

        e.addEdge(0,1);
        e.addEdge(0,2);

        e.addEdge(0,3);
        e.addEdge(0,4);
        e.addEdge(1,5);
        e.addEdge(5,6);
        e.addEdge(6,7);
        e.addEdge(7,8);
        e.addEdge(8,9);

        e.dfs(0);
        System.out.println();
        e.dfs(2);
    }
}
