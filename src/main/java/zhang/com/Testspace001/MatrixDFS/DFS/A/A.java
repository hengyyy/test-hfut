package zhang.com.Testspace001.MatrixDFS.DFS.A;

import java.util.Stack;

class Vertex{
    String data;
    boolean wasvisited;

    public Vertex(String e){
        this.data=e;
        wasvisited=false;
    }

}
public class A {
    Vertex vertexlist[];
    int matrix[][];
    Stack<Integer> stack = new Stack<Integer>();

    int numVertex;
    public A(int n){
        vertexlist =new Vertex[n];
        matrix=new int[n][n];
    }

    public void addVertex(String e){
        this.vertexlist[numVertex]=new Vertex(e);
        numVertex++;
    }

    public void addEdge(int start,int end){
        this.matrix[start][end]=1;
        this.matrix[start][end]=1;
    }

    //dfs
    public void dfs(){
        this.stack.push(0);
        this.vertexlist[0].wasvisited=true;
        System.out.print(vertexlist[0].data+" ");
        while(!stack.isEmpty()){
            int v=getNextUnvisitedVertex(stack.peek());
            if(v==-1){
                this.stack.pop();
            }else{
                this.vertexlist[v].wasvisited=true;
                stack.push(v);
                System.out.print(this.vertexlist[v].data+" ");
            }
        }
        for(int i=0;i<numVertex;i++){
            vertexlist[i].wasvisited=false;
        }
    }

    //
    public int getNextUnvisitedVertex(int index){
        for(int i=0;i<numVertex;i++){
            if(matrix[index][i]==1 && !vertexlist[i].wasvisited){
                return i;
            }
        }
        return -1;
    }

    public static void main(String args[]){
        A a=new A(20);
        a.addVertex("1");
        a.addVertex("+");
        a.addVertex("1");
        a.addVertex("9");
        a.addVertex("7");
        a.addVertex("5");
        a.addVertex("2");
        a.addVertex(" = ");
        a.addVertex("3");
        a.addVertex("哥德巴赫猜想！");
        a.addEdge(0,1);
        a.addEdge(1,6);
        a.addEdge(6,7);
        a.addEdge(7,8);
        a.addEdge(8,9);

        a.addEdge(0,2);
        a.addEdge(0,3);
        a.addEdge(0,4);
        a.addEdge(0,5);
        System.out.println("a的节点数目 = "+a.numVertex);

        a.dfs();
    }

}
