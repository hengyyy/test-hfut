package zhang.com.Testspace001.MatrixDFS;

public class regex_04_10 {

    public static void main(String[] args) {
        String str01 = "2018,text,今天";
        //单个分隔符用引号括起来即可
        String[] data01 = str01.split(",");
        for (int i = 0; i < data01.length; i++) {
            System.out.println(data01[i]);
        }

		System.out.println("=============");

        String str02 = "a|bc|8";
        //java中\\表示一个普通\,\+特殊字符表示字符本身
        String[] data02 = str02.split("\\|");
        for (int i = 0; i < data02.length; i++) {
            System.out.println(data02[i]);
        }
		System.out.println("=============");
		String str03="a|bc|8";
		//java中\\表示一个普通\,\+特殊字符表示字符本身
		String[] data03 = str03.split("|");
		for(int i=0;i< data03.length;i++) {
			System.out.println(data03[i]);
		}

		System.out.println("=============");
		String str04="2021年11月18日;英语,数学,语文;";
		//多个分隔符用引号括起来，并且用“|”进行分割
		String[] data04 = str04.split(",|;");
		for(int i=0;i< data04.length;i++){
			System.out.println(data04[i]);
		}

		System.out.println("=============");
		String str="2018年11月18日abcd85gg688";
		//正则表达式中\d+表示一个或多个数字,java中\\表示一个普通\
		String[] data = str.split("\\d+");
		for(int i=0;i< data.length;i++){
			System.out.println(data[i]);
		}


    }
}
