package zhang.com.Testspace001.邻接矩阵;


/**
 * 无向图---邻接矩阵
 * @param
 */
public class MatrixGraph {

    String[] vexs;     //存放顶点信息
    int MAXV =100;      //表示最多顶点个数
    int INF =0x3f3f3f3f;   //表示无穷大
    int[][] edgeMatrix;     //邻接矩阵数组
    int edges;              //边的数量
    int vertices;           //顶点数量

    public MatrixGraph(int size){

        vexs=new String[MAXV];
        edgeMatrix = new int [size][size];
    }


    public void addEdge(int from, int to) {
        this.addEdge(from,to,1);
    }


    public void addEdge(int from, int to, int weight) {         //邻接矩阵是对称的

        this.edges++;
    }

    public void addEdge2(int from, int to, int weight) {

        this.edges++;
    }

    public void displayGraph() {
        //显示当前图
        System.out.println("这是一个无向图（邻接矩阵）");

        System.out.println("图的邻接矩阵是：");
        for(int i=0;i<this.vertices;i++){
            for(int j=0;j<this.vertices;j++){
                System.out.print(this.edgeMatrix[i][j]+"  ");//
            }
            System.out.println();
        }
    }

    private void findWay() {

    }


    public static void main(String []args){
        MatrixGraph graph = new MatrixGraph(10);
//        graph.addVertex(0);
//        graph.addVertex(1);
//        graph.addVertex(2);
//        graph.addVertex(3);
//        graph.addVertex(4);
//        graph.addVertex(5);
//
//        graph.addEdge(0,1);
//        graph.addEdge(0,3);
//        graph.addEdge(1,5);
//        graph.addEdge(2,5);
//        graph.addEdge(2,1);
//        graph.addEdge(3,4);
//        graph.addEdge(4,1);
//        graph.addEdge(4,5);
//
//        graph.displayGraph();
//        graph.findWay();
        /*
0 1 0 1 0 0
1 0 1 0 1 1
0 1 0 0 0 1
1 0 0 0 1 0
0 1 0 1 0 1
0 1 1 0 1 0

0 1 0 1 0 0
0 0 0 0 0 1
0 1 0 0 0 1
0 0 0 0 1 0
0 1 0 0 0 1
0 0 0 0 0 0
         */
//[[0, 1, 5], [0, 3, 4, 1, 5], [0, 3, 4, 5]]
    }
}
