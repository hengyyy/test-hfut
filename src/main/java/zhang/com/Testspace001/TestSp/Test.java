package zhang.com.Testspace001.TestSp;


import java.util.*;

class Solution {
     public int[] topKFrequent(int[] nums, int k) {

          Map<Integer,Integer> map = new HashMap<Integer,Integer>();
          for(int a : nums ){
               if(map.containsKey(a)){
                    map.put(a,map.get(a)+1);
               } else {
                    map.put(a,1);
               }
          }                                       //进行词频统计

          map.forEach((ea,v) ->System.out.println("map:k , v"+ea+", "+ v));

          System.out.println(map.isEmpty());
          int[] arr = new int[k];

          //用栈来解决
          Stack<Integer> stack = new Stack<>();
          for(Integer e : map.keySet()) {
               if(stack==null) stack.push(e);

               while (!stack.isEmpty()){
                    if(stack.peek()>e){
                         stack.push(e);
                    } else {
                         stack.pop();
                    }
               }


          }

          return arr;
     }
}
public class Test{
     public static void main(String []args){

          Solution s = new Solution();
          int[] nums = new int[] {1,1,1,2,2,3};
          Arrays.stream(s.topKFrequent(nums,2)).forEach(System.out::println);
     }

}
