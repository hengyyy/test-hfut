package zhang.com.Testspace001.MyGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Solution2 {
    public int maxDistance(int[][] grid) {
        int n = grid.length;
        List<int[]> landCells = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    landCells.add(new int[]{i, j});
                }
            }
        }

        if (landCells.size() == 1) {
            return -1;
        }
        int maxDistance = 0;
        for (int[] cell : landCells) {
            for (int[] otherCell : landCells) {
                maxDistance = Math.max(maxDistance, Math.abs(cell[0] - otherCell[0]) + Math.abs(cell[1] - otherCell[1]));
            }
        }
        return maxDistance;
    }

    public static void main(String []args){
        Solution2 s = new Solution2();
        int map[][]=new int[5][5];
        Random rand=new Random();
        for(int i=0;i<map.length;i++){
            for(int j=0;j<map[0].length;j++){
                map[i][j]=rand.nextInt(2);
            }
        }
        int map2[][]={{1,0,1},
                      {0,0,0},
                      {1,0,1}
        };
        System.out.println(s.maxDistance(map2));
    }
}
