package zhang.com.Testspace001.MyGraph;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Solution {
    /*
     这道题要弄具体的路径出来，肯定BFS就不行了，用DFS做。然后这道题是有向图，注意一点就可以了。
     然后注意，如果在 Main 函数里面使用 DFS，弄了几个全局变量用 Static 修饰，然后直接访问就可以了
      */
    private static List<List<Integer>> res;
    private static Deque<Integer> path;
    public List<List<Integer>> allPathsSourceTarget(int[][] graph) {
        res = new ArrayList<>();
        path = new ArrayDeque<>();
        // 节点数量
        int numb = graph.length;
        // 初始化
        path.add(0);
        dfs(0,numb - 1,graph);
        return res;
    }

    // dfs 从 begin 一直深入带 end 去，找到了就返回
    private void dfs(int begin, int end, int[][] graph) {
        // 如果找到了
        if (begin == end) {
            res.add(new ArrayList<>(path));
            return;
        }
        // 没有找到就继续深入
        for (int next : graph[begin]) {
            path.add(next);
            dfs(next,end,graph);
            // 回退
            path.pollLast();
        }
    }
    public static void main(String []args){
        int graph[][]={
                {1,3},{5},{1,5},{4},{1,5},{}
        };
        Solution s= new Solution();

        System.out.println(s.allPathsSourceTarget(graph));
    }
}