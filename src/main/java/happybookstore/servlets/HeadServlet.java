package happybookstore.servlets;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;


// This is a simple example of an HTTP Servlet.
public class HeadServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        output(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        output(request, response);
    }

    private void output(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        PrintWriter out = response.getWriter();

        // then write the data of the response
        out.println("<body  bgcolor=\"#ffffff\">"  +"<center>"+"<img src=\"pic/logo.jpg\" border=\"0\" />"+
            "</center>" + "<hr>");
    }
}
