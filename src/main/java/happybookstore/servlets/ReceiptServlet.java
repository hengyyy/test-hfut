package happybookstore.servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import happybookstore.cart.ShoppingCart;
import happybookstore.database.*;
import happybookstore.exception.*;

public class ReceiptServlet extends HttpServlet {
    private BookDBAO bookDB;

    public void init() throws ServletException {
        bookDB = (BookDBAO) getServletContext()
                                .getAttribute("bookDB");

        if (bookDB == null) {
            throw new UnavailableException("Couldn't get database.");
        }
    }

    public void destroy() {
        bookDB = null;
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
        boolean orderCompleted = true;

        // Get the user's session and shopping cart
        HttpSession session = request.getSession(true);
        
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");

        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }

        // Update the inventory
        try {
            bookDB.buyBooks(cart);
        } catch (OrderException e) {
            System.err.println(e.getMessage());
            orderCompleted = false;
        }

        // Payment received -- invalidate the session
        session.invalidate();

        // set content type header before accessing the Writer
        response.setContentType("text/html;charset=gb2312");
        response.setBufferSize(8192);

        PrintWriter out = response.getWriter();

        // then write the response
        out.println("<html>" + "<head><title>BookStore</title></head>");

        // Get the dispatcher; it gets the banner to the user
        RequestDispatcher dispatcher =
            getServletContext()
                .getRequestDispatcher("/head");

        if (dispatcher != null) {
            dispatcher.include(request, response);
        }

        if (orderCompleted) {
            out.println("<center><h3>" + "лл���٣���ӭ�´����� " +
                request.getParameter("cardname") + ".");
        } else {
            out.println("<h3>" + "�Բ�����ʱȱ����");
        }

        out.println("<p> &nbsp; <p><strong><a href=\"" +
            response.encodeURL(request.getContextPath()) + "/bookcatalog\">" +
            "��������" +
            "</a> &nbsp; &nbsp; &nbsp;" + "</center>"+"</body>"+"</html>");
        out.close();
    }

    public String getServletInfo() {
        return "The Receipt servlet updates the book database inventory, invalidates the user session, " +
        "thanks the user for the order.";
    }
}
