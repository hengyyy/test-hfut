package shopping.cart;

import lombok.Data;

@Data
public class CartItem {  //购物车项，购物车中每一项都是product对象
    private Product product;
    
    private int count;    
    

} 