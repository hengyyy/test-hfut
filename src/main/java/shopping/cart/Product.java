package shopping.cart;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Product {//商品信息
	private String id;//产品标识
	private String name;//产品名称
	private String description;//产品描述
	private double price;//产品价格


}