import org.junit.runner.RunWith;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import zhang.ssm.Spring.test04.homework.HomeWork;


    import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

    /**
     * 测试注解方式的IoC
     */
    @RunWith(SpringRunner.class)
    @ContextConfiguration("classpath:spring.xml")
    @Component
    public class A {
        private HomeWork homeWork;
        //Spring 容器注入依赖的Deliverable对象
        @org.junit.Test
        public void test() {
            homeWork.doHomeWork();
        }

}
