<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-04-01
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>EL 标签访问实例</title>
</head>
<body>
<%
    pageContext.setAttribute("name","Smith");
    request.setAttribute("age",20);
    session.setAttribute("address","china");
    application.setAttribute("sex","male");
%>
<h3 style="text-align: center">
    访问演示
</h3>
<table border="1" width="100%">
    <tr>
        <td align="center">姓名</td>
        <td align="center">年龄</td>
        <td align="center">性别</td>
        <td align="center">地址</td>
    </tr>
    <tr>
        <td align="center">${pageScope.name}</td>
        <td align="center">${requestScope.age}</td>
        <td align="center">${sessionScope.address}</td>
        <td align="center">${applicationScope.sex}</td>
    </tr>

    <tr>
        <td align="center">${name}</td>
        <td align="center">${age}</td>
        <td align="center">${address}</td>
        <td align="center">${sex}</td>
    </tr>

</table>
</body>
</html>
