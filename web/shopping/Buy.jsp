<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=gb2312" pageEncoding="gb2312"%>
<%@ page import="shopping.cart.*"%>
<%
	Cart c = (Cart) session.getAttribute("cart");
	if (c == null) {
		c = new Cart();
		session.setAttribute("cart", c);
	}
	double totalPrice = c.getTotalPrice();
	request.setCharacterEncoding("GBK");
	String action = request.getParameter("action");

	Map products = (HashMap) session.getAttribute("products");

	if (action != null && action.trim().equals("add")) {
		String id = request.getParameter("id");
		Product p = (Product) products.get(id);
		CartItem ci = new CartItem();
		ci.setProduct(p);
		ci.setCount(1);
		c.add(ci);
	}

	if (action != null && action.trim().equals("delete")) {
		String id = request.getParameter("id");
		c.deleteItemById(id);
	}

	if (action != null && action.trim().equals("update")) {
		for (int i = 0; i < c.getItems().size(); i++) {
			CartItem ci = c.getItems().get(i);
			int count = Integer.parseInt(request.getParameter("p" + ci.getProduct().getId()));
			ci.setCount(count);
		}
	}

	List<CartItem> items = c.getItems();
%>
<html>
<head>
<title>购物车</title>
</head>
<body background=images/bckgrd.jpg>
	<style>
#tab {
	font-size: 20px;
	font-family: 黑体;
	text-algn: left;
}
</style>
<body>
	<center>
		<H1>您的购物车</H1>
		<form action="Buy.jsp" method="get">
			<input type="hidden" name="action" value="update" />
			<table id="tab" align="center" border="1" cellspacing="0">
				<tr>
					<td>产品ID</td>
					<td>产品名称</td>
					<td>购买数量</td>
					<td>单价</td>
					<td>总价</td>
					<td>处理</td>
				</tr>
				<%
					for (Iterator<CartItem> it = items.iterator(); it.hasNext();) {
						CartItem ci = it.next();
				%>
				<tr>
					<td><%=ci.getProduct().getId()%></td>
					<td><%=ci.getProduct().getName()%></td>
					<td><input type="text" size=3
						name="<%="p" + ci.getProduct().getId()%>"
						value="<%=ci.getCount()%>"
						onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"
						onchange="document.forms[0].submit()"></td>
					<td><%=ci.getProduct().getPrice()%></td>
					<td><%=ci.getProduct().getPrice() * ci.getCount()%></td>
					<td><a
						href="Buy.jsp?action=delete&id=<%=ci.getProduct().getId()%>">删除</a>
					</td>
				</tr>
				<%
					}
				%>
				<tr>
					<td colspan=3 align="right">所有商品总价格为：</td>
					<td colspan=3><%=c.getTotalPrice()%></td>
				</tr>
				<tr>
					<td colspan=6 align="right"><a href="Order.jsp">下单</a></td>
				</tr>
			</table>
		</form>
</body>
</html>
