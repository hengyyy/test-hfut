<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-03-27
  Time: 16:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>读取session值</title>
</head>
<body>
<%
    Object id =session.getAttribute("name");
    Object sex =session.getAttribute("sex");

    if(id!=null){
        out.println("姓名："+id.toString());
        out.println("<br>");
        out.println("性别："+sex.toString());
    }else {
        out.println("未设置session数据");
    }
%>
</body>
</html>
