<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-03-27
  Time: 16:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>设置session数据</title>
</head>
<body>
<%
    String name = request.getParameter("name");
    String sex = request.getParameter("sex");

    session.setAttribute("name",name);
    session.setAttribute("sex",sex);
%>
<a href="usingSession.jsp">显式已设置的session数据内容</a>

</body>
</html>
