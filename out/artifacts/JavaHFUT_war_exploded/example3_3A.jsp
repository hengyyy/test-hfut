<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-03-27
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>

<%!
    long nfact(int n){
        long s = 1; for(int i=1;i<=n;i++) s*=i; return s;
    }
%>


<html>
<head>
    <title>Hello World!</title>
</head>
<body>
</body>
<%
    HttpSession session1 = request.getSession();
    out.println("<font color='blue'>"+session1.getId()+"</font>");

    request.setCharacterEncoding("utf-8");
    String ns = request.getParameter("num");
    String submitName=request.getParameter("submit");
    if(ns==null||submitName==null){
        out.write("<font color='red'>获取参数失败</font>"); return;
    }
    int n;
    try{n = Integer.parseInt(ns);}
    catch (Exception e){
        out.write("<font color='red'>URL中的参数不是有效的数字</font>"); return;
    }
    out.write(submitName+"结果是：");
    out.write("<font color='blue'>"+n+"!="+nfact(n)+"</font>");

%>

</html>
