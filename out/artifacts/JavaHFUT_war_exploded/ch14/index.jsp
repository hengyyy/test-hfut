<%@page pageEncoding="GB2312"%>
<%@page contentType="text/html; charset=GB2312"%>
<%request.setCharacterEncoding("GB2312");%>

<%@ page import="java.util.*,happybookstore.database.*" %>
<jsp:useBean id="bookDB" class="happybookstore.database.BookDBAO" scope="session" />

<html>

<head>

		<title>kuaileG-快乐购书网-kuaileG</title>
		<link href="book.css" rel="stylesheet" type="text/css">
		<link href="second.css" rel="stylesheet" type="text/css">
</head>
<body style="background-color:#fafafa">		

<!--link href="01.47_files/unite_header_1101.css" rel="stylesheet" type="text/css">
<script src="01.47_files/pagetop_1123.js" type="text/javascript"></script-->
 <div class="ddnewhead_wrap">
    <div class="ddnewhead_content">
        <div class="ddnewhead_operate" id="__ddnav_menu">
          <ul class="ddnewhead_operate_nav">
        
            <li class="ddnewhead_separate"></li>
            <li></li>
          </ul>
      </div>
    </div>
    <div class="ddnewhead_bottom">
    <div class="ddnewhead_search_panel">
      <div class="ddnewhead_search">
        <div class="ddnewhead_adsearch"></div>
  </div>          <div class="clear"></div>
         </div>
    </div>
</div>
<script type="text/javascript">initHeaderOperate();Suggest_Initialize("key_S",255,0,30);</script><div id="suggest_key" class="suggest_key" style="position: absolute; left: 472px; top: 138px; z-index: 10000; background-color: White; display: none;"></div>

		<!--通栏广告-->
				<div class="book_banner" name="__Ad-TL"><img src="pic/logo.jpg" border="0" /></div>

		<div class="book_wrap">
			<!--左栏-->
			<div class="book_sub">

<div class="sub_box" name="__ReMenZZ">
    <h2 class="book_author">热门作者</h2>
    <div class="sub_box_author">
        <ul>
                        <li><a href="#" target="_blank" title="任汝芬">任汝芬</a>
							<a href="#" target="_blank" title="宫东风">宫东风</a></li>
							                                <li><a href="#" target="_blank" title="陈文灯">陈文灯</a>
							<a href="#" target="_blank" title="华图">华图</a></li>
							                                <li><a href="#" target="_blank" title="中公版">中公版</a>
							<a href="#" target="_blank" title="光华版">光华版</a></li>
							                                <li><a href="#" target="_blank" title="郭崇兴">郭崇兴</a>
							<a href="#" target="_blank" title="新东方">新东方</a></li>
							                                <li><a href="#" target="_blank" title="星火考研">星火考<span class="dot">...</span></a>
							<a href="#" target="_blank" title="张剑">张剑</a></li>
							                                <li><a href="#" target="_blank" title="胡敏">胡敏</a>
							<a href="#" target="_blank" title="石春祯">石春祯</a></li>
							                                <li><a href="#" target="_blank" title="朱泰祺">朱泰祺</a>
							<a href="#" target="_blank" title="夏荣">夏荣</a></li>
							                                <li><a href="#" target="_blank" title="李永乐">李永乐</a>
							<a href="#" target="_blank" title="蔡子华">蔡子华</a></li>
							                                <li><a href="#" target="_blank" title="黄先开">黄先开</a>
							<a href="#" target="_blank" title="陈先奎">陈先奎</a></li>
							                                <li class="noline"><a href="#" target="_blank" title="祁非系列">祁非系<span class="dot">...</span></a>
							    <a href="#" target="_blank" title="肖秀荣">肖秀荣</a></li>
                            </ul>
    </div>
</div>


			</div>


			<!--中栏-->
			<div class="book_main">

<div class="book_channel"></div>				


<div id="main_focus" name="__LunZhuan"></div>

<!--script languang="\&quot;javascript\&quot;">
	$('#point_list').cycle({
		fx:'fade',
		cleartype:  0,
		timeout:5000,
		speed:1000,
		click:null,
		pause:true,
		next:null,
		prev:null
	});
</script-->
				
<div class="main_box" name="__TeJiaS">
	<div class="main_box_t">
		<h2>库存图书</h2>
	</div>
	<div class="main_box_c">
		<ul class="book_special">
			<%
			for(ListIterator iter=bookDB.getBooks().listIterator();iter.hasNext();){
				BookDetails book=(BookDetails)iter.next();
			%>
    <li ><span class="book_special_t">        
        	<a href="bookdetails.jsp?bookId=<%=book.getBookId()%>">
        		<b><%=book.getTitle()%>&nbsp;</b>
        	</a>
       </span>
    	<span class="book_special_price"> 
    		<%=book.getPrice()%>元 
    	</span> 
    	&nbsp;&nbsp;&nbsp;&nbsp;
    	<span style="background:#CFC" class="book_special_t2"> 
    		<a href="bookcatalog?Add=<%=book.getBookId()%>">
    			<b>加入购物车</b>
    		</a>
    	</span>
    	&nbsp;&nbsp;&nbsp;&nbsp;
         <span style="background-color:#FFC" class="book_special_price"> 作者: <em><%=book.getName()%>&nbsp;</em>
    </span>
    </li> 
    <br/>
    <%}%>
		</ul>
		<div class="clear"></div>
	</div>
</div>
<!--特价书结束-->

			</div>


			<!--右栏-->
		  <div class="book_third">

				<!--书讯快递-->
<!--书讯快递开始-->
<div class="book_news">
    <h2>书讯快递</h2>
    <div class="book_news_c">
                <ul>
                            	 <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="2011最新版计算机等级考试教材上市">2011最新版计算机等级考试教<span class="dot">..</span></a>
					<li name="____ShuXunKD-w">·<a href="#" target="_blank" title="2011光华版公务员考试最后冲刺！">2011光华版公务员考试最后冲<span class="dot">...</span></a></li>
                                <li name="____ShuXunKD-w"><a href="#" target="_blank" title="2010软考大纲及指定教材80折！">&middot;2010软考大纲及指定教材80折</a></li>
                                <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="抢购！2011新大纲公务员教材半价抢！">抢购！2011新大纲公务员教材<span class="dot">...</span></a></li>
 <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="2011最新版计算机等级考试教材上市">2011最新版计算机等级考试教<span class="dot">..</span></a>                                
<li name="____ShuXunKD-w">·<a href="#" target="_blank" title="抢购！2011考研全面冲刺，辅导书50折起！">抢购！2011考研全面冲刺，辅<span class="dot">...</span></a></li>
                                <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="2011最新版计算机等级考试教材上市">2011最新版计算机等级考试教<span class="dot">..</span></a><a href="#" title="2010成人高考教材现货80折"></a></li>

                                <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="抢购！2011新大纲公务员教材半价抢！">抢购！2011新大纲公务员教材<span class="dot">...</span></a></li>
                                <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="抢购！2011考研全面冲刺，辅导书50折起！">抢购！2011考研全面冲刺，辅<span class="dot">...</span></a></li>
        <li name="____ShuXunKD-w">·<a href="#" target="_blank" title="2011最新版计算机等级考试教材上市">2011最新版计算机等级考试教<span class="dot">..</span></a>
 </ul>
<div class="book_news_banner" name="__ShuXunKD-p"><img src="pic/bhky.jpg"></div>
    </div>
</div>



<div class="book_bang_5star" name="__5xingbang"></div>	 

			</div>

			<div class="clear"></div>
			<!--class="second"结尾-->
		</div>
    <div class="footer_copyright"><span>Copyright (C) 快乐购书网 2010-2016, All Rights Reserved</span><a href="#"><img src="pic/validate.gif"></a><span><a href="#" target="_blank">京ICP证041189号</a></span><span><a href="#" target="_blank">音像制品经营许可证 京音网8号</a></span></div>

</body>
</html>