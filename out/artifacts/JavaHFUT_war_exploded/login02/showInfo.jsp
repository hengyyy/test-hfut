<%@ page import="java.util.Enumeration" %><%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-03-31
  Time: 11:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    request.setCharacterEncoding("gb2312");
%>
<html>
<head>
    <title>使用Request对象</title>
</head>
<body bgcolor="#7fffd4">
<h1>你刚才输入的内容是：<br></h1>
<%
    Enumeration enu = request.getParameterNames();
    while (enu.hasMoreElements()){
        String parameterName = (String) enu.nextElement();
        String parameterValue = (String) request.getParameter(parameterName);
        out.print("参数名称："+parameterName+"<br>");
        out.print("参数内容："+parameterValue+"<br>");
    }
%>

</body>
</html>
