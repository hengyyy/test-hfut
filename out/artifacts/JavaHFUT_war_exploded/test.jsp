<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-03-31
  Time: 10:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<body>
<p>请输入圆的半径</p>
<form action="" method="get" name="form">
    <input type="text" name="radius">
    <input type="submit" value="开始计算" name="submit">
</form>

<%!double area(double r){
    return Math.PI*r*r;
}
double perimeter(double r){
    return Math.PI*2*r;
}%>
<% String str = request.getParameter("radius");
if (str!=null){
    try{
        double r;
        r=Double.parseDouble(str);
        %>
<p>圆的面积是： <%=area(r)%>
<p>圆的周长是：<%=perimeter(r)%>
    <%
        }catch (Exception e){
        out.print(e.getMessage());
        }
    }
        %>

</body>
</html>
