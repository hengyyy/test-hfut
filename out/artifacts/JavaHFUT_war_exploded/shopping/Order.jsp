<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=gb2312" pageEncoding="gb2312"%>
<%@ page import="shopping.cart.*"%>
<%
	Cart c = (Cart) session.getAttribute("cart");
	if (c == null) {
		c = new Cart();
		session.setAttribute("cart", c);
	}
	double totalPrice = c.getTotalPrice();
	request.setCharacterEncoding("GBK");
	String action = request.getParameter("action");
	Map products = (HashMap) session.getAttribute("products");
	List<CartItem> items = c.getItems();
%>
<html>
<head>
<title>订单</title>
</head>
<body background=images/bckgrd.jpg>
	<style>
#tab {
	font-size: 20px;
	font-family: 黑体;
	text-algn: left;
}
</style>
	<body>
	<center> 
	<H1>您的订单</H1>
	<table id="tab" align="center" border="1" cellspacing="0">
		<tr>
			<td>产品ID</td>
			<td>产品名称</td>
			<td>购买数量</td>
			<td>单价</td>
			<td>总价</td>
		</tr>
		<%
			for (Iterator<CartItem> it = items.iterator(); it.hasNext();) {
				CartItem ci = it.next();
		%>
		<tr>
			<td><%=ci.getProduct().getId()%></td>
			<td><%=ci.getProduct().getName()%></td>
			<td><%=ci.getCount()%></td>
			<td><%=ci.getProduct().getPrice()%></td>
			<td><%=ci.getProduct().getPrice() * ci.getCount()%></td>

		</tr>
		<%
			}
		%>
		<tr>
			<td colspan=3 align="right">所有商品总价格为：</td>
			<td colspan=3><%=c.getTotalPrice()%></td>
		</tr>
	</table>
</body>
    

</html>  