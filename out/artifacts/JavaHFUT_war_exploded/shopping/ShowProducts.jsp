<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=gb2312" pageEncoding="gb2312"%>
<%@ page import="shopping.cart.*"%>
<html>
<head>
<title>产品展示</title>
</head>
<body background="images/bckgrd.jpg">
	<style>
#tab {
	font-size: 16px;
	font-family: 黑体;
	text-algn: left;
}
</style>
	<%
		Map products = new HashMap();
		products.put("1612001", new Product("1612001", "mp3播放器",
				"3.54英寸的960×640显示屏 内存容量：运行内存1G 续航时间：最高10小时的续航时间 接口：Type-C、支持USB3.1标准。", 1699.00));
		products.put("1612002", new Product("1612002", "索尼NW-WM1A/BM",
				"内存容量：128G	接口：USB 2.0接口 屏幕尺寸：4英寸 支持音频格式：MP3,WMA,AAC,FLAC", 8999));
		products.put("1612003", new Product("1612003", "山灵M2s无损音乐播放器",
				"接口：USB 2.0接口 屏幕尺寸：3英寸	支持音频格式：MP3,WMA,WAV,OGG,APE,AAC,FLAC,ALAC,,AIFF,DSF,DIFF", 1098.00));
		products.put("1612004", new Product("1612004", "苹果iPod nano 7",
				"存储容量： 16GB 存储介质： 闪存  屏幕尺寸： 2.5英寸	屏幕分辨率： 240×432像素", 1148.00));
		products.put("1612005",
				new Product("1612005", "爱国者M6", "内存容量：32G 续航时间：理论音频回放时间25小时 接口：USB 2.0接口	屏幕尺寸：2.3英寸", 899.00));
		products.put("1612006", new Product("1612006", "纽曼B51",
				"内存容量：8G 屏幕尺寸：1英寸 支持音频格式：MP3,WMA,WAV FM功能：支持FM功能 	文本阅读：支持文本阅读功能,支持.TXT", 119.00));
		session.setAttribute("products", products);
	%>
	<center>
		<H1>产品显示</H1>
		<form name="productForm" action="" method="POST">
			<input type="hidden" name="action" value="purchase">
			<table id="tab" border="1" cellspacing="0">
				<tr>
					<td>序号</td>
					<td>产品图赏</td>
					<td>产品名称</td>
					<td>产品描述</td>
					<td>产品单价（￥）</td>
					<td>添加到购物车</td>
				</tr>
				<%
					Set productIdSet = products.keySet();
					Iterator it = productIdSet.iterator();
					int number = 1;
					while (it.hasNext()) {
						String id = (String) it.next();
						Product product = (Product) products.get(id);
				%><tr>
					<td><%=number++%></td>
					<%
						String image = "images/00" + number + ".jpg";
					%>

					<td><img src=<%=image%> align=right alt="产品" width=200
						height=150></td>
					<td><%=product.getName()%></td>
					<td><%=product.getDescription()%></td>
					<td><%=product.getPrice()%></td>
					<td><a href="Buy.jsp?id=<%=product.getId()%>&action=add"
						target="cart">我要购买</a></td>
				</tr>
				<%
					}
				%>
			</table>
		</form>
</body>
</html>
