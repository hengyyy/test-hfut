<%--
  Created by IntelliJ IDEA.
  User: 86176
  Date: 2024-04-01
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page import="zhang.com.pojo.Users" %>
<html>
<head>
    <title>EL 标签访问示例</title>

</head>
<body>
<%
    Users user = new Users();
    user.setAddress("中国");
    user.setAge(20);
    user.setName("王五");
    request.setAttribute("user",user);
%>

用户信息： ${user};<br/>
用户年龄： ${user.age}，用户姓名：${user.name}

</body>
</html>
